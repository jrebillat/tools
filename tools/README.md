# Tools
This package contains - and will contain more - classes containing tools used by the other Alantea packages. This tools are heavier than the ones contained in the *utils* package.

Current tools are :
- Addons management : how to create addons and upload them at run time
- Class loaders management : adding class loaders from jars
- Color management for simple color scheme definition
- Configuration : configuration file and values management
- Crypto : a simple tool
- File watching : to survey if a file or directory has changed in real time
- Lang : a language tool- Plugin management : how to create plugins and upload them at run time
- Plugins management : how to create and manage plugins
- Class scanner : scanning jars to find classes, based on Fast Class Path Scanner (from io.github.lukehutch).
- Steam : State machines management
- XML managing : simple XML reading and writing API over Sax HML handler
- Zip management : zip file storing and recovering

## Addons management
The addons utility classes helps to load addons jars at run time into new class loaders.
Basically, an addon is a jar file (along with its dependencies if needed) that must be activated on startup of the application or at some moment.
Addons are put in a directory (a proposal we may give, not mandatory, is to name it "addons"). They may be right in this directory or in direct subdirectories (an advice is to put each addon and its dependencies in a separate subdirectory)
Addons may also be in the master (standard) class path for the application and loaded when required by the application.
Addons classes are supposed to throw AddOnsException on errors.

A jar is an addon jar only if :
- it contains one or more concrete classes deriving from the net.alantea.tools.addons.AddOn abstract class
- there is, at the root path in the jar, a text file containing the full name of each addon class to load and manage, named "addon.txt" (mandatory)

Note : the addon.txt file may content comment lines (lines where the first non-blank character is a # character).

### AddOn classes
An addOn derived class must implement the `public void initialize() throws AddOnsException` method that will be called on loading the addon.
Its content is up to the addon writer and the application owner, to define what to do and what may be done in it to initialize the addon.
This is the only class that a addon writer has to consider.

### AddOnManager
This class manage addons. The application has to give it the directories to search for addons implementations, thru the methods :
- `public static final void loadDirectories(String... directories)` to load addons from several directories, given their path names.
- `public static final void loadDirectory(String directory)` to load addons from one directory, given it path name.
- `public static final void loadDirectory(File directory)` to load addons from one directory.

An application may have addons already wrapped in the distribution and  want them to be loaded directly. This is done by using a method :
- `public static final void loadMasterClassLoaderAddOns(String infoFilePath)` to load addons, getting the list in a given text file.

The master text file may be anywhere in the class path and must contain a list (one per line) of addon.txt-like files to gather.

For applications that may have other actions to done on addons, there is a way : just derive the AddOnManager and the AddOn classes to add functionnalities and use the protected method to get the found addons list :
- `protected static List<net.alantea.tools.addons.AddOn> getAddOns()` to get the found addons list.

### AddOnApplication

## Class loaders
The ClassLoaderManager utility class helps to load jars at run time into new class loaders. It offers many methods to load those jars in various situations :
- `public static URLClassLoader createClassLoader(File... files)` to create a class loader specific for a list of jar files.
- `public static URLClassLoader createClassLoader(ClassLoader parent, File... files)` to create a class loader specific for a list of jar files, specifying the parent class loader.
- `public static URLClassLoader createClassLoader(URL... urls)` to create a class loader specific for a list of jars given their URLs.
- `public static URLClassLoader createClassLoader(ClassLoader parent, URL... urls)` to create a class loader specific for a list of jars given their URLs, specifying the parent class loader.
- `public static URLClassLoader createClassLoader(String... paths)` to create a class loader specific for a list of jars given their paths on disk.
- `public static URLClassLoader createClassLoader(ClassLoader parent, String... paths)` to create a class loader specific for a list of jars given their paths on disk, specifying the parent class loader.
- `public static URLClassLoader createClassLoaderFromDirectories(File... dirs)` to create a class loader specific for a list of jars all located in some directories, recursing on subdirectories.
- `public static URLClassLoader createClassLoaderFromDirectories(ClassLoader parent, File... dirs)` to create a class loader specific for a list of jars all located in some directories, recursing on subdirectories, specifying the parent class loader.
- `public static URLClassLoader createClassLoaderFromDirectories(boolean recurse, File... dirs)` to create a class loader specific for a list of jars all located in some directories, recursing or not on subdirectories.
- `public static URLClassLoader createClassLoaderFromDirectories(ClassLoader parent, boolean recurse, File... dirs)` to create a class loader specific for a list of jars all located in some directories, recursing or not on subdirectories, specifying the parent class loader.

This utility allows also to compile sources at run time and instanciate it (if it has an empty public constructor)

## Color wheel
A color wheel define a set of colors depending on one base value.
Think of colors as a disk with the various colors as rays on it. If you select one color as the base one, then there are some evident choices for others colors on the screen.
First, let us try to keep the same light as the base color. You may find the opposite color (green versus red, for example). Then comes two colors at left and right of the base, that are best choices. Then if you cut the wheel in three parts, you will find two other colors to use.
Then, for the base color, you may want to use darker ou lighter colors or maybe a quite-black or quite-white color similar to the base color.
You may also want to get a color with variation in the opacity (alpha) for the color.
You may also want to change the values for the color as hue, saturation or brightness.
To finish, there is a particular color : the same as the base one, but grayed (no color).

The values are stored internally as properties (see the lightprops package). These properties are accessible.

There are too many methods to list them here. Most of them are made to get every of the previously listed colors (or a property containing it) from a Color wheel.

The constructors for a color wheel are :
- `public ColorWheel(Color color)` to get a color wheel for a specific AWT color.
- `public ColorWheel(String colorname)` to get a color wheel for a specific color given its name as found in the net.alantea.utils.ColorNames list.
- `public ColorWheel(Property<Color> colorProperty)` to get a color wheel for a specific AWT color stored in a property. All color wheel values will reflect changes in the original property.
- `public ColorWheel(double hue, double saturation, double brightness, double alpha)` to get a color wheel for a specific color given its definition values.

## Configuration
A configuration file for an application is a file holding key/value pairs of information to parameterize the application. The configuration tool manage an XML file named *configuration.xml* and offers ways to access, read, add or modify its content.

The file is loaded at the first call to the Configuration tool and saved when some methods are called - see below. It may also be saved when wanted, as an example when the application exits. A file containing default values for the keys may be stored by the application coder in  a file in the "/configuration/defaults.xml" file. Configuration values may also be stored in .properties files in entries of the form Configuration.<key> = <value>.
Use `public static void save()` to save the configuration file.

The key/value pairs may be used by several methods.

### Reading a key
The String value associated to a key may be read by:
- `public static String getConfigurationValue(String key)` to get the value.
- `public static String getConfigurationValue(String key, String attribute)` to get the a key:attribut value.

The value associated to a key may be read as a simple number by:
- `public static int getInteger(String key)`.
- `public static double getDouble(String key)`.
- `public static boolean getBoolean(String key)`.
- `public static int getInteger(String key)`.

### Storing or modifying a value
To store a key and its value :
- `public static void setConfigurationValue(String key, String value)` to store a key/value pair without saving the configuration file.
- `public static void setConfigurationValue(String key, String value, boolean store)` to store a key/value pair and save the configuration file.
- `public static void setConfigurationValue(String key, String attribute, String value)` to store a key:attribute/value pair and save the configuration file.

### Getting a key as Property
Internally, the keys are stored as Properties (from the liteprops package) containing a String. A property may be got to be used by the code:
- `public static StringProperty getConfigurationValueProperty(String key)` to get the value property.
- `public static StringProperty getConfigurationValueProperty(, String attribute)` to get the a key:attribut value property.

The values may be used as simple values in the application. It is possible to associate an application Property to the key property:
- `public static void associateProperty(BooleanProperty property, String key)` to associate the value property to a boolean property.
- `public static void associateProperty(DoubleProperty property, String key)` to associate the value property to a double property.
- `public static void associateProperty(FloatProperty property, String key)` to associate the value property to a float property.
- `public static void associateProperty(IntegerProperty property, String key)` to associate the value property to a integer property.
- `public static void associateProperty(LongProperty property, String key)` to associate the value property to a long property.
- `public static void associateProperty(StringProperty property, String key)` to associate the value property to a String property.

## Crypto
This tool defines a Codec class with static methods to manage 64 bits encryption. The given algorithm is AES.

The method are
- `public static byte[] encrypt(String data, String key, String algorithm) throws GeneralSecurityException` to encrypt a string with an algorithm.
- `public static byte[] encrypt(byte[] data, String key, String algorithm) throws GeneralSecurityException` to encrypt a byte array with an algorithm.
- `public static byte[] decrypt(File data, String key, String algorithm) throws GeneralSecurityException` to decrypt a file content with an algorithm.
- `public static byte[] decrypt(InputStream data, String key, String algorithm) throws GeneralSecurityException` to encrypt an input stream content with an algorithm.
- `public static byte[] decrypt(String data, String key, String algorithm) throws GeneralSecurityException` to decrypt a string with an algorithm.
- `public static byte[] decrypt(byte[] data, String key, String algorithm) throws GeneralSecurityException` to decrypt a byte array with an algorithm.

## Lang
TBW.

## Plugin management
This part deals with adding plugins to a piece of software, either by adding the plugin jars into the cla	ss path or loading them at run time using the ClassLoaderManager utility.

### Plugin API definition
First of all, it is needed to define, in the application, an API for the plugin. It is mostly an interface, but may be also a class (abstract or not) to be derived in the plugin implementation.
To define a API, just mark the API interface or class with a @PluginApi("apiKey") annotation. The API key should be unique and not null.

### Plugin implementation
Implementing a plugin API in a plugin class is as straightforward as implementing the API (either by implementing the API interface or extending the API class) ans setting an annotation @PluginInstance() to the implementation class.
An implementation may implement one or more APIs, it does not matter.
There is only one requirement on the class : * There must be a public constructor without argument * for the plugin class to be instanciated.

### Using the plugin manager
In the application, plugins are managed using the PluginManager utility. It is mostly transparent, except that the application will have to use those methods :
- `public static <T> List<T> getPlugins(String apiKey)` to get the list of the currently instanciated implementation for the plugin API with given api key. Methods on these plugins may then be called.
- `public static <T> List<T> getPlugins(Class<T> apiInterface)` to get the list of the currently instanciated implementation for the given plugin API. Methods on these plugins may then be called.

To allow the application to load at run time some plugins, use the method :
- `public static void addPluginContainer(ClassLoader loader)` to add a new class loader (see above on class loader management) to the plugin implementations search path. The found instances will be created on next request to a getPlugins method.

## Scanner
The Scanner class contains static methods to search the complete class path for classes with specific features. It is internally base on the *io.github.lukehutch.fastclasspathscanner.FastClasspathScanner* utility. FastClassScanner is fast but may take a few seconds to scan the class path. Scanner makes only one call to it and, afterwards, may give any information required amongst the available ones.

It is faster this way than using several calls to FastClassScanner.

Then available methods are :
- `public static List<String> getNamesOfAllClasses()` to get the names of all classes in the class path.
- `public static List<String> getNamesOfClassesWithAnnotationsAnyOf(Class<?>... annotations)` to get the names of all classes in the class path with one of the given annotation classes.
- `public static List<String> getNamesOfClassesWithAnnotationsAnyOf(String... annotations)` to get the names of all classes in the class path with one of the given annotation class names.
- `public static List<String> getNamesOfClassesWithAnnotationsAllOf(Class<?>... annotations)` to get the names of all classes in the class path with all of the given annotation classes.
- `public static List<String> getNamesOfClassesWithAnnotationsAllOf(String... annotations)` to get the names of all classes in the class path with all of the given annotation class names.
- `public static List<String> getNamesOfClassesImplementing(Class<?>... interfaceClass)` to get the names of all classes in the class path implementing the interface class.
- `public static List<String> getNamesOfClassesImplementing(String... interfaceClass)` to get the names of all classes in the class path implementing the interface class name.
- `public static List<String> getNamesOfClassesWithAnnotation(Class<?> annotation)` to get the names of all classes in the class path with the annotation class.
- `public static List<String> getNamesOfClassesWithAnnotation(String annotation)` to get the names of all classes in the class path with the annotation class name.
- `public static List<String> getNamesOfSubclassesOf(Class<?> baseClass)` to get the names of all classes in the class path that are subclassing the base class.
- `public static List<String> getNamesOfSubclassesOf(String baseClass)` to get the names of all classes in the class path that are subclassing the base class name.
- `public static List<String> getNamesOfClassesWithFieldAnnotation(Class<?> annotation)` to get the names of all classes in the class path that have a Field annotated with given annotation.
- `public static List<String> getNamesOfClassesWithFieldAnnotation(String annotation)` to get the names of all classes in the class path that have a Field annotated with given annotation name.
- `public static List<String> getNamesOfClassesWithMethodAnnotation(Class<?> annotation)` to get the names of all classes in the class path that have a Method annotated with given annotation.
- `public static List<String> getNamesOfClassesWithMethodAnnotation(String annotation)` to get the names of all classes in the class path that have a Method annotated with given annotation name.
- ` public static void appendClassLoader(ClassLoader loader)` to append a class loader to the search path. This will trigger a new scan (that may be long several seconds) on, the next "get" call.

## Steam - State Machines
A state machine is a class that manage a current State and its changes. There are two main ways to see a state machine:
- State-oriented : The machine is driven by state changes. When state changes, some listeners are called to advertise about the state change and do some work.
- Event-oriented : The machine has a current state, that may change when some events occurred. Listeners are here set on events

### State orientation
This type of state machine is related to some sort of states. It is chosen here to assign a reference class to the state notion.
All possible states must be of the reference class or their class have to inherit it. The StateMachine class is generic (the State class is referred to in methods as "S"). The base class is thus StateMachine<S>.

As the state may be changed from outside, the machine must know its possible states : it is thus needed to inform the machine of the states that it may be set to. This may be occulted by special classes, as the EnumeratedStateMachine.
- `public boolean addState(S newState)` to add a possible state.
- `public boolean addStates(S... newStates)` to add a few possible states.

All changes are not allowed at first. It is needed to specify which changes are allowed - or to allow all changes.
- `public boolean allowChange(S oldState, S newState)` to add a possible state change from an identified one to an identified other.
- `public boolean allowLeaveChange(S oldState)` to add all possible state changes from an identified one.
- `public boolean allowEnterChange(S newState)` to add all possible state changes to an identified state.
- `public boolean allowAllChanges()` to allow all state changes.

It is possible to add listeners to some kind of changes. To be noticed that adding a listener implies to allow the corresponding change.
- `public addListener(S oldState, S newState, StateListener listeners)` to add a listener to a specific change.
- `public addLeaveListener(S oldState, StateListener listener)` to add a listener to all changes from a specified state.
- `public addEnterListener(S newState, StateListener listener)` to add a listener to all changes to a specified state.
- `public addListener(StateListener listener)` to add a global change listener.
- `public addListener(S oldState, S newState, StateListener... listeners)` to add listeners to a specific change.
- `public addLeaveListener(S oldState, StateListener... listeners)` to add listeners to all changes from a specified state.
- `public addEnterListener(S newState, StateListener... listeners)` to add listeners to all changes to a specified state.
- `public addListener(StateListener... listeners)` to add global change listeners.

It is also possible to add a thread (here named as a Reactor) to a state. The reactor is stated on entering the state and stopped when leaving.
The reactor loop on calling a runnable and wait some timeout before looping. A state may have several reactors.
- `public boolean createStateReactor(S state, long timeout, Runnable runnable)` to a reactor a state.

When all is defined, it is possible to manage the current state.
- `public S getState()` to get the current state.
- `public boolean setState(S newState)` to trigger a state change.
- `public boolean setStateReference(String newStateReference)` to trigger a state change by giving the state reference, defined as the toString() value of the state.
- `public S identifyState(String key)` to get a state reference, defined as the toString() value of the state.

The current state is a Property. It is possible to link a Property<S> to the state :
- `public void linkToState(Property<S> linkedProperty)` to link a Property<S> to the state of the machine.

The listeners are called whenever it is needed. There are various modes possible to call the listeners, depending on the way the application is built :
- Sequential : call each listeners in the order they have been added to the change. This way, you know the call order but it may push your application to be blocked for a while.
- One thread : create a thread and call each listeners in it, following the order they have been added to the change. This way, you know the call order and it will not block the application but may block the following listeners.
- Threaded : create a thread for each listener, to call it. The listeners are called in parallel and do not block the application and do not block any listener, but the execution order is not defined.
The method is :
- `public void setMode(StateMachine.Mode mode)` with the mode being one of SEQUENTIAL, ONE_THREADED or THREADED.

Please notice that, except for the sequential mode, the listeners are called in separate threads, thus, if there are several state changes set quickly once after another, listeners from the first change may be called after those of the following changes.

You may create a state machine simply by specifying, or also giving it a name.
- `public StateMachine(S initialState)` creates a state machine and set the initial State, that is automatically added to the machine.
- `public StateMachine(String reference, S initialState)` creates a state machine with a reference name and set the initial State, that is automatically added to the machine.

A named machine may be recalled.
- `public static StateMachine<?> getMachine(String reference)` returns the named state machine or null.

### Derived State machines
There are some classes deriving from StateMachine, adding functionalities or simplifying the work for some kind of state classes.

#### BaseStateMachine
This is just a StateMachine<Object>.

#### StringStateMachine
This is just a StateMachine<String>.

#### EnumeratedStateMachine
This is a state machine with the states coming from an Enum. Every possible state value is added during creation. It is not possible to add a state.
The creation is done through:
- `public EnumeratedStateMachine(Class<E> enumClass, E startingState) throws LntException` creates the state machine.
- `public EnumeratedStateMachine(String machineReference, Class<E> enumClass, E startingState) throws LntException` creates the named state machine.

#### EventStateMachine
This is basically a state machine and all StateMachine methods may be used with it - but it is clearly not recommended. All the changes are driven through "events".

You may create an event state machine simply by specifying, or also giving it a name.
- `public EventStateMachine<S, E>(S initialState)` creates an event state machine and set the initial State.
- `public EventStateMachine<S, E>(String reference, S initialState)` creates an event state machine with a reference name and set the initial State.

An event is an object of type Event. An Event is created from an event key, which has an unique UUID.
- `public Event(EventKey key)` creates an event from an event key.
- `public Event(EventKey key, Object data)` creates an event from an event key and attach a user data object to it.

An EventKey is just created by
- `public EventKey()` creates an event key.

An event state machine may receive transition information. A transition comes when an event of a given EventKey is triggered when the machine is in a given (initial) state. This will call the specified actions and set the machine in a new (targeted state) state.
- `public final EventStateMachine<S, E> addTransitions(S initial, S targeted, E event, MachineEventListener<S,E>... actions)` adds the transition to the machine.

The application may afterwards trigger an event through
- `public void trigger(Event event)` to trigger all transitions in all event state machines that has registered one for the event's EvenKey.


### Event machines
As you have seen above, a state machine is a big thing with heavy behavior. On the contrary, Event-driven machines are light- weight instances with only a few methods.
An event machine needs two classes for generics : the state class and the event class
You may create an event machine simply by specifying, or also giving it a name.
- `public EventMachine<S, E>(S initialState)` creates an event machine and set the initial State.
- `public EventMachine<S, E>(String reference, S initialState)` creates an event machine with a reference name and set the initial State.

An event may receive transition information. A transition comes when an event is triggered when the machine is in targeted state. This will call the specified actions and set the machine in a new state.
- `public final EventStateMachine<S, E> addTransitions(S initial, S targeted, E event, MachineEventListener<S,E>... actions)` creates an event machine with a reference name and set the initial State.

The application may then trigger actions on the machine when needed.
- `public void trigger(E event)` trigger the specified event.

The listeners are called whenever it is needed. There are various modes possible to call the listeners, depending on the way the application is built :
- Sequential : call each listeners in the order they have been added to the change. This way, you know the call order but it may push your application to be blocked for a while.
- One thread : create a thread and call each listeners in it, following the order they have been added to the change. This way, you know the call order and it will not block the application but may block the following listeners.
- Threaded : create a thread for each listener, to call it. The listeners are called in parallel and do not block the application and do not block any listener, but the execution order is not defined.
The method is :
- `public void setMode(EventMachine.Mode mode)` with the mode being one of SEQUENTIAL, ONE_THREADED or THREADED.

Please notice that, except for the sequential mode, the listeners are called in separate threads, thus, if there are several events triggered quickly once after another, listeners from the first event may be called after those of the following events.

## XML managing
This tool offers a simplified class over the SAX DefaultHandler and an abstract XML writer class, both to be extended by subclasses.

### XML Parsing
The XMLParser offers some advanced facilities to help parsing an XML file. Basically, it is just an SAXhandler, but the standard XMLParser methods should not be used. As normally, the parser will trigger a method when starting to parse an element, and another when ending the element parsing.

But, instead of using the same method for all elements, the mechanism is using reflection to find the correct method for starting or ending the element parsing.

The method to parse the start of an element named "element" is a method of the class - or of a base class - which name is `elementStart` (the name of the element followed by Start) and which takes as single argument an Attributes instance. The method may be either private, protected or public, but not static. It may throw a SaxException.

The method to parse the end of an element named "element" is a method of the class - or of a base class - which name is `elementEnd` (the name of the element followed by End) and which takes no argument. The method may be either private, protected or public, but not static. It may throw a SaxException.

During the ending part, it is possible to call the `String getCurrentText()` to get the text characters of the element.

The parsing of a file with a "MyParser" class is triggered by one of the methods :
-  `Object new MyParser().load(File file);`
-  `Object new MyParser().load(Stream stream);`
-  `Object new MyParser().load(String xlmString);`.

These methods returns an Object  (or a List or whatever suits the needs) which is defined by calling the `setDocumentObject()`method.
### XML Writing
The XML writer is an abstract container for XML file writing.
For an end-user coder (using one of its derived class) it offers several simple ways to write a file :
-  `String new MyWriter().write();` which write the XML in a String an returns it.
-  `Object new MyWriter().write(OutputStream stream);` write in a Stream.
-  `Object new MyWriter().write(String path);` which create a File onto the given path and write the XML result into it.

As a derived-class coder, you will have to implment the abstract method :
-  `void populate(Document document);` which contains all the writing logic to fill an XML document.
You will also have the help of several methods to simplify the writing :
-  `void setAttribute(Document doc, Element element, String attributeName, BigInteger value);` which adds a BigInteger value in the given attribute for the given element.
-  `void setAttribute(Document doc, Element element, String attributeName, Date value, int outmode);` which adds a Date value in the given attribute for the given element, with a specified format (either XmlWriter.MODE_TIMESTAMP, XmlWriter.MODE_DATESTRING or XmlWriter.MODE_TIMESTRING).
-  `void setAttribute(Document doc, Element element, String attributeName, boolean value);` which adds a boolean value in the given attribute for the given element.
-  `void setAttribute(Document doc, Element element, String attributeName, String value);` which adds a String value in the given attribute for the given element.
-  `String escapeXml(String s);` to create a valid XML content String from a String.

## Zip
These tools give access to zip file storing and recovering.

### ZipInputStreamContainer
This class offers methods that allow to read the content of an existing Zip file :
-  `public ZipInputStreamContainer(String path) throws IOException;` opens the zip file for reading.
-  `public ZipInputStreamContainer(OutputStream stream) throws IOException;` opens the zip output stream for reading.
-  `public List<String> getEntries();` list the content of the zip file and returns the inner path names.
-  `public InputStream getFile(String path) throws IOException;` get a contained file as an input stream.
-  `public void saveFile(String root, String path) throws IOException;` extract a contained file and save it on disk.

### ZipOutputStreamContainer
This class offers methods that allow to write the content of a Zip file :
-  `public ZipOutputStreamContainer();` creates the zip file for writing.
-  `public void append(File file, String outputPath) throws IOException;` copies a file or directory to the zip content.
-  `public void append(OutputStream stream, String outputPath) throws IOException;` copies the content of an input stream to the zip content.
-  `public void append(String path, String outputPath) throws IOException;` copies a file or directory given by its path to the zip content.
-  `public void appendDirectory(File file, String outputPath) throws IOException;` copies a directory to the zip content at the given path.
-  `public void appendDirectory(File file) throws IOException;` copies a top directory to the zip content.
-  `public byte[] close();` closes the zip file and returns its content as bytes.

### ZipUtils
This class offers static methods to simplify ZIP management.
-  `public static File openTemporaryDirectory(String key, String containerPath) throws IOException;` opens a zip file given by a name and put its content in a temporary directory.
-  `public static File openTemporaryDirectory(String key, File containerFile) throws IOException;` opens a zip file and put its content in a temporary directory.
-  `public static File openTemporaryDirectory(String key, ZipInputStreamContainer container) throws IOException;` opens a zip input stream and put its content in a temporary directory.
-  `public static void openDirectory(File containerFile, String destination) throws IOException;` opens a zip file and put its content in the given directory.
-  `public static void openDirectory(String containerPath, String destination) throws IOException;` opens a zip file given by a name and put its content in the given directory.
-  `public static void openDirectory(ZipInputStreamContainer container, String destination) throws IOException;` opens a zip input stream and put its content in the given directory.
-  `public static void saveDirectory(String directoryPath, String containerPath) throws IOException;` svaes a directory to a zip file given by a name.
