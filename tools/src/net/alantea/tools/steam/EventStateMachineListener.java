package net.alantea.tools.steam;

/**
 * The listener interface for receiving eventStateMachine events.
 * The class that is interested in processing a eventStateMachine
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addEventStateMachineListener</code> method. When
 * the eventStateMachine event occurs, that object's appropriate
 * method is invoked.
 *
 * @param <S> the generic type
 */
@FunctionalInterface
public interface EventStateMachineListener<S>
{
   
   /**
    * On event.
    *
    * @param oldState the old state
    * @param newState the new state
    * @param event the event
    */
   public void onEvent(S oldState, S newState, Event event);
}
