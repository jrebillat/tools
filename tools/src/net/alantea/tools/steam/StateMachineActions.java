package net.alantea.tools.steam;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class StateMachineActions.
 */
public final class StateMachineActions
{
   
   /** The runnables. */
   private static Map <String, Runnable> runnables = new HashMap<>();

   /**
    * Adds the action.
    *
    * @param reference the reference
    * @param runnable the runnable
    */
   public static void addAction(String reference, Runnable runnable)
   {
      if (reference != null)
      {
         runnables.put(reference,  runnable);
      }
   }
   
   /**
    * Run.
    *
    * @param reference the reference
    */
   public static void run(String reference)
   {
      Runnable runnable = runnables.get(reference);
      if (runnable != null)
      {
         runnable.run();
      }
   }
}
