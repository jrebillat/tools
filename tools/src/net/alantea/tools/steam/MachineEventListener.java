package net.alantea.tools.steam;

/**
 * The listener interface for receiving machineEvent events.
 * The class that is interested in processing a machineEvent
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addMachineEventListener</code> method. When
 * the machineEvent event occurs, that object's appropriate
 * method is invoked.
 *
 * @param <S> the generic type
 * @param <E> the element type
 */
@FunctionalInterface
public interface MachineEventListener<S, E>
{
   
   /**
    * On event.
    *
    * @param oldState the old state
    * @param newState the new state
    * @param event the event
    */
   public void onEvent(S oldState, S newState, E event);
}
