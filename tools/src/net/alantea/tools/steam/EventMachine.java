package net.alantea.tools.steam;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class EventMachine.
 *
 * @param <S> the state generic type
 * @param <E> the event element type
 */
public class EventMachine<S, E>
{
   /**
    * The Enum Mode.
    */
   public static enum Mode {
      
      /** The sequential. */
      SEQUENTIAL,
      
      /** The one thread. */
      ONE_THREAD,
      
      /** The threaded. */
      THREADED
   }
   
   /** The events map. */
   private Map<E, Map<S, Target<S, E>>> eventsMap = new HashMap<>();

   /** The current state. */
   private S currentState;
   
   /** The threading mode. */
   private Mode mode = Mode.SEQUENTIAL;

   /**
    * Instantiates a new event machine.
    *
    * @param initialState the initial state
    */
   protected EventMachine(S initialState)
   {
      currentState = initialState;
   }

   /**
    * Adds the transition.
    *
    * @param initial the initial
    * @param targeted the targeted
    * @param event the event
    * @param actions the actions
    * @return the state machine
    */
   @SafeVarargs
   public final EventMachine<S, E> addTransition(S initial, S targeted, E event, MachineEventListener<S,E>... actions)
   {
      if ((initial == null) || (targeted == null) || (event == null))
      {
         return this;
      }
      
      Map<S, Target<S, E>> eventMap = eventsMap.get(event);
      if (eventMap == null)
      {
         eventMap = new HashMap<>();
         eventsMap.put(event, eventMap);
      }
            
      Target<S, E> target = new Target<>();
      target.target = targeted;
      target.actions = Arrays.asList(actions);
      eventMap.put(initial, target);
      return this;
   }

   /**
    * Trigger.
    *
    * @param event the event
    */
   public void trigger(E event)
   {
      Map<S, Target<S, E>> eventMap = eventsMap.get(event);
      if ((eventMap != null) && (currentState != null))
      {
         Target<S, E> target = eventMap.get(currentState);
         if (target != null)
         {
            switch(mode) {
               case SEQUENTIAL :
                  callActions(currentState, target.target, event, target.actions);
                  break;

               case ONE_THREAD :
                  callActionsThread(currentState, target.target, event, target.actions);
                  break;

               case THREADED :
                  callActionsThreads(currentState, target.target, event, target.actions);
                  break;
            }
            currentState = target.target;
         }
      }
   }

   /**
    * Call actions.
    *
    * @param origin the origin
    * @param target the target
    * @param event the event
    * @param actions the actions
    */
   private void callActions(S origin, S target, E event, List<MachineEventListener<S, E>> actions)
   {
      for (MachineEventListener<S, E> action : actions)
      {
         try
         {
            action.onEvent(origin, target, event);
         }
         catch (Exception exception)
         {
            // Action may fail : system failure is not an option !
         }
      }
   }

   /**
    * Call actions thread.
    *
    * @param origin the origin
    * @param target the target
    * @param event the event
    * @param actions the actions
    */
   private void callActionsThread(S origin, S target, E event, List<MachineEventListener<S, E>> actions)
   {
      // Do the job
      new Thread() {
         public void run()
         {
            for (MachineEventListener<S, E> action : actions)
            {
               try
               {
                  action.onEvent(origin, target, event);
               }
               catch (Exception exception)
               {
                  // Action may fail : system failure is not an option !
               }
            }
         }
      }.start();
   }

   /**
    * Call actions threads.
    *
    * @param origin the origin
    * @param target the target
    * @param event the event
    * @param actions the actions
    */
   private void callActionsThreads(S origin, S target, E event, List<MachineEventListener<S, E>> actions)
   {
      // Do the job
      for (MachineEventListener<S, E> action : actions)
      {
         new Thread() {
            public void run()
            {
               try
               {
                  action.onEvent(origin, target, event);
               }
               catch (Exception exception)
               {
                  // Action may fail : system failure is not an option !
               }
            }
         }.start();
      }
   }

   /**
    * Gets the state.
    *
    * @return the state
    */
   public S getState()
   {
      return currentState;
   }
   
   /**
    * Sets the mode.
    *
    * @param mode the new mode
    * @return true, if successful
    */
   public boolean setMode(Mode mode)
   {
      this.mode = mode;
      return true;
   }
   
   /**
    * The Class Target.
    *
    * @param <S> the generic type
    * @param <E> the element type
    */
   private static class Target<S, E>
   {
      /** The target. */
      private S target;
      
      /** The actions. */
      private List<MachineEventListener<S, E>> actions;
   }
}
