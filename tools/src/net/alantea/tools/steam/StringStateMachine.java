package net.alantea.tools.steam;

/**
 * The Class StringStateMachine is a StateMachine with States based on Strings.
 */
public class StringStateMachine extends StateMachine<String>
{
   /**
    * Instantiates a new base state machine.
    *
    * @param startingState the starting state
    */
   public StringStateMachine(String startingState)
   {
      super(startingState);
   }
   
   /**
    * Instantiates a new base state machine.
    *
    * @param machineReference the machine reference
    * @param startingState the starting state
    */
   public StringStateMachine(String machineReference, String startingState)
   {
      super(machineReference, startingState);
   }
}
