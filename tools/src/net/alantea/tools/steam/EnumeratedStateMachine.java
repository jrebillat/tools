package net.alantea.tools.steam;

import net.alantea.utils.exception.LntException;

/**
 * The Class EnumeratedStateMachine is a StateMachine with States based on an Enum type for potential values.
 *
 * @param <E> the element type
 */
public class EnumeratedStateMachine<E extends Enum<?>> extends StateMachine<E>
{   
   
   /**
    * Instantiates a new enumerated state machine.
    *
    * @param enumClass the enum class
    * @param startingState the starting state
    * @throws LntException the lnt exception
    */
   public EnumeratedStateMachine(Class<E> enumClass, E startingState) throws LntException
   {
      super(startingState);
      if ((enumClass == null) || (!enumClass.isEnum()))
      {
         throw new LntException("bad State enum : " + enumClass);
      }
      
      for (E state : enumClass.getEnumConstants())
      {
         super.addState(state);
      }
   }

   /**
    * Instantiates a new enumerated state machine.
    *
    * @param machineReference the machine reference
    * @param enumClass the enum class
    * @param startingState the starting state
    * @throws LntException the lnt exception
    */
   public EnumeratedStateMachine(String machineReference, Class<? extends Enum<?>> enumClass, E startingState) throws LntException
   {
      super(machineReference, startingState);
      if (enumClass == null)
      {
         throw new LntException("bad State enum : " + enumClass);
      }
   }
   
   /**
    * Adds the states.
    *
    * @param newStates the new states
    * @return true, if successful
    */
   @SuppressWarnings("unchecked")
   public boolean addStates(E... newStates)
   {
      return false;
   }
   
   /**
    * Adds the state.
    *
    * @param newState the new state
    * @return true, if successful
    */
   public boolean addState(E newState)
   {
      return false;
   }
}
