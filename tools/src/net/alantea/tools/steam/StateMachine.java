package net.alantea.tools.steam;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.alantea.liteprops.Property;

/**
 * The Class BaseStateMachine.
 *
 * @param <S> the generic type
 */
public class StateMachine<S>
{
   /**
    * The Enum Mode.
    */
   public static enum Mode {
      
      /** The sequential. */
      SEQUENTIAL,
      
      /** The one thread. */
      ONE_THREAD,
      
      /** The threaded. */
      THREADED
   }
   
   /** The machines map. */
   private static Map<String, StateMachine<?>> machinesMap = new HashMap<>();
   
   /** The state. */
   private Property<S> state = new Property<>();
   
   /** The state. */
   private List<S> states = new LinkedList<>();
   
   /** The listeners. */
   private Map<S, Map<S, List<StateListener>>> identifiedListeners = new HashMap<>();
   
   /** The from listeners. */
   private Map<S, List<StateListener>> fromListeners = new HashMap<>();
   
   /** The to listeners. */
   private Map<S, List<StateListener>> toListeners = new HashMap<>();
   
   /** The global listeners. */
   private List<StateListener> globalListeners = new LinkedList<>();
   
   /** The changes maps. */
   private Map<S, List<S>> identifiedChangesMap = null;
   
   /** The from changes list. */
   private List<S> fromChangesList = null;
   
   /** The to changes list. */
   private List<S> toChangesList = null;
   
   /** The all changes allowed. */
   private boolean allChangesAllowed = false;
   
   /** The threading mode. */
   private Mode mode = Mode.THREADED;

   /**
    * Instantiates a new base state machine.
    *
    * @param startingState the starting state
    */
   public StateMachine(S startingState)
   {
      addState(startingState);
      setState(startingState);
   }
   
   /**
    * Instantiates a new base state machine.
    *
    * @param machineReference the machine reference
    * @param startingState the starting state
    */
   public StateMachine(String machineReference, S startingState)
   {
      this(startingState);
      machinesMap.put(machineReference, this);
   }
   
   /**
    * Gets the machine.
    *
    * @param reference the reference
    * @return the machine
    */
   public static StateMachine<?> getMachine(String reference)
   {
      return machinesMap.get(reference);
   }
   
   /**
    * Adds the states.
    *
    * @param newStates the new state
    * @return true, if successful
    */
   @SuppressWarnings("unchecked")
   public boolean addStates(S... newStates)
   {
      boolean ret = true;
      for (S state : newStates)
      {
         ret &= addState(state);
      }
      return ret;
   }
   
   /**
    * Adds the state.
    *
    * @param newState the new state
    * @return true, if successful
    */
   public boolean addState(S newState)
   {
      if (newState == null)
      {
         return false;
      }
      
      if (!states.contains(newState))
      {
         states.add(newState);
      }
      return true;
   }
   
   /**
    * Identify state.
    *
    * @param key the key
    * @return the s
    */
   public S identifyState(String key)
   {
      if (key == null)
      {
         return null;
      }
      
      for (S state : states)
      {
         if (key.equals(state.toString()))
         {
            return state;
         }
      }
      return null;
   }
   
   /**
    * Sets the state.
    *
    * @param newStateReference the new state reference
    * @return true, if successful
    */
   public boolean setStateReference(String newStateReference)
   {
      S newState = identifyState(newStateReference);
      if (newState != null)
      {
         setState(newState);
         return true;
      }
      return false;
   }
   
   /**
    * Sets the state.
    *
    * @param newState the new state
    * @return true, if successful
    */
   public boolean setState(S newState)
   {
      if (newState == null)
      {
         return false;
      }
      
      if ((!states.isEmpty()) && (!states.contains(newState)))
      {
         return false;
      }
      
      boolean doIt = false;
      if (allChangesAllowed)
      {
         doIt = true;
      }
      else if ((identifiedChangesMap == null)
            && (fromChangesList == null)
            && (toChangesList == null))
      {
         doIt = true;
      }
      else if ((globalListeners != null) && (!globalListeners.isEmpty()))
      {
         doIt = true;
      }
      else if (identifiedChangesMap != null)
      {
         List<S> allowed = identifiedChangesMap.get(getState());
         if ((allowed != null) && (allowed.contains(newState)))
         {
            doIt = true;
         }
      }
      else if ((fromChangesList != null) && (fromChangesList.contains(getState())))
      {
         doIt = true;
      }
      else if ((toChangesList != null) && (toChangesList.contains(newState)))
      {
         doIt = true;
      }

      if (doIt)
      {
         doChange(newState);
      }
      return doIt;
   }

   /**
    * Do change.
    *
    * @param newState the new state
    */
   private void doChange(S newState)
   {
      S oldState = getState();
      this.state.set(newState);

      // Calculate listeners list
      List<StateListener> allListeners = new LinkedList<>();
      Map<S, List<StateListener>> oldStateMap = identifiedListeners.get(oldState);
      if (oldStateMap != null)
      {
         List<StateListener> existing = oldStateMap.get(newState);
         if (existing != null)
         {
            allListeners.addAll(existing);
         }
      }

      List<StateListener> allForNew = fromListeners.get(oldState);
      if (allForNew != null)
      {
         allListeners.addAll(allForNew);
      }
      
      List<StateListener> allForOld = toListeners.get(newState);
      if (allForOld != null)
      {
         allListeners.addAll(allForOld);
      }

      allListeners.addAll(globalListeners);

      switch(mode) {
         case SEQUENTIAL :
            callListeners(allListeners, oldState, newState);
            break;

         case ONE_THREAD :
            callListenerInOneThread(allListeners, oldState, newState);
            break;

         case THREADED :
            callListenerInThreads(allListeners, oldState, newState);
            break;
      }

      if (oldState != null)
      {
         executeLeaveMethod(oldState);
         executeLeaveMethod(null);
         executeEnterMethod(newState);
         executeEnterMethod(null);
      }
   }

   /**
    * Call listener in threads.
    *
    * @param listeners the listeners
    * @param oldState the old state
    * @param newState the new state
    */
   private void callListenerInThreads(List<StateListener> listeners, S oldState, S newState)
   {
      // Do the job
      for (StateListener listener : listeners)
      {
         new Thread() {
            public void run()
            {
               try
               {
                  listener.onState(oldState, newState);
               }
               catch (Exception e)
               {
               // do nothing : listeners should mind their own business
               }
            }
         }.start();
      }
   }

   /**
    * Call listener in one thread.
    *
    * @param listeners the listeners
    * @param oldState the old state
    * @param newState the new state
    */
   private void callListenerInOneThread(List<StateListener> listeners, S oldState, S newState)
   {
      // Do the job
      new Thread() {
         public void run()
         {
            for (StateListener listener : listeners)
            {
               try
               {
                  listener.onState(oldState, newState);
               }
               catch (Exception e)
               {
                  // do nothing : listeners should mind their own business
               }
            }
         }
      }.start();
   }

   /**
    * Call listeners.
    *
    * @param listeners the listeners
    * @param oldState the old state
    * @param newState the new state
    */
   private void callListeners(List<StateListener> listeners, S oldState, S newState)
   {
      // Do the job
      for (StateListener listener : listeners)
      {
         try
         {
            listener.onState(oldState, newState);
         }
         catch (Exception e)
         {
            // do nothing : listeners should mind their own business
         }
      }
   }

   /**
    * Execute leave method.
    *
    * @param oldState the old state
    */
   private void executeLeaveMethod(S oldState)
   {
      executeStateMethod(oldState, "Leave");
   }

   /**
    * Execute enter method.
    *
    * @param newState the new state
    */
   private void executeEnterMethod(S newState)
   {
      executeStateMethod(newState, "Enter");
   }

   /**
    * Execute state method.
    *
    * @param state the state
    * @param suffix the suffix
    */
   private void executeStateMethod(S state, String suffix)
   {
      try
      {
         String stateName = (state == null) ? "AnyState" : state.toString();
         Method method = findMethod("on" + stateName + suffix, getClass());
         if (method != null)
         {
            method.setAccessible(true);
            method.invoke(this);
         }
      }
      catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
      {
         // do nothing : it is possible that the method does not exist.
      }
      
   }
   
   /**
    * Find method.
    *
    * @param name the name
    * @param cl the cl
    * @return the method
    */
   private Method findMethod(String name, Class<?> cl)
   {
      try
      {
         Method method = cl.getDeclaredMethod(name);
         if ((method == null) && (cl != Object.class))
         {
            method = findMethod(name, cl.getSuperclass());
         }
         return method;
      }
      catch (NoSuchMethodException | SecurityException | IllegalArgumentException e)
      {
         // do nothing : it is possible that the method does not exist.
      }

      return null;
   }
   
   /**
    * Sets the mode.
    *
    * @param mode the new mode
    * @return true, if successful
    */
   public boolean setMode(Mode mode)
   {
      this.mode = mode;
      return true;
   }

   /**
    * Gets the state.
    *
    * @return the state
    */
   public S getState()
   {
      return this.state.get();
   }
   
   /**
    * Link to state.
    *
    * @param linkedProperty the property
    */
   public void linkToState(Property<S> linkedProperty)
   {
      linkedProperty.link(state);
   }
   
   /**
    * Allow all changes.
    *
    * @return true, if successful
    */
   public boolean allowAllChanges()
   {
      return allowChange(null, null);
   }
   
   /**
    * Allow all changes from a state.
    *
    * @param oldState the old state
    * @return true, if successful
    */
   public boolean allowAllChangesFrom(S oldState)
   {
      return allowChange(oldState, null);
   }
   
   /**
    * Allow all changes to a state.
    *
    * @param newState the new state
    * @return true, if successful
    */
   public boolean allowAllChangesTo(S newState)
   {
      return allowChange(null, newState);
   }
   
   /**
    * Allow change.
    *
    * @param oldState the old state
    * @param newState the new state
    * @return true, if successful
    */
   public boolean allowChange(S oldState, S newState)
   {
      boolean ret = true;
      if (((newState != null) && (!states.contains(newState))) || ((oldState != null) && (!states.contains(oldState))))
      {
         ret = false;
      }
      else if ((oldState == null) && (newState == null))
      {
         internAllowAllChanges();
      }
      else if (oldState == null)
      {
         internAllowEnterChanges(newState);
      }
      else if (newState == null)
      {
         internAllowLeaveChanges(oldState);
      }
      else
      {
         internAllowSpecificChanges(oldState, newState);
      }
      return ret;
   }
   
   /**
    * Intern allow all changes.
    */
   private void internAllowAllChanges()
   {
      allChangesAllowed = true;
   }
   
   /**
    * Intern allow enter changes.
    *
    * @param newState the new state
    */
   private void internAllowEnterChanges(S newState)
   {
      if (toChangesList == null)
      {
         toChangesList = new LinkedList<>();
      }
      toChangesList.add(newState);
   }
   
   /**
    * Intern allow leave changes.
    *
    * @param oldState the old state
    */
   private void internAllowLeaveChanges(S oldState)
   {
      if (fromChangesList == null)
      {
         fromChangesList = new LinkedList<>();
      }
      fromChangesList.add(oldState);
   }

   /**
    * Intern allow specific changes.
    *
    * @param oldState the old state
    * @param newState the new state
    */
   private void internAllowSpecificChanges(S oldState, S newState)
   {
      if (identifiedChangesMap == null)
      {
         identifiedChangesMap = new HashMap<>();
      }
      List<S> allowed = identifiedChangesMap.get(oldState);
      if (allowed == null)
      {
         allowed = new LinkedList<>();
         identifiedChangesMap.put(oldState, allowed);
      }
      if (!allowed.contains(newState))
      {
         allowed.add(newState);
      }
   }
   
   /**
    * Adds the listener.
    *
    * @param listener the listener
    * @return true, if successful
    */
   public boolean addListener(StateListener listener)
   {
      return addListeners(listener);
   }
   
   /**
    * Adds the listeners.
    *
    * @param listeners the listeners
    * @return true, if successful
    */
   public boolean addListeners(StateListener... listeners)
   {
      return addListeners(null, null, listeners);
   }
   
   /**
    * Adds the enter listener.
    *
    * @param newState the new state
    * @param listener the listener
    * @return true, if successful
    */
   public boolean addEnterListener(S newState, StateListener listener)
   {
      return addEnterListeners(newState, listener);
   }
   
   /**
    * Adds the enter listeners.
    *
    * @param newState the new state
    * @param listeners the listeners
    * @return true, if successful
    */
   public boolean addEnterListeners(S newState, StateListener... listeners)
   {
      return addListeners(null, newState, listeners);
   }
   
   /**
    * Adds the leave listener.
    *
    * @param oldState the old state
    * @param listener the listener
    * @return true, if successful
    */
   public boolean addLeaveListener(S oldState, StateListener listener)
   {
      addLeaveListeners(oldState, listener);
      return true;
   }
   
   /**
    * Adds the leave listeners.
    *
    * @param oldState the old state
    * @param listeners the listeners
    * @return true, if successful
    */
   public boolean addLeaveListeners(S oldState, StateListener... listeners)
   {
      return addListeners(oldState, null, listeners);
   }
   
   /**
    * Adds the listener.
    *
    * @param oldState the old state
    * @param newState the new state
    * @param listener the listener
    * @return true, if successful
    */
   public boolean addListener(S oldState, S newState, StateListener listener)
   {
      return addListeners( oldState, newState, listener);
   }
   
   /**
    * Adds the listeners.
    *
    * @param oldState the old state
    * @param newState the new state
    * @param listeners the listeners
    * @return true, if successful
    */
   public boolean addListeners(S oldState, S newState, StateListener... listeners)
   {
      if (listeners == null)
      {
         return false;
      }
      
      boolean ret = true;
      for (StateListener listener : listeners)
      {
         ret &= internAddListener(oldState, newState, listener);
      }
      return ret;
   }
   
   /**
    * Intern add listener.
    *
    * @param oldState the old state
    * @param newState the new state
    * @param listener the listener
    * @return true, if successful
    */
   private boolean internAddListener(S oldState, S newState, StateListener listener)
   {
      if ((oldState == null) && (newState == null))
      {
         globalListeners.add(listener);
      }
      else if (oldState == null)
      {
         internAddEnterListener(newState, listener);
      }
      else if (newState == null)
      {
         internAddLeaveListener(oldState, listener);
      }
      else
      {
         internAddSpecificListener(oldState, newState, listener);
      }
      return true;
   }
   
   /**
    * Intern add leave listener.
    *
    * @param oldState the old state
    * @param listener the listener
    * @return true, if successful
    */
   private boolean internAddLeaveListener(S oldState, StateListener listener)
   {
      if (states.contains(oldState))
      {
         internAllowLeaveChanges(oldState);
         List<StateListener> existing = fromListeners.get(oldState);
         if (existing == null)
         {
            existing = new LinkedList<>();
            fromListeners.put(oldState, existing);
         }
         existing.add(listener);
         return true;
      }
      return false;
   }
   
   /**
    * Intern add enter listener.
    *
    * @param newState the new state
    * @param listener the listener
    * @return true, if successful
    */
   private boolean internAddEnterListener(S newState, StateListener listener)
   {
      if (states.contains(newState))
      {
         internAllowEnterChanges(newState);
         List<StateListener> existing = toListeners.get(newState);
         if (existing == null)
         {
            existing = new LinkedList<>();
            toListeners.put(newState, existing);
         }
         existing.add(listener);
         return true;
      }
      return false;
   }

   /**
    * Intern add specific listener.
    *
    * @param oldState the old state
    * @param newState the new state
    * @param listener the listener
    * @return true, if successful
    */
   private boolean internAddSpecificListener(S oldState, S newState, StateListener listener)
   {
      if ((!states.contains(newState)) || (!states.contains(oldState)))
      {
         return false;
      }

      internAllowSpecificChanges(oldState, newState);
      
      Map<S, List<StateListener>> oldStateMap = identifiedListeners.get(oldState);
      if (oldStateMap == null)
      {
         oldStateMap = new HashMap<>();
         identifiedListeners.put(oldState, oldStateMap);
      }
      List<StateListener> existing = oldStateMap.get(newState);
      if (existing == null)
      {
         existing = new LinkedList<>();
         oldStateMap.put(newState, existing);
      }
      if (!existing.contains(listener))
      {
         existing.add(listener);
      }
      return true;
   }

   /**
    * Creates a state reactor.
    *
    * @param state the state
    * @param timeout the timeout
    * @param runnable the runnable
    * @return true, if successful
    */
   public boolean createStateReactor(S state, long timeout, Runnable runnable)
   {
      if (!states.contains(state))
      {
         return false;
      }
      if ((state != null) && (timeout > 0) && (runnable != null))
      {
         StateReactor.createStateReactor(this, state, timeout, runnable);
      }
      return true;
   }
   
   /**
    * Run action.
    *
    * @param name the name
    */
   void runAction(String name)
   {
      Method method = findMethod(name, this.getClass());
      if (method == null)
      {
         StateMachineActions.run(name);
      }
      else
      {
         try
         {
            method.invoke(this);
         }
         catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
         {
            // method may fail...
         }
      }
   }
}
