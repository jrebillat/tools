package net.alantea.tools.steam;

/**
 * The listener interface for receiving state events.
 * The class that is interested in processing a state
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addStateListener</code> method. When
 * the state event occurs, that object's appropriate
 * method is invoked.
 *
 */
@FunctionalInterface
public interface StateListener
{
   
   /**
    * On state.
    *
    * @param oldState the old state
    * @param newState the new state
    */
   public void onState(Object oldState, Object newState);
}
