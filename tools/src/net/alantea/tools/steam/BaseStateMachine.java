package net.alantea.tools.steam;

/**
 * The Class StringStateMachine is a StateMachine with States based on Objects, thus anything.
 */
public class BaseStateMachine extends StateMachine<Object>
{
   /**
    * Instantiates a new base state machine.
    *
    * @param startingState the starting state
    */
   public BaseStateMachine(Object startingState)
   {
      super(startingState);
   }
   
   /**
    * Instantiates a new base state machine.
    *
    * @param machineReference the machine reference
    * @param startingState the starting state
    */
   public BaseStateMachine(String machineReference, Object startingState)
   {
      super(machineReference, startingState);
   }
}
