package net.alantea.tools.steam;

import java.util.List;

import net.alantea.tools.lang.old.LangReader;
import net.alantea.tools.lang.old.external.ErrorType;
import net.alantea.tools.lang.old.external.Manager;
import net.alantea.tools.lang.old.external.ManagerError;
import net.alantea.utils.exception.LntException;

/**
 * The Class StateMachineReader.
 *
 * @param <S> the generic type
 */
public class StateMachineReader<S> extends LangReader
{
   
   /** The Constant CONTEXT_IN_STATE. */
   private static final String CONTEXT_IN_STATE = "ContextInState";
   
   /** The state. */
   private S state = null;

   /** The machine. */
   private StateMachine<S> machine;
   
   /**
    * Instantiates a new state machine reader.
    *
    * @param machine the machine
    */
   public StateMachineReader(StateMachine<S> machine)
   {
      super();
      this.machine = machine;
      
      addState(CONTEXT_IN_STATE);

      addCommand("in state", 1, this::manageState);
      addCommand("on enter execute", 1, this::manageOnEnterDoAction);
      addCommand("on enter", 2, this::manageOnEnterFromExecute, "from", "execute");
      addCommand("on leave", 2, this::manageOnLeaveChange, "change", "to");
      addCommand("allow enter from", 1, this::manageAllowEnter);
      addCommand("allow leave to", 1, this::manageAllowLeave);
   }

   /**
    * Manage state.
    *
    * @param manager the manager
    * @param strings the strings
    * @return the manager error
    * @throws LntException the lnt exception
    */
   private ManagerError manageState(Manager manager, List<Object> strings) throws LntException
   {
      unsetContext();
      state = machine.identifyState((String)strings.get(0));
      if (state == null)
      {
         return new ManagerError(ErrorType.BADARGUMENTERROR, "not a known state : " + strings.get(0));
      }
      
      this.setContext(CONTEXT_IN_STATE);
      return null;
   }

   /**
    * Manage on enter do action.
    *
    * @param manager the manager
    * @param tokens the tokens
    * @return the manager error
    * @throws LntException the lnt exception
    */
   private ManagerError manageOnEnterDoAction(Manager manager, List<Object> tokens) throws LntException
   {
      if (!getState().equals(CONTEXT_IN_STATE))
      {
         return new ManagerError(ErrorType.BADSTATEERROR, "not in a state description");
      }
      machine.addEnterListener(state, (o, n) -> machine.runAction((String)tokens.get(0)));
      return null;
   }

   /**
    * Manage allow enter.
    *
    * @param manager the manager
    * @param tokens the tokens
    * @return the manager error
    * @throws LntException the lnt exception
    */
   private ManagerError manageAllowEnter(Manager manager, List<Object> tokens) throws LntException
   {
      if (!getState().equals(CONTEXT_IN_STATE))
      {
         return new ManagerError(ErrorType.BADSTATEERROR, "not in a state description");
      }
      
      if (((String)(tokens.get(0))).equalsIgnoreCase("ALL"))
      {
         machine.allowAllChangesFrom(state);
      }
      else
      {
         S origin = machine.identifyState((String)tokens.get(0));
         if (origin != null)
         {
            machine.allowChange(origin, state);
         }
      }
      return null;
   }

   /**
    * Manage allow leave.
    *
    * @param manager the manager
    * @param tokens the tokens
    * @return the manager error
    * @throws LntException the lnt exception
    */
   private ManagerError manageAllowLeave(Manager manager, List<Object> tokens) throws LntException
   {
      if (!getState().equals(CONTEXT_IN_STATE))
      {
         return new ManagerError(ErrorType.BADSTATEERROR, "not in a state description");
      }
      
      if (((String)(tokens.get(0))).equalsIgnoreCase("ALL"))
      {
         machine.allowAllChangesTo(state);
      }
      else
      {
         S destination = machine.identifyState((String)tokens.get(0));
         if (destination != null)
         {
            machine.allowChange(state, destination);
         }
      }
      return null;
   }

   /**
    * Manage on leave change.
    *
    * @param manager the manager
    * @param tokens the tokens
    * @return the manager error
    * @throws LntException the lnt exception
    */
   private ManagerError manageOnLeaveChange(Manager manager, List<Object> tokens) throws LntException
   {
      if (!getState().equals(CONTEXT_IN_STATE))
      {
         return new ManagerError(ErrorType.BADSTATEERROR, "not in a state description");
      }
      
      StateMachine<?> otherMachine = StateMachine.getMachine((String)tokens.get(0));
      if (otherMachine == null)
      {
         return new ManagerError(ErrorType.BADARGUMENTERROR, "not a know state machine : " + tokens.get(0));
      }
      
      machine.addLeaveListener(state, (o, n) -> otherMachine.setStateReference((String)tokens.get(1)));
      return null;
   }

   /**
    * Manage on enter from execute.
    *
    * @param manager the manager
    * @param tokens the tokens
    * @return the manager error
    * @throws LntException the lnt exception
    */
   private ManagerError manageOnEnterFromExecute(Manager manager, List<Object> tokens) throws LntException
   {
      if (!getState().equals(CONTEXT_IN_STATE))
      {
         return new ManagerError(ErrorType.BADSTATEERROR, "not in a state description");
      }
      
      S origin = machine.identifyState((String)tokens.get(0));
      if (origin == null)
      {
         return new ManagerError(ErrorType.BADARGUMENTERROR, "not a know state : " + tokens.get(0));
      }

      machine.addListener(origin, state, (o, n) -> machine.runAction((String)tokens.get(0)));
      return null;
   }
}
