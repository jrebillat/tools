package net.alantea.tools.steam;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * The Class EventKey.
 */
public class EventKey
{
   
   /** The machines map. */
   private static Map<UUID, List<EventStateMachine<?>>> machinesMap = new HashMap<>();
   
   /** The machines list. */
   private List<EventStateMachine<?>> machinesList = new LinkedList<>();
   
   /** The key. */
   private UUID key;
   
   /**
    * Instantiates a new event key.
    */
   public EventKey()
   {
      key = UUID.randomUUID();
      machinesMap.put(key,  machinesList);
   }
   
   /**
    * Equals.
    *
    * @param other the other
    * @return true, if successful
    */
   public boolean equals(Object other)
   {
      return ((other instanceof EventKey) && (((EventKey)other).key.equals(key)));
   }
   
   /**
    * Adds the machine.
    *
    * @param machine the machine
    */
   void addMachine(EventStateMachine<?> machine)
   {
      if (! machinesList.contains(machine))
      {
         machinesList.add(machine);
      }
   }
   
   /**
    * Trigger.
    *
    * @param event the event
    */
   public void trigger(Event event)
   {
//      List<EventStateMachine<?>> machines = machinesMap.get(event.getKey());
//      for (EventStateMachine<?> machine : machines)
//      {
//         machine.trigger(event);
//      }
      for (EventStateMachine<?> machine : machinesList)
      {
         machine.trigger(event);
      }
   }
}
