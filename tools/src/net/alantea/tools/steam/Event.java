package net.alantea.tools.steam;

/**
 * The Class Event.
 */
public class Event
{
   
   /** The key. */
   private EventKey key;
   
   /** The data. */
   private Object data;

   /**
    * Instantiates a new event.
    *
    * @param type the type
    */
   public Event(EventKey type)
   {
      key = type;
      data = null;
   }

   /**
    * Instantiates a new event.
    *
    * @param type the type
    * @param userData the user data
    */
   public Event(EventKey type, Object userData)
   {
      key = type;
      data = userData;
   }
   
   /**
    * Gets the user data.
    *
    * @return the user data
    */
   public Object getUserData()
   {
      return data;
   }

   /**
    * Gets the key.
    *
    * @return the key
    */
   public Object getKey()
   {
      return key;
   }
   
   /**
    * Trigger.
    */
   public void trigger()
   {
      key.trigger(this);
   }
}
