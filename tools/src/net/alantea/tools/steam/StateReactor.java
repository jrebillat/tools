package net.alantea.tools.steam;

/**
 * The Class StateReactor to add a running thread while a StateMachine is in a specified state.
 */
class StateReactor
{
   
   /** The do run flag. */
   private boolean doRun;
   
   /** The timeout in milliseconds. */
   private long timeout;
   
   /** The runnable. */
   private Runnable runnable;

   /**
    * Instantiates a new state reactor.
    *
    * @param <S> the generic type
    * @param machine the machine
    * @param state the state to add the reactor to
    * @param timeout the timeout in milliseconds
    * @param runnable the runnable to run every timeout milliseconds
    */
   private <S> StateReactor(StateMachine<S> machine, S state, long timeout, Runnable runnable)
   {
      this.runnable = runnable;
      this.timeout = Math.max(100, timeout);
      this.doRun = false;
      machine.addEnterListener(state, (o,n) -> new ReactorThread().start());
      machine.addLeaveListener(state, (o,n) -> doRun = false);
   }
   
   /**
    * Creates a state reactor.
    *
    * @param <S> the generic type
    * @param machine the machine
    * @param state the state
    * @param timeout the timeout
    * @param runnable the runnable
    */
   public static <S> void createStateReactor(StateMachine<S> machine, S state, long timeout, Runnable runnable)
   {
      if ((machine != null) && (state != null) && (runnable != null))
      {
         new StateReactor(machine, state, timeout, runnable);
      }
   }
   
   /**
    * The Class ReactorThread.
    */
   private class ReactorThread extends Thread
   {
      /**
       * Run.
       */
      public void run()
      {
         doRun = true;
         while (doRun)
         {
            try
            {
               new Thread() {
                  public void run()
                  {
                     runnable.run();
                  }
               }.start();
               
               Thread.sleep(timeout);
            }
            catch (InterruptedException e)
            {
            }
         }
      }
   }
}
