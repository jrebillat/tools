package net.alantea.tools.steam;

import net.alantea.liteprops.Property;

/**
 * The Class Trigger.
 */
public class Trigger
{

   /**
    * Creates the property trigger.
    *
    * @param cause the cause
    * @param action the action
    */
   public static void createPropertyTrigger(Property<?> cause, Runnable action)
   {
      cause.addListener((o, n) ->action.run());
   }

   /**
    * Creates the property trigger.
    *
    * @param <S> the generic type
    * @param cause the cause
    * @param machine the machine
    * @param state the state
    */
   public static <S> void createPropertyTrigger(Property<?> cause, StateMachine<S> machine, S state)
   {
      createPropertyTrigger(cause, () -> machine.setState(state));
   }

   /**
    * Creates the machine trigger.
    *
    * @param <S> the generic type
    * @param machine the machine
    * @param oldState the old state
    * @param newState the new state
    * @param action the action
    */
   public static <S> void createMachineTrigger(StateMachine<S> machine, S oldState, S newState, Runnable action)
   {
      machine.addListener(oldState, newState, (o, n) -> action.run());
   }

   /**
    * Creates the machine trigger.
    *
    * @param <P> the generic type
    * @param <S> the generic type
    * @param machine the machine
    * @param oldState the old state
    * @param newState the new state
    * @param property the property
    * @param value the value
    */
   public static <P,S> void createMachineTrigger(StateMachine<S> machine, S oldState, S newState, Property<P> property, P value)
   {
      machine.addListener(oldState, newState, (o, n) -> property.set(value));
   }

   /**
    * Creates the machine from trigger.
    *
    * @param <P> the generic type
    * @param <S> the generic type
    * @param machine the machine
    * @param oldState the old state
    * @param property the property
    * @param value the value
    */
   public static <P,S> void createMachineFromTrigger(StateMachine<S> machine, S oldState, Property<P> property, P value)
   {
      machine.addLeaveListener(oldState, (o, n) -> property.set(value));
   }

   /**
    * Creates the machine to trigger.
    *
    * @param <P> the generic type
    * @param <S> the generic type
    * @param machine the machine
    * @param newState the new state
    * @param property the property
    * @param value the value
    */
   public static <P,S> void createMachineToTrigger(StateMachine<S> machine, S newState, Property<P> property, P value)
   {
      machine.addEnterListener(newState, (o, n) -> property.set(value));
   }
}
