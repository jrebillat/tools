package net.alantea.tools.steam;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class EventStateMachine is a StateMachine with States based on Events.
 *
 * @param <S> the generic type
 */
public class EventStateMachine<S> extends StateMachine<S>
{
   /** The events map. */
   private Map<EventKey, Map<S, Target<S>>> eventsMap = new HashMap<>();
   
   /**
    * Instantiates a new base state machine.
    *
    * @param startingState the starting state
    */
   public EventStateMachine(S startingState)
   {
      super(startingState);
   }
   
   /**
    * Instantiates a new base state machine.
    *
    * @param machineReference the machine reference
    * @param startingState the starting state
    */
   public EventStateMachine(String machineReference, S startingState)
   {
      super(machineReference, startingState);
   }
   
   /**
    * Adds the transition.
    *
    * @param initial the initial
    * @param targeted the targeted
    * @param event the event
    * @param actions the actions
    * @return the state machine
    */
   @SuppressWarnings("unchecked")
   @SafeVarargs
   public final EventStateMachine<S> addTransition(S initial, S targeted, EventKey event, EventStateMachineListener<S>... actions)
   {
      if ((initial == null) || (targeted == null) || (event == null))
      {
         return this;
      }
      
      this.addStates(initial, targeted);
      event.addMachine(this);
      
      Map<S, Target<S>> eventMap = eventsMap.get(event);
      if (eventMap == null)
      {
         eventMap = new HashMap<>();
         eventsMap.put(event, eventMap);
      }
            
      Target<S> target = new Target<>();
      target.target = targeted;
      target.actions = Arrays.asList(actions);
      eventMap.put(initial, target);
      return this;
   }

   /**
    * Trigger.
    *
    * @param event the event
    */
   public void trigger(Event event)
   {
      Map<S, Target<S>> eventMap = eventsMap.get(event.getKey());
      if ((eventMap != null) && (getState() != null))
      {
         Target<S> target = eventMap.get(getState());
         if (target != null)
         {
            for (EventStateMachineListener<S> action : target.actions)
            {
               try
               {
                  action.onEvent(getState(), target.target, event);
               }
               catch (Exception exception)
               {
                  // Action may fail : system failure is not an option !
               }
            }
            setState(target.target);
         }
      }
   }
   
   /**
    * The Class Target.
    *
    * @param <S> the generic type
    */
   private static class Target<S>
   {
      /** The target. */
      private S target;
      
      /** The actions. */
      private List<EventStateMachineListener<S>> actions;
   }
}
