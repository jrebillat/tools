package net.alantea.tools.xml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import net.alantea.utils.exception.LntException;

/**
 * XML Parser for documentation files.
 *
 */
public abstract class XMLParser extends DefaultHandler
{
   
   /** The document object. */
   private Object documentObject;
   
   /** The characters lifo. */
   private Stack<String> charactersLifo = new Stack<>();
   
   /** The current text. */
   private String currentText = "";

   /** The line locator. */
   private Locator _locator;
   
   /** The compact characters. */
   private boolean compactCharacters = true;
   
   /**
    * load a file.
    *
    * @param file path for file
    * @return the default handler
    */
   public Object load(File file)
   {
      try
      {
         return load(new FileInputStream(file));
      }
      catch (FileNotFoundException e)
      {
         new LntException("unable to parse XML file " + file.getPath(), e);
      }
      return null;
   }
   
   /**
    * Compact chars.
    *
    * @param flag the flag
    */
   public void compactChars(boolean flag)
   {
      compactCharacters = flag;
   }
   
   /**
    * load a string.
    *
    * @param content the content
    * @return the default handler
    */
   public Object load(String content)
   {
      return load(new ByteArrayInputStream(content.getBytes()));
   }
   
   /**
    * load a file.
    *
    * @param file path for file
    * @return the default handler
    */
   public Object load(InputStream file)
   {
      charactersLifo.clear();
      charactersLifo.push("");
      
      try
      {
         SAXParserFactory spfac = SAXParserFactory.newInstance();

         // Now use the parser factory to create a SAXParser object
         SAXParser sp = spfac.newSAXParser();

         // Finally, tell the parser to parse the input and notify the handler
         sp.parse(file, this);
      }
      catch (SAXException saxE)
      {
         new LntException("unable to parse as SAX stream", saxE);
      }
      catch (ParserConfigurationException e)
      {
         new LntException("unable to configure SAX stream", e);
      }
      catch (IOException e)
      {
         new LntException("unable to access stream", e);
      }
      
      return documentObject;
   }
   
   /**
    * Sets the document locator.
    *
    * @param locator the new document locator
    */
   /* (non-Javadoc)
    * @see org.xml.sax.helpers.DefaultHandler#setDocumentLocator(org.xml.sax.Locator)
    */
   @Override
   public final void setDocumentLocator(final Locator locator)
   {
      _locator = locator;
   }
   
   /**
    * Gets the line number.
    *
    * @return the line number
    */
   protected final int getLineNumber()
   {
      return _locator.getLineNumber();
   }
   
   /**
    * Gets the column number.
    *
    * @return the column number
    */
   protected final int getColumnNumber()
   {
      return _locator.getColumnNumber();
   }

   /**
    * Every time the parser encounters the beginning of a new element, it calls this method.
    *
    * @param uri the uri
    * @param localName the local name
    * @param qName the q name
    * @param attributes the attributes
    * @throws SAXException the SAX exception
    */
   @Override
   public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
   {
      charactersLifo.push(currentText);
      currentText = "";
      
      Method method = getMethodFor(qName, "Start", this.getClass(), Attributes.class);
      if (method == null)
      {
         method = getMethodFor("catchAll", "Start", this.getClass(), Attributes.class);
      }
      if (method != null)
      {
         try
         {
            method.setAccessible(true);
            method.invoke(this, attributes);
         }
         catch (IllegalAccessException | IllegalArgumentException e)
         {
         }
         catch (InvocationTargetException e)
         {
            throw new SAXException(e.getMessage(), e);
         }
      }
   }

   /**
    * Every time the parser encounters the end of an element, it calls this method.
    *
    * @param uri the uri
    * @param localName the local name
    * @param qName the q name
    * @throws SAXException the SAX exception
    */
   @Override
   public void endElement(String uri, String localName, String qName) throws SAXException
   {
      Method method = getMethodFor(qName, "End", this.getClass());
      if (method == null)
      {
         method = getMethodFor("catchAll", "End", this.getClass());
      }
      if (method != null)
      {
         try
         {
            method.setAccessible(true);
            method.invoke(this);
         }
         catch (IllegalAccessException | IllegalArgumentException e)
         {
         }
         catch (InvocationTargetException e)
         {
            throw new SAXException(e.getMessage(), e);
         }
      }
      if (!charactersLifo.isEmpty())
      {
         currentText = charactersLifo.pop();
      }
   }
   
   /**
    * Gets the current text.
    *
    * @return the current text
    */
   protected String getCurrentText()
   {
      String txt = currentText.trim();
      txt = txt.replaceAll("[\n\r]$", "");
      return txt;
   }
   
   /**
    * Gets the method for.
    *
    * @param qName the q name
    * @param suffix the suffix
    * @param cl the cl
    * @param args the args
    * @return the method for
    * @throws SAXException the SAX exception
    */
   private Method getMethodFor(String qName, String suffix, Class<?> cl, Class<?>... args) throws SAXException
   {
      Method method = null;
      try
      {
         method = cl.getDeclaredMethod(qName.replaceAll(":",  "_") + suffix, args);
      }
      catch (NoSuchMethodException | SecurityException e)
      {
         if (!Object.class.equals(cl))
         {
            method = getMethodFor(qName, suffix, cl.getSuperclass(), args);
         }
      }
      return method;
   }
   
   /**
    * Characters.
    *
    * @param chars the chars
    * @param start the start
    * @param length the length
    * @throws SAXException the SAX exception
    */
   /* (non-Javadoc)
    * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
    */
   @Override
   public void characters(char[] chars, int start, int length) throws SAXException
   {
      String str = String.copyValueOf(chars).substring(start, start + length);
      if (compactCharacters)
      {
         str = str.replaceAll("\\R", "");
         str = str.replaceAll("\\r", "");
         str = str.replaceAll("\\n", "");
         str = str.trim();
         if (!str.isEmpty())
         {
            currentText += str;
         }
      }
      else
      {
         currentText += str;
      }
   }
   
   /**
    * Sets the document object.
    *
    * @param object the new document object
    */
   protected void setDocumentObject(Object object)
   {
      documentObject = object;
   }
   
   /**
    * Gets the document object.
    *
    * @return the document object
    */
   protected Object getDocumentObject()
   {
      return documentObject;
   }

   /**
    * Get date value from a string.
    * @param value  to parse
    * @return the date
    */
   protected Date getDateValue(String value)
   {
      Date ret = new Date(0L);
      if (value == null)
      {
         return ret;
      }
      else if (value.matches("^[0-9]+$"))
      {
         long timestamp = Long.parseLong(value);
         ret = new java.util.Date(timestamp * 1000L);
      }
      else if (value.matches("^[0-9][0-9]\\/[0-9][0-9]\\/[0-9][0-9][0-9][0-9]$"))
      {
         SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
         try
         {
            ret = format.parse(value);
         }
         catch (ParseException e)
         {
            new LntException("unable to parse date '" + value + "'", e);
         }
      }
      else if (value.matches("^[0-9][0-9]\\/[0-9][0-9]\\/[0-9][0-9][0-9][0-9]\\s[0-9]+:[0-9]+:[0-9]+$"))
      {
         SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
         try
         {
            ret = format.parse(value);
         }
         catch (ParseException e)
         {
            new LntException("unable to parse date and time '" + value + "'", e);
         }
      }
      return ret;
   }

   
   /**
    * Unescape xml.
    *
    * @param s the s
    * @return the string
    */
   public static String unescapeXml(String s)
   {
      return s.replaceAll("&amp;", "&").replaceAll("&gt;", ">").replaceAll("&lt;", "<").replaceAll("&quot;", "\"").replaceAll("&apos;", "'");
   }
}
