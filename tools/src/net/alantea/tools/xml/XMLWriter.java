package net.alantea.tools.xml;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import net.alantea.utils.exception.LntException;

/**
 * Writer for documentation XML files.
 *
 */
public abstract class XMLWriter
{
   
   /** The Constant MODE_TIMESTAMP. */
   public static final int MODE_TIMESTAMP = 1;
   
   /** The Constant MODE_DATESTRING. */
   public static final int MODE_DATESTRING = 2;
   
   /** The Constant MODE_TIMESTRING. */
   public static final int MODE_TIMESTRING = 3;

   /**
    * Write data in a file.
    * @param path to the file
    */
   public void write(String path)
   {
      try
      {
         write(new FileOutputStream(new File(path)));
      }
      catch (FileNotFoundException e)
      {
         new LntException("File not found '" + path + "'", e);
      }
   }
   
   /**
    * Write in a String.
    *
    * @return the string
    */
   public String write()
   {
      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      write(stream);
      return new String(stream.toByteArray());
   }
   
   /**
    * Write data in a stream.
    *
    * @param stream the stream
    */
   public void write(OutputStream stream)
   {
      try
      {      
         DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder docBuilder = null;

         docBuilder = docFactory.newDocumentBuilder();

         // root elements
         Document domDoc = docBuilder.newDocument();
         
         populate(domDoc);
         
         //write the content into xml stream
         TransformerFactory transformerFactory = TransformerFactory.newInstance();
         Transformer transformer = transformerFactory.newTransformer();
         transformer.setOutputProperty(OutputKeys.INDENT, "yes");
         transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
         DOMSource source = new DOMSource(domDoc);

         StreamResult result =  new StreamResult(stream);
         transformer.transform(source, result);
      }
      catch (TransformerException | ParserConfigurationException e)
      {
         new LntException("Could not generate XML file", e);
      }
   }
   
   /**
    * Populate document with elements.
    * @param domDoc document to populate
    */
   protected abstract void populate(Document domDoc);
   
   /**
    * Sets an attribute to element.
    *
    * @param doc the current doc 
    * @param elt the elt to modify
    * @param attr the attr to add
    * @param value the value to set
    */
   protected void setAttribute(Document doc, Element elt, String attr, String value)
   {
      Attr idAtt = doc.createAttribute(attr);
      idAtt.setValue(value);
      elt.setAttributeNode(idAtt);
   }
   
   /**
    * Sets the attribute.
    *
    * @param doc the current doc 
    * @param elt the elt to modify
    * @param attr the attr to add
    * @param value the value to set
    */
   protected void setAttribute(Document doc, Element elt, String attr, BigInteger value)
   {
      Attr idAtt = doc.createAttribute(attr);
      if (value != null)
      {
         idAtt.setValue(value.toString(16));
      }
      else
      {
         idAtt.setValue("");
      }
      elt.setAttributeNode(idAtt);
   }
   
   /**
    * Sets the attribute.
    *
    * @param doc the current doc 
    * @param elt the elt to modify
    * @param attr the attr to add
    * @param value the value to set
    */
   protected void setAttribute(Document doc, Element elt, String attr, boolean value)
   {
      Attr idAtt = doc.createAttribute(attr);
      idAtt.setValue(String.valueOf(value));
      elt.setAttributeNode(idAtt);
   }
   
   /**
    * Sets the attribute.
    *
    * @param doc the current doc 
    * @param elt the elt to modify
    * @param attr the attr to add
    * @param value the value to set
    * @param outmode mode to write information in XML file
    */
   protected void setAttribute(Document doc, Element elt, String attr, Date value, int outmode)
   {
      Attr idAtt = doc.createAttribute(attr);
      if (value != null)
      {
         switch (outmode)
         {
            case MODE_TIMESTAMP :
               long timestamp = value.getTime() / 1000L;
               idAtt.setValue(String.valueOf(timestamp));
               break;
            
            case MODE_DATESTRING :
               SimpleDateFormat dformat = new SimpleDateFormat("dd/MM/yyyy");
               idAtt.setValue(dformat.format(value));
               break;
         
            case MODE_TIMESTRING :
               SimpleDateFormat tformat = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
               idAtt.setValue(tformat.format(value));
               break;
            
            default :
               idAtt.setValue(String.valueOf(0L));
         }
      }
      elt.setAttributeNode(idAtt);
   }
   
   /**
    * Escape xml.
    *
    * @param s the s
    * @return the string
    */
   public static String escapeXml(String s)
   {
      return s.replaceAll("&", "&amp;").replaceAll(">", "&gt;").replaceAll("<", "&lt;").replaceAll("\"", "&quot;").replaceAll("'", "&apos;");
   }
}
