package net.alantea.tools.configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import net.alantea.tools.xml.XMLWriter;

/**
 * Writer for documentation XML files.
 *
 */
public final class ConfigurationWriter extends XMLWriter
{
   
   /** The key set. */
   private Set<String> _keySet;
   
   /**
    * Constructor.
    * @param keySet to associate.
    */
   public ConfigurationWriter(Set<String> keySet)
   {
      _keySet = keySet;
   }

   /**
    * Write data in a file.
    * @param path to the file
    */
   @Override
   public void write(String path)
   {
      super.write(path);
   }

   /* (non-Javadoc)
    * @see fr.nexess.rfidinventory.tools.xml.AbstractXMLWriter#populate(org.w3c.dom.Document)
    */
   @Override
   protected void populate(Document domDoc)
   {
      Element configElt = domDoc.createElement("configuration");
      domDoc.appendChild(configElt);
      
      Map<String, Element> map = new HashMap<String, Element>();
      
      for (String key : _keySet)
      {
         Element elt = null;
         String tname = key;
         if (!key.contains(":"))
         {
            elt = domDoc.createElement(key);
            setAttribute(domDoc, elt, "value", Configuration.getConfigurationValue(key));
            map.put(tname, elt);
            configElt.appendChild(elt);
         }
         else
         {
            String name = key.substring(0,  key.indexOf(":"));
            String attr = key.substring(name.length() + 1);
            elt = map.get(name);

            if (elt == null)
            {
               elt = domDoc.createElement(name);
               map.put(name, elt);
               configElt.appendChild(elt);
            }
            setAttribute(domDoc, elt, attr, Configuration.getConfigurationValue(key));
         }
      }
   }
}
