package net.alantea.tools.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.alantea.liteprops.BooleanProperty;
import net.alantea.liteprops.DoubleProperty;
import net.alantea.liteprops.FloatProperty;
import net.alantea.liteprops.IntegerProperty;
import net.alantea.liteprops.LongProperty;
import net.alantea.liteprops.StringProperty;
import net.alantea.utils.MultiMessages;
import net.alantea.utils.exception.LntException;

/**
 * The Class Configuration.
 */
public class Configuration
{
   /** The _logger. */
   private static Logger logger = LoggerFactory.getLogger(Configuration.class);
   
   /** File path to store the default device name. */
   private static String configurationFile = System.getProperty("user.home") + "/.writekeeper.xml";
   
   /** The map. */
   private static Map<String, StringProperty> map = new HashMap<String, StringProperty>();
   
   /** The loading. */
   private static boolean loading;
   
   /** The loading default. */
   private static boolean loadingDefault;

   /** The recursive. */
   private static boolean recursive;

   private static boolean toBeLoaded = true;
   
   static
   {
      // Add a hook on shutdown to force saving file.
      Runtime.getRuntime().addShutdownHook(new Thread()
      {
         @Override
         public void run()
         {
            save();
         }
      });
   }
   
   /**
    * Instantiates a new configuration.
    */
   private Configuration()
   {
   }

   /**
    * Get configuration from file.
    *
    * @param name the name
    */
   public static void load(String name)
   {
      if (new File(name).exists())
      {
         configurationFile = name;
      }
      else
      {
         configurationFile = System.getProperty("user.home") + "/." + name + ".xml";
      }
      load();
   }

   /**
    * Get configuration from file.
    */
   private static void load()
   {
      try
      {
         load(new FileInputStream(configurationFile));
         toBeLoaded = false;
      }
      catch (FileNotFoundException e)
      {
         if (!loadingDefault)
         {
            loadDefaults();
         }
         else
         {
            new LntException("File not found : " + configurationFile, e);
         }
      }
   }

   /**
    * Get default configuration from file.
    */
   private static void loadDefaults()
   {
      loadingDefault = true;
      InputStream stream = Configuration.class.getResourceAsStream("/configuration/defaults.xml");
      load(stream);
      loadingDefault = false;
   }

   /**
    * Get configuration from file.
    *
    * @param stream the stream
    */
   public static final void load(InputStream stream)
   {
      loading = true;
      if (stream != null)
      {
         new ConfigurationParser().load(stream);
      }
      loading = false;
   }

   /**
    * Save.
    */
   public static void save()
   {
      if ((!loading) && (!toBeLoaded))
      {
         new ConfigurationWriter(map.keySet()).write(configurationFile);
      }
   }

   /**
    * Get some value attribute property.
    * @param key key name to search
    * @param attr key attribute to search
    * @return property corresponding to key.
    */
   public static final StringProperty getConfigurationValueProperty(String key, String attr)
   {
      return getConfigurationValueProperty(key + ":" + attr);
   }

   /**
    * Get some value property.
    * @param key key name to search
    * @return property corresponding to key.
    */
   public static final StringProperty getConfigurationValueProperty(String key)
   {
      if ((toBeLoaded) && (!loading))
      {
         load();
      }
      StringProperty prop = map.get(key);
      if (prop == null)
      {
         prop = new StringProperty("");
         map.put(key,  prop);
      }
      return prop;
   }

   /**
    * Checks for item.
    *
    * @param name the name
    * @return true, if successful
    */
   public static boolean hasItem(String name)
   {
      return getConfigurationValue(name) != null;
   }

   /**
    * Checks for item.
    *
    * @param key the key
    * @param attr the attr
    * @return true, if successful
    */
   public static boolean hasItem(String key, String attr)
   {
      return getConfigurationValue(key, attr) != null;
   }

   /**
    * Get some value attribute.
    * @param key key name to search
    * @param attr key attribute to search
    * @return string corresponding to key.
    */
   public static final String getConfigurationValue(String key, String attr)
   {
      return getConfigurationValue(key + ":" + attr);
   }

   /**
    * Get some value.
    *
    * @param key key name to search
    * @return string corresponding to key.
    */
   public static final String getConfigurationValue(String key) 
   {
      StringProperty prop = getConfigurationValueProperty(key);
      String value = (prop == null) ? null : prop.get();
      if (value == null)
      {
         value = MultiMessages.get("Configuration." + key);
         if (("Configuration." + key).equals(value))
         {
            value = null;
         }
      }
      return (value == null) ? "" : value;
   }

   /**
    * Set some value.
    * @param key key name to search
    * @param attr key attribute to search
    * @param value new value to set.
    */
   public static final void setConfigurationValue(String key, String attr, String value)
   {
      setConfigurationValue(key + ":" + attr, value);
   }

   /**
    * Set some value.
    * @param key key name to search
    * @param value new value to set.
    */
   public static final void setConfigurationValue(String key, String value)
   {
      setConfigurationValue(key, value, true);
   }

   /**
    * Set some value.
    *
    * @param key key name to search
    * @param value new value to set.
    * @param save the save
    */
   public static final void setConfigurationValue(String key, String value, boolean save)
   {
      StringProperty prop = Configuration.getConfigurationValueProperty(key);
      if (prop == null)
      {
         prop = new StringProperty(value);
         map.put(key,  prop);
      }
      else
      {
         prop.set(value);
      }
      if (save)
      {
         save();
      }
   }
   
   /**
    * Associate a double business property to a key.
    * @param property to associate
    * @param key to associate
    */
   public static void associateProperty(DoubleProperty property, String key)
   {
      property.addListener((oldV, newV) ->
      {
            if (!recursive)
            {
               recursive = true;
               Configuration.setConfigurationValue(key, newV.toString());
               recursive = false;
            }
         });
      StringProperty stringProperty = Configuration.getConfigurationValueProperty(key);
      
      stringProperty.addListener((oldV, newV) ->
      {
         property.set(Double.parseDouble(newV));
      });
      property.set(Double.parseDouble(stringProperty.get()));
   }
   
   /**
    * Associate a integer business property to a key.
    * @param property to associate
    * @param key to associate
    */
   public static void associateProperty(IntegerProperty property, String key)
   {
      property.addListener((oldV, newV) ->
      {
            if (!recursive)
            {
               recursive = true;
               Configuration.setConfigurationValue(key, newV.toString());
               recursive = false;
            }
         });
      StringProperty stringProperty = Configuration.getConfigurationValueProperty(key);
      stringProperty.addListener((oldV, newV) ->
      {
         property.set(Integer.parseInt(newV));
      });
      property.set(Integer.parseInt(stringProperty.get()));
   }

   /**
    * Associate a long business property to a key.
    * @param property to associate
    * @param key to associate
    */
   public static void associateProperty(LongProperty property, String key)
   {
      property.addListener((oldV, newV) ->
      {
            if (!recursive)
            {
               recursive = true;
               Configuration.setConfigurationValue(key, newV.toString());
               recursive = false;
            }
         });
      StringProperty stringProperty = Configuration.getConfigurationValueProperty(key);
      
      stringProperty.addListener((oldV, newV) ->
      {
         property.set(Long.parseLong(newV));
      });
      property.set(Long.parseLong(stringProperty.get()));
   }

   /**
    * Associate a float business property to a key.
    *
    * @param property to associate
    * @param key to associate
    */
   public static void associateProperty(FloatProperty property, String key)
   {
      property.addListener((oldV, newV) ->
      {
            if (!recursive)
            {
               recursive = true;
               Configuration.setConfigurationValue(key, newV.toString());
               recursive = false;
            }
         });
      StringProperty stringProperty = Configuration.getConfigurationValueProperty(key);
     
      stringProperty.addListener((oldV, newV) ->
      {
         property.set(Float.parseFloat(newV));
      });
      property.set(Float.parseFloat(stringProperty.get()));
   }

   /**
    * Associate a boolean business property to a key.
    *
    * @param property to associate
    * @param key to associate
    */
   public static void associateProperty(BooleanProperty property, String key)
   {
      property.addListener((oldV, newV) ->
      {
            if (!recursive)
            {
               recursive = true;
               Configuration.setConfigurationValue(key, newV.toString());
               recursive = false;
            }
         });
      StringProperty stringProperty = Configuration.getConfigurationValueProperty(key);
      stringProperty.addListener((oldV, newV) ->
      {
         property.set(Boolean.parseBoolean(newV));
      });
      property.set(Boolean.parseBoolean(stringProperty.get()));
   }
   
   /**
    * Associate a String property to a key.
    * @param property to associate
    * @param key to associate
    */
   public static void associateProperty(StringProperty property, String key)
   {
      property.addListener((oldV, newV) ->
      {
            if (!recursive)
            {
               recursive = true;
               Configuration.setConfigurationValue(key, newV.toString());
               recursive = false;
            }
         });
      StringProperty stringProperty = Configuration.getConfigurationValueProperty(key);
      
      stringProperty.addListener((oldV, newV) ->
      {
         property.set(newV);
      });
      property.set(stringProperty.get());
   }
   
   /**
    * Gets the value.
    *
    * @param key the key
    * @return the value
    */
   private static String getValue(String key)
   {
      String val = MultiMessages.get("Configuration." + key);
      
      StringProperty prop = Configuration.getConfigurationValueProperty(key);
      if (prop != null)
      {
         val = prop.get();
      }
      return val;
   }

   /**
    * Get a configuration value as an integer.
    *
    * @param key to search in properties
    * @param defaultValue the default value
    * @return the int value or 0.
    */
   public static int getInteger(String key, int defaultValue)
   {
      String val = getValue(key);
      int ret = defaultValue;
      try
      {
         ret = Integer.parseInt(val);
      }
      catch (NumberFormatException e)
      {
         if (defaultValue == 0)
         {
            logger.warn("Bad integer found : " + val, e);
         }
      }
      return ret;
   }

   /**
    * Get a configuration value as a double.
    *
    * @param key to search in properties
    * @param defaultValue the default value
    * @return the double value or 0.
    */
   public static double getDouble(String key, double defaultValue)
   {
      String val = getValue(key);
      double ret = defaultValue;
      try
      {
         ret = Double.parseDouble(val);
      }
      catch (NumberFormatException e)
      {
         if (defaultValue == 0)
         {
            logger.warn("Bad double found : " + val, e);
         }
      }
      return ret;
   }

   /**
    * Get a configuration value as a boolean.
    *
    * @param key to search in properties
    * @param defaultValue the default value
    * @return the int value or 0.
    */
   public static boolean getBoolean(String key, boolean defaultValue)
   {
      String val = getValue(key);
      boolean ret = defaultValue;
      try
      {
         ret = Boolean.parseBoolean(val);
      }
      catch (NumberFormatException e)
      {
         logger.warn("Bad boolean found : " + val, e);
      }
      return ret;
   }
}
