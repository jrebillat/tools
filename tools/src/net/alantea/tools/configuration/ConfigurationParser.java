package net.alantea.tools.configuration;

import org.xml.sax.Attributes;

import net.alantea.tools.xml.XMLParser;

/**
 * XML Parser for configuration file.
 *
 */
public class ConfigurationParser extends XMLParser
{

   /**
    * Every time the parser encounters the beginning of a new element, it calls this method, which
    * resets the string buffer.
    *
    * @param uri the uri
    * @param localName the local name
    * @param qName the q name
    * @param attributes the attributes
    */
   @Override
   public void startElement(String uri, String localName, String qName, Attributes attributes)
   {
      if ("configuration".equalsIgnoreCase(qName))
      {
         // nothing to do.
      }
      else
      {
         String tname = qName;
         String value = attributes.getValue("value");
         if (value != null)
         {
            Configuration.setConfigurationValue(tname, value, false);
         }
         for (int i = 0; i < attributes.getLength(); i++)
         {
            String attrName = attributes.getQName(i);
            if ((!("value".equals(attrName))) && !("name".equals(attrName)))
            {
               Configuration.setConfigurationValue(tname + ":" + attrName, attributes.getValue(i), false);
            }
         }
      }
   }
}
