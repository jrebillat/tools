package net.alantea.tools.classes;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

/**
 * The Class ClassLoaderManager.
 */
public class ClassLoaderManager {
	   /**
	    * Instantiates a new plugin manager.
	    */
	   private ClassLoaderManager()
	   {
	   }

	   /**
   	 * Adds the containers.
   	 *
   	 * @param paths the paths
   	 * @return the URL class loader
   	 */
	   public static URLClassLoader createClassLoader(String... paths)
	   {
		   return createClassLoader(ClassLoader.getSystemClassLoader(), paths);
	   }

	   /**
   	 * Adds the containers.
   	 *
   	 * @param parent the parent
   	 * @param paths the paths
   	 * @return the URL class loader
   	 */
	   public static URLClassLoader createClassLoader(ClassLoader parent, String... paths)
	   {
		   List<URL> urlsList = new LinkedList<>();
		   for (String path : paths)
		   {
			   try
			   {
				   File file = new File(path);
				   if (file.exists())
				   {
				      urlsList.add(file.toURI().toURL());
				   }
			   }
			   catch (MalformedURLException e)
			   {
				  e.printStackTrace();
			   }
		   }
		   return createClassLoader(parent, urlsList.toArray(new URL[0]));
	   }

	   /**
   	 * Adds the containers.
   	 *
   	 * @param files the files
   	 * @return the URL class loader
   	 */
	   public static URLClassLoader createClassLoader(File... files)
	   {
		   return createClassLoader(ClassLoader.getSystemClassLoader(), files);
	   }

	   /**
   	 * Adds the containers.
   	 *
   	 * @param parent the parent
   	 * @param files the files
   	 * @return the URL class loader
   	 */
	   public static URLClassLoader createClassLoader(ClassLoader parent, File... files)
	   {
		   List<URL> urlsList = new LinkedList<>();
		   for (File file : files)
		   {
			   try
			   {
				  urlsList.add(file.toURI().toURL());
			   }
			   catch (MalformedURLException e)
			   {
				  e.printStackTrace();
			   }
		   }
		   return createClassLoader(parent, urlsList.toArray(new URL[0]));
	   }

	   /**
   	 * Adds the containers.
   	 *
   	 * @param urls the urls
   	 * @return the URL class loader
   	 */
	   public static URLClassLoader createClassLoader(URL... urls)
	   {
		   return new URLClassLoader(urls);
	   }

	   /**
   	 * Adds the containers.
   	 *
   	 * @param parent the parent
   	 * @param urls the urls
   	 * @return the URL class loader
   	 */
	   public static URLClassLoader createClassLoader(ClassLoader parent, URL... urls)
	   {
	      URLClassLoader loader =  new URLClassLoader(urls, parent);
		   return loader;
	   }

      /**
       * Adds the containers in directory.
       *
       * @param dirs the directories
       * @return the URL class loader
       */
      public static URLClassLoader createClassLoaderFromDirectories(File... dirs)
      {
         return createClassLoaderFromDirectories(ClassLoader.getSystemClassLoader(), true, dirs);
      }

      /**
       * Adds the containers in directory.
       *
       * @param parent the parent
       * @param dirs the directories
       * @return the URL class loader
       */
      public static URLClassLoader createClassLoaderFromDirectories(ClassLoader parent, File... dirs)
      {
         return createClassLoaderFromDirectories(parent, true, dirs);
      }

      /**
       * Adds the containers in directory.
       *
       * @param recurse the recurse
       * @param dirs the directories
       * @return the URL class loader
       */
      public static URLClassLoader createClassLoaderFromDirectories(boolean recurse, File... dirs)
      {
         return createClassLoaderFromDirectories(ClassLoader.getSystemClassLoader(), recurse, dirs);
      }

      /**
       * Adds the containers in directory.
       *
       * @param parent the parent
       * @param recurse the recurse
       * @param dirs the directories
       * @return the URL class loader
       */
      public static URLClassLoader createClassLoaderFromDirectories(ClassLoader parent, boolean recurse, File... dirs)
      {
         List<File> containers = new LinkedList<File>();
         for (File dir : dirs)
         {
            containers.add(dir);
            containers.addAll(computeContainersDirectory(dir, recurse));
         }
         return createClassLoader(parent, containers.toArray(new File[0]));
      }

	   /**
	    * Compute containers in directory.
	    *
	    * @param dir the dir
	    * @param recurse the recurse
	    * @return the list
	    */
	   private static List<File> computeContainersDirectory(File dir, boolean recurse)
	   {
		   List<File> filesList = new LinkedList<>();
		   for (File file : dir.listFiles())
		   {
			   if (file.isDirectory())
			   {
				   if (recurse)
				   {
				      filesList.addAll(computeContainersDirectory(file, true));
				   }
			   }
			   else if (file.getName().endsWith(".jar"))
			   {
			      filesList.add(file);
			   }
		   }
		   return filesList;
	   }

	   /**
   	 * Creates the class from source.
   	 *
   	 * @param rootPath the root path
   	 * @param classPath the class path
   	 * @param source the source
   	 * @return true, if successful
   	 */
   	public static boolean createClassFromSource(String rootPath, String classPath, String source)
	   {
		   try {
			   File root = new File(rootPath);
			   File sourceFile = new File(root, classPath.replaceAll("\\.",  "/") + ".java");
			   sourceFile.getParentFile().mkdirs();
			   Files.write(sourceFile.toPath(), source.getBytes(StandardCharsets.UTF_8));

			   // Compile source file.
			   JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
			   int ret = compiler.run(null, null, null, sourceFile.getPath());

			   return (ret == 0);
		   } catch (IOException e) {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   }
		   return false;
	   }

	   /**
   	 * Compile class source.
   	 *
   	 * @param <T> the generic type
   	 * @param rootPath the root path
   	 * @param classPath the class path
   	 * @param source the source
   	 * @return the class
   	 */
   	public static <T> Class<T> compileClassSource(String rootPath, String classPath, String source)
	   {
		   try {
			   File root = new File(rootPath);
			   boolean ok = createClassFromSource(rootPath, classPath, source);

			   if (ok)
			   {
				   URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] { root.toURI().toURL() });
				   if (classLoader != null)
				   {
					   @SuppressWarnings("unchecked")
					   Class<T> cls = (Class<T>) Class.forName(classPath, true, classLoader); 
					   return cls;
				   }
			   }
		   } catch (IOException | ClassNotFoundException e) {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   }
		   return null;
	   }

	   /**
   	 * Load class source.
   	 *
   	 * @param <T> the generic type
   	 * @param rootPath the root path
   	 * @param classPath the class path
   	 * @param source the source
   	 * @return the t
   	 */
   	@SuppressWarnings("unchecked")
	   public static <T> T loadClassSource(String rootPath, String classPath, String source)
	   {
		   try {
			   Class<T> cls = compileClassSource(rootPath, classPath, source);
			   if (cls != null)
			   {
			      Object instance = cls.newInstance();
			      return (T) instance;
			   }
		   } catch (InstantiationException | IllegalAccessException e) {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   }
		   return null;
	   }
}
