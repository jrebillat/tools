package net.alantea.tools.classes;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;

/**
 * The Class JarPackager.
 */
public final class JarPackager
{
   
   /**
    * Instantiates a new jar packager.
    */
   private JarPackager()
   {
      
   }
   
   /**
    * Package jar file.
    *
    * @param rootDir the root dir
    * @param outPath the out path
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static void packageJarFile(File rootDir, String outPath) throws IOException
   {
      FileOutputStream fout = new FileOutputStream(outPath);
      JarOutputStream jarOut = new JarOutputStream(fout);
      
      addJarDirectoryEntry(jarOut, rootDir, null);
      
      jarOut.putNextEntry(new ZipEntry("META-INF/")); // Folders must end with "/".
      jarOut.putNextEntry(new ZipEntry("META-INF/MANIFEST.MF"));
      jarOut.write("Manifest-Version: 1.0\n".getBytes());
      jarOut.closeEntry();
      
      jarOut.close();
      fout.close();
   }
   
   /**
    * Adds the jar file entry.
    *
    * @param jarOut the jar out
    * @param source the source
    * @param entryPath the entry path
    * @throws IOException Signals that an I/O exception has occurred.
    */
   private static void addJarFileEntry(JarOutputStream jarOut, File source, String entryPath) throws IOException
   {
      JarEntry entry = new JarEntry(entryPath);
      entry.setTime(source.lastModified());
      jarOut.putNextEntry(entry);
      byte[] fileContent = Files.readAllBytes(source.toPath());
      jarOut.write(fileContent);
      jarOut.closeEntry();
   }
   
   /**
    * Adds the jar directory entry.
    *
    * @param jarOut the jar out
    * @param source the source
    * @param parentPath the parent path
    * @throws IOException Signals that an I/O exception has occurred.
    */
   private static void addJarDirectoryEntry(JarOutputStream jarOut, File source, String parentPath) throws IOException
   {
      String path = (parentPath == null) ? "" : (parentPath + source.getName() + "/");
      if (parentPath != null)
      {
         JarEntry entry = new JarEntry(path);
         entry.setTime(source.lastModified());
         jarOut.putNextEntry(entry); // Folders must end with "/".
         jarOut.closeEntry();
      }
      for (File file : source.listFiles())
      {
         if (file.isDirectory())
         {
            addJarDirectoryEntry(jarOut, file, path);
         }
         else
         {
            addJarFileEntry(jarOut, file, path + file.getName());
         }
         
      }
   }
}
