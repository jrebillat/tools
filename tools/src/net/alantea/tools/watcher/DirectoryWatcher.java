package net.alantea.tools.watcher;

import java.io.File;

/**
 * The Class DirectoryWatcher.
 */
public class DirectoryWatcher
{
   
   /** The thread. */
   private DirectoryWatcherThread thread;

   /**
    * Instantiates a new directory watcher.
    *
    * @param path the path
    * @param listener the listener
    * @throws Exception the exception
    */
   public DirectoryWatcher(String path, WatcherListener listener) throws Exception
   {
      this(new File(path), listener);
   }

   /**
    * Instantiates a new directory watcher.
    *
    * @param file the file
    * @param listener the listener
    * @throws Exception the exception
    */
   public DirectoryWatcher(File file, WatcherListener listener) throws Exception
   {
      if (file != null)
      {
         if (!file.exists())
         {
            throw new Exception("Non-existing directory : " + file.getAbsolutePath());
         }
         if (!file.isDirectory())
         {
            throw new Exception("Not a directory" + file.getAbsolutePath());
         }

         thread = new DirectoryWatcherThread(file);

         if (listener != null)
         {
            addListener(listener);
         }
         thread.start();
      }
   }

   /**
    * Adds the listener.
    *
    * @param listener the listener
    */
   public void addListener(WatcherListener listener)
   {
      thread.addListener(listener);
   }
   
   /**
    * Removes the listener.
    *
    * @param listener the listener
    */
   public void removeListener(WatcherListener listener)
   {
      thread.removeListener(listener);
   }
   
   /**
    * Stop.
    */
   public void stop()
   {
      thread.stopThread();
   }
}
