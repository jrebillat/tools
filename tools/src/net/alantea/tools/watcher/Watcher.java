package net.alantea.tools.watcher;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * The Class WatcherThread.
 */
public class Watcher extends Thread
{   
   /** The file. */
   private final File file;
   
   /** The stop. */
   private AtomicBoolean stop = new AtomicBoolean(false);
   
   /** The listeners. */
   private List<WatcherListener> listeners = new LinkedList<>();
   
   /** The directory. */
   private boolean directory;

   /**
    * Instantiates a new watcher thread.
    *
    * @param file the file
    * @throws Exception the exception
    */
   Watcher(File file) throws Exception
   {
      this(file, null);
   }

   /**
    * Instantiates a new watcher.
    *
    * @param file the file
    * @param listener the listener
    */
   public Watcher(File file, WatcherListener listener)
   {
      this.file = file;
      
      if (file != null)
      {
         if (!file.exists())
         {
            return;
         }
      }

      directory = file.isDirectory();

      if (listener != null)
      {
         addListener(listener);
      }
      
      this.setDaemon(true);
      this.start();
   }
   
   /**
    * Watch.
    *
    * @param file the file
    * @return the watcher
    */
   public Watcher watch(File file)
   {
      return watch(file, null);
   }
   
   /**
    * Watch.
    *
    * @param file the file
    * @param listener the listener
    * @return the watcher
    */
   public Watcher watch(File file, WatcherListener listener)
   {
      return new Watcher(file, listener);
   }
   
   /**
    * Adds the listener.
    *
    * @param listener the listener
    */
   public void addListener(WatcherListener listener)
   {
      if (!listeners.contains(listener))
      {
         listeners.add(listener);
      }
   }
   
   /**
    * Removes the listener.
    *
    * @param listener the listener
    */
   public void removeListener(WatcherListener listener)
   {
      if (listeners.contains(listener))
      {
         listeners.remove(listener);
      }
   }

   /**
    * Checks if is stopped.
    *
    * @return true, if is stopped
    */
   public boolean isStopped()
   {
      return stop.get();
   }

   /**
    * Stop thread.
    */
   public void stopThread()
   {
      stop.set(true);
   }
   
   /**
    * Run.
    */
   @Override
   public void run()
   {
      try (WatchService watcherService = FileSystems.getDefault().newWatchService())
      {
         Path path = file.toPath();
         Path path1 = path.getParent();
         if (directory)
         {
         path.register(watcherService,
               StandardWatchEventKinds.ENTRY_CREATE,
               StandardWatchEventKinds.ENTRY_DELETE,
               StandardWatchEventKinds.ENTRY_MODIFY);
         }
         else
         {
            path1.register(watcherService, StandardWatchEventKinds.ENTRY_MODIFY);
         }
         
         while (!isStopped())
         {
            WatchKey key;
            try
            {
               key = watcherService.take();
            }
            catch (InterruptedException e)
            {
               return;
            }
            if (key == null)
            {
               Thread.yield();
               continue;
            }

            for (WatchEvent<?> event : key.pollEvents())
            {
               WatchEvent.Kind<?> kind = event.kind();

               @SuppressWarnings("unchecked")
               WatchEvent<Path> ev = (WatchEvent<Path>) event;
               Path filename = ev.context();

               if (kind == StandardWatchEventKinds.OVERFLOW)
               {
                  Thread.yield();
                  continue;
               }
               else if (filename.toString().equals(file.getName()))
               {
                  WatcherListener.EventType type = null;
                  if (kind == StandardWatchEventKinds.ENTRY_CREATE)
                  {
                     type = WatcherListener.EventType.CREATE;
                  }
                  else if (kind == StandardWatchEventKinds.ENTRY_DELETE)
                  {
                     type = WatcherListener.EventType.DELETE;
                  }
                  else if (kind == StandardWatchEventKinds.ENTRY_MODIFY)
                  {
                     if ((directory) || (filename.toString().equals(file.getName())))
                     {
                        type = WatcherListener.EventType.MODIFY;
                     }  
                  }
                  
                  if (type!= null)
                  {
                     for (WatcherListener listener : listeners)
                     {
                        listener.onEvent(type, file);
                     }
                  }
               }
               boolean valid = key.reset();
               if (!valid)
               {
                  break;
               }
            }
            Thread.yield();
         }
      }
      catch (Throwable e)
      {
         e.printStackTrace();
         // Log or rethrow the error
      }
   }
}
