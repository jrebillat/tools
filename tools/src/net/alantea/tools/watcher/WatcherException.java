/****************************************************************************
 * 
 * Innovation Technologies Support, Services and Solutions
 * 
 * Copyright (c) Airbus Defence and Space 2015
 */
//  
/****************************************************************************************************/
package net.alantea.tools.watcher;

import java.util.LinkedList;
import java.util.List;

/**
 * *********************************.
 */
/**
 * general Exception.
 * @author Alantea
 *
 */
public class WatcherException extends Exception
{
   
   /** The exception list. */
   static List<WatcherException> exceptionList = new LinkedList<>();

   
   /** The Constant serialVersionUID. */
   private static final long serialVersionUID = 1L;

   /**
    * Constructor.
    * @param text to show.
    */
   public WatcherException(String text)
   {
      super(text);
      addToList(this);
   }

   /**
    * Constructor.
    * @param text to show.
    * @param cause of the exception
    */
   public WatcherException(String text, Throwable cause)
   {
      super(text, cause);
      addToList(this);
   }
   
   /**
    * Pop exceptions.
    *
    * @return the list
    */
   public static List<WatcherException> popExceptions()
   {
      List<WatcherException> list = exceptionList;
      exceptionList = new LinkedList<>();
      return list;
   }
   
   /**
    * Adds the to list.
    *
    * @param exp the exp
    */
   private static void addToList(WatcherException exp)
   {
      if (exceptionList.size() >= 10)
      {
         exceptionList.remove(0);
      }
      exceptionList.add(exp);
   }
}
