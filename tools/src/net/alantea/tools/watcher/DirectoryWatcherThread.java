package net.alantea.tools.watcher;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * The Class DirectoryWatcherThread.
 */
class DirectoryWatcherThread extends Thread
{
   
   /** The file. */
   private final File file;
   
   /** The stop. */
   private AtomicBoolean stop = new AtomicBoolean(false);
   
   /** The listeners. */
   List<WatcherListener> listeners = new LinkedList<>();

   /**
    * Instantiates a new directory watcher thread.
    *
    * @param file the file
    */
   DirectoryWatcherThread(File file)
   {
      this.file = file;
   }
   
   /**
    * Adds the listener.
    *
    * @param listener the listener
    */
   void addListener(WatcherListener listener)
   {
      if (!listeners.contains(listener))
      {
         listeners.add(listener);
      }
   }
   
   /**
    * Removes the listener.
    *
    * @param listener the listener
    */
   void removeListener(WatcherListener listener)
   {
      if (listeners.contains(listener))
      {
         listeners.remove(listener);
      }
   }

   /**
    * Checks if is stopped.
    *
    * @return true, if is stopped
    */
   boolean isStopped()
   {
      return stop.get();
   }

   /**
    * Stop thread.
    */
   void stopThread()
   {
      stop.set(true);
   }


   /**
    * Run.
    */
   @Override
   public void run()
   {
      try (WatchService watcher = FileSystems.getDefault().newWatchService())
      {
         Path path = file.toPath().getParent();
         path.register(watcher,
               StandardWatchEventKinds.ENTRY_CREATE,
               StandardWatchEventKinds.ENTRY_DELETE,
               StandardWatchEventKinds.ENTRY_MODIFY);
         while (!isStopped())
         {
            WatchKey key;
            try
            {
               key = watcher.take();
            }
            catch (InterruptedException e)
            {
               return;
            }
            if (key == null)
            {
               Thread.yield();
               continue;
            }

            for (WatchEvent<?> event : key.pollEvents())
            {
               WatchEvent.Kind<?> kind = event.kind();

               @SuppressWarnings("unchecked")
               WatchEvent<Path> ev = (WatchEvent<Path>) event;
               Path filename = ev.context();

               if (kind == StandardWatchEventKinds.OVERFLOW)
               {
                  Thread.yield();
                  continue;
               }
               else if (filename.toString().equals(file.getName()))
               {
                  WatcherListener.EventType type = null;
                  if (kind == StandardWatchEventKinds.ENTRY_CREATE)
                  {
                     type = WatcherListener.EventType.CREATE;
                  }
                  else if (kind == StandardWatchEventKinds.ENTRY_DELETE)
                  {
                     type = WatcherListener.EventType.DELETE;
                  }
                  else if (kind == StandardWatchEventKinds.ENTRY_MODIFY)
                  {
                     type = WatcherListener.EventType.MODIFY;
                  }
                  
                  if (type!= null)
                  {
                     for (WatcherListener listener : listeners)
                     {
                        listener.onEvent(type, file);
                     }
                  }
               }
               boolean valid = key.reset();
               if (!valid)
               {
                  break;
               }
            }
            Thread.yield();
         }
      }
      catch (Throwable e)
      {
         // Log or rethrow the error
      }
   }
}