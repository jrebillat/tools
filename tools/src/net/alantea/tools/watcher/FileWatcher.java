package net.alantea.tools.watcher;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * The Class FileWatcher.
 */
public class FileWatcher extends Thread
{
   
   /** The file. */
   private final File file;
   
   /** The stop. */
   private AtomicBoolean stop = new AtomicBoolean(false);
   
   /** The listeners. */
   List<WatcherListener> listeners = new LinkedList<>();

   /**
    * Instantiates a new file watcher.
    *
    * @param file the file
    * @param listener the listener
    */
   public FileWatcher(File file, WatcherListener listener)
   {
      this.file = file;
      listeners.add(listener);
   }

   /**
    * Checks if is stopped.
    *
    * @return true, if is stopped
    */
   public boolean isStopped() { return stop.get(); }
   
   /**
    * Stop thread.
    */
   public void stopThread() { stop.set(true); }
   
   /**
    * Adds the listener.
    *
    * @param listener the listener
    */
   void addListener(WatcherListener listener)
   {
      if (!listeners.contains(listener))
      {
         listeners.add(listener);
      }
   }
   
   /**
    * Removes the listener.
    *
    * @param listener the listener
    */
   void removeListener(WatcherListener listener)
   {
      if (listeners.contains(listener))
      {
         listeners.remove(listener);
      }
   }

   /**
    * Run.
    */
   @Override
   public void run()
   {
      try (WatchService watcher = FileSystems.getDefault().newWatchService())
      {
         Path path = file.toPath().getParent();
         path.register(watcher, StandardWatchEventKinds.ENTRY_MODIFY);
         while (!isStopped())
         {
            WatchKey key;
            try
            {
               key = watcher.take();
            }
            catch (InterruptedException e)
            {
               return;
            }
            if (key == null)
            {
               Thread.yield();
               continue;
            }

            for (WatchEvent<?> event : key.pollEvents())
            {
               WatchEvent.Kind<?> kind = event.kind();

               @SuppressWarnings("unchecked")
               WatchEvent<Path> ev = (WatchEvent<Path>) event;
               Path filename = ev.context();

               if (kind == StandardWatchEventKinds.OVERFLOW)
               {
                  Thread.yield();
                  continue;
               }
               else if (kind == java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY
                     && filename.toString().equals(file.getName())) {

                  for (WatcherListener listener : listeners)
                  {
                     listener.onEvent(WatcherListener.EventType.MODIFY, file);
                  }
             }
               }
               boolean valid = key.reset();
               if (!valid)
               {
                  break;
               }
            }
            Thread.yield();
         }
      catch (Throwable e)
      {
      }
   }
}