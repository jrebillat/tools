package net.alantea.tools.watcher;

import java.io.File;

/**
 * The listener interface for receiving watcher events.
 * The class that is interested in processing a watcher
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addWatcherListener</code> method. When
 * the watcher event occurs, that object's appropriate
 * method is invoked.
 *
 */
@FunctionalInterface
public interface WatcherListener
{
   
   /**
    * The Enum EventType.
    */
   public enum EventType
   {
      
      /** The create. */  
      CREATE,
      
      /** The delete. */
      DELETE,
      
      /** The modify. */
      MODIFY
   }
   
   /**
    * On event.
    *
    * @param type the type
    * @param file the file
    */
   public void onEvent(EventType type, File file);
}
