package net.alantea.tools.watcher;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class SelectedDirectoryWatcher.
 */
public class SelectedDirectoryWatcher extends DirectoryWatcher
{
   
   /** The listener. */
   private WatcherListener listener;
   
   /** The matches. */
   private List<String> matches = new LinkedList<>();
   
   /**
    * Instantiates a new selected directory watcher.
    *
    * @param directory the directory
    * @param listener the listener
    * @throws Exception the exception
    */
   public SelectedDirectoryWatcher(File directory, WatcherListener listener) throws Exception
   {
      super(directory, null);
      this.addListener(this::onChange);
   }
   
   /**
    * On change.
    *
    * @param type the type
    * @param file the file
    */
   public void onChange(WatcherListener.EventType type, File file)
   {
      String name = file.getName();
      boolean found = false;
      for (String match : matches)
      {
         if (name.matches(match))
         {
            found = true;
            break;
         }
      }
      
      if (found)
      {
         listener.onEvent(type, file);
      }
   }

   /**
    * Adds the regex filter.
    *
    * @param filter the filter
    */
   public void addRegexFilter(String filter)
   {
      matches.add(filter);
   }

   /**
    * Adds the suffix filter.
    *
    * @param filter the filter
    */
   public void addSuffixFilter(String filter)
   {
      matches.add("^.*\\." + filter + "$");
   }

   /**
    * Adds the simplified filter.
    *
    * @param filter the filter
    */
   public void addSimplifiedFilter(String filter)
   {
      String value = filter.replaceAll("\\*", ".*");
      matches.add("^" + value + "$");
   }
}
