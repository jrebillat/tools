package net.alantea.tools.crypto;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * The Class Codec.
 */
public final class Codec
{
   
   /** The Constant AES. */
   public static final String ALGO_AES = "AES";

   /**
    * Instantiates a new codec.
    */
   private Codec()
   {
   }
   
   /**
    * Encrypt a string with AES algorithm.
    *
    * @param data is a string
    * @param key the key
    * @param algorithm the algorithm
    * @return the encrypted string
    * @throws GeneralSecurityException the general security exception
    */
   public static byte[] encrypt(String data, String key, String algorithm) throws GeneralSecurityException
   {
      return encrypt(data.getBytes(), key, algorithm);
   }
   
   /**
    * Encrypt a byte array with AES algorithm.
    *
    * @param data is a string
    * @param key the key
    * @param algorithm the algorithm
    * @return the encrypted string
    * @throws GeneralSecurityException the general security exception
    */
   public static byte[] encrypt(byte[] data, String key, String algorithm) throws GeneralSecurityException
   {
      Key k = generateKey(key, algorithm);
      Cipher c = Cipher.getInstance(algorithm);
      c.init(Cipher.ENCRYPT_MODE, k);
      return Base64.getEncoder().encode(c.doFinal(data));
   }

   /**
    * Decrypt.
    *
    * @param file the file
    * @param key the key
    * @param algorithm the algorithm
    * @return the byte[]
    * @throws GeneralSecurityException the general security exception
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static byte[] decrypt(File file, String key, String algorithm) throws GeneralSecurityException, IOException
   {
      return decrypt(new FileInputStream(file), key, algorithm);
   }

   /**
    * Decrypt.
    *
    * @param stream the stream
    * @param key the key
    * @param algorithm the algorithm
    * @return the byte[]
    * @throws GeneralSecurityException the general security exception
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static byte[] decrypt(InputStream stream, String key, String algorithm) throws GeneralSecurityException, IOException
   {
      DataInputStream dataStream = new DataInputStream(stream);
      byte[] bytes = new byte[dataStream.available()];
      dataStream.readFully(bytes);
      return decrypt(bytes, key, algorithm);
   }
   
   /**
    * Decrypt a string with AES algorithm.
    *
    * @param data is a string
    * @param key the key
    * @param algorithm the algorithm
    * @return the encrypted string
    * @throws GeneralSecurityException the general security exception
    */
   public static byte[] decrypt(String data, String key, String algorithm) throws GeneralSecurityException 
   {
      return decrypt(data.getBytes(), key, algorithm);
   }
   
   /**
    * Decrypt a string with AES algorithm.
    *
    * @param encryptedData is a string
    * @param key the key
    * @param algorithm the algorithm
    * @return the decrypted string
    * @throws GeneralSecurityException the general security exception
    */
   public static byte[] decrypt(byte[] encryptedData, String key, String algorithm) throws GeneralSecurityException
   {
      Key k = generateKey(key, algorithm);
      Cipher c = Cipher.getInstance(algorithm);
      c.init(Cipher.DECRYPT_MODE, k);
      byte[] decordedValue = Base64.getDecoder().decode(encryptedData);
      byte[] decValue = c.doFinal(decordedValue);
      return decValue;
   }

   /**
    * Generate a new encryption key.
    *
    * @param key the key
    * @param algorithm the algorithm
    * @return the key
    */
   private static Key generateKey(String key, String algorithm)
   {
      return new SecretKeySpec(key.getBytes(), algorithm);
   }
}
