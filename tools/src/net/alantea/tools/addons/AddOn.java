package net.alantea.tools.addons;

/**
 * The base Class to integrate an AddOn.
 */
public abstract class AddOn
{
   
   /**
    * Initialize the addOn.
    * Do there whatever should be done to integrate the addOn.
    *
    * @throws AddOnsException the add ons exception
    */
   public abstract void initialize() throws AddOnsException;
}
