package net.alantea.tools.addons;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import net.alantea.tools.zip.ZipInputStreamContainer;

// -----------------------------------------------------
/**
 * The Class Master for AddOns.
 * This class manages all addons (see the AddOn base class for more) in the application; either those present in the application classpath.
 * AddOns are listed in each addon jars in a file named "/src/addon.txt", one class name on each line.
 * Lines beginning by a # character are considered as commented.
 * 
 * For addons included in the master jar or in classpath, the addon files are anywhere in the class path and listed in a "/src/Storybook.txt"
 * file without the ".txt" ending (thus the same pattern as addon.txt files).
 */
public class AddOnManager
{
   /** The already loaded add ons (to avoid double loading). */
   private static List<String> loadedAddOnPaths = new LinkedList<>();

   /** The loaded add ons. */
   private static List<AddOn>addOns = new LinkedList<>();
   
   /**
    * Instantiates a new manager is not authorized.
    */
   protected AddOnManager()
   {
   }

   /**
    * Load addons included in master class loader.
    *
    * @param infoFilePath the info file path
    */
   public static final void loadMasterClassLoaderAddOns(String infoFilePath)
   {
      List<String> jarInfos = new LinkedList<>();
      
      // Open file information path
      InputStream istream = AddOnManager.class.getResourceAsStream(infoFilePath);
      try
      {
         // load addons list file content : these are files where to find addons list
         List<String> addonsTxtFiles = loadStreamInfo(istream);
         
         // For each addons file
         for (String addOn : addonsTxtFiles)
         {
            // Correct file path to resource path name
            String path = addOn;
            if (!path.startsWith("/"))
            {
               // append start slash
               path = "/" + path;
            }
            // Replace dots with slashes
            path = path.replaceAll("\\.", "/");
            // Append file name ending
            path += ".txt";
            
            // Add all addons
            InputStream stream = AddOnManager.class.getResourceAsStream(path);
            jarInfos.addAll(loadStreamInfo(stream));
         }
      }
      catch (UnsupportedEncodingException e)
      {
         new AddOnsException("Bad path : " + infoFilePath);
         return;
      }
      
      // Initialize all found addons
      initializeNewAddOns(jarInfos, AddOnManager.class.getClassLoader(), false);
   }

   /**
    * Load a list of directories that shall contain jar files either inside the directory on in the direct subdirectories.
    * Each directory will correspond to a separate class loader, created just for it.
    *
    * @param directories the directories to search in
    */
   public static final void loadDirectories(String... directories)
   {
      for (String directory : directories)
      {
         // load information from the directory
         File dir = new File(directory);
         if ((dir.exists()) && (dir.isDirectory()))
         {
            loadDirectory(dir);
         }
         else
         {
            new AddOnsException("Unable to find " + directory + " directory").printStackTrace();
         }
      }
   }

   /**
    * Load a list of directories that shall contain jar files.
    *
    * @param dir the dir
    */
   public static final void loadDirectory(File dir)
   {
      // load information from the directory
      loadDirectoryJarsInformation(dir);
      loadSubDirectoriesInformation(dir);
   }

   /**
    * Load sub directories information.
    *
    * @param dir the dir
    */
   private static void loadSubDirectoriesInformation(File dir)
   {
      File[] subdirs = dir.listFiles(new FileFilter()
      {
         @Override
         public boolean accept(File dir)
         {
            return dir.isDirectory();
         }
      });
      for (File subdir : subdirs)
      {
         loadDirectoryJarsInformation(subdir);
      }
   }

   /**
    * Load directory jars information.
    *
    * @param dir the dir
    */
   private static final void loadDirectoryJarsInformation(File dir)
   {
         // List jar files in directory
         File[] jars = dir.listFiles(new FilenameFilter()
         {
            @Override
            public boolean accept(File dir, String name)
            {
               return name.matches(".*\\.jar");
            }
         });
         
         if (jars == null )
         {
            new AddOnsException("No jar found in " + dir.getPath() + " directory");
         }
         else
         {
            loadAddOnsJars(jars);
         }
   }

   /**
    * Load add ons jars.
    *
    * @param jars the jars
    */
   private static void loadAddOnsJars(File[] jars)
   {
      AddOnsURLClassLoader loader = new AddOnsURLClassLoader();
      List<String> jarInfos = new LinkedList<>();
      
      // Fill list of found jars
      jarInfos = new LinkedList<>();
      for (File jar : jars)
      {
         // Add every jar in loader
         try
         {
            URL url = jar.toURI().toURL();
            try
            {
               loader.addURL(url);

               // load possible addons text information found in the jar file
               ZipInputStreamContainer container = new ZipInputStreamContainer(jar.getAbsolutePath());
               InputStream istream = container.getFile("addon.txt");
               jarInfos.addAll(loadStreamInfo(istream));
               initializeNewAddOns(jarInfos, loader, true);
            }
            catch (Exception ex)
            {
            }
         }
         catch (MalformedURLException e)
         {
            new AddOnsException("Bad jar URL : " + jar.getAbsolutePath(), e);
         }
      }
   }

   /**
    * Load stream info.
    *
    * @param istream the file stream to parse for information
    * @return the list
    * @throws UnsupportedEncodingException the unsupported encoding exception
    */
   private static final List<String> loadStreamInfo(InputStream istream) throws UnsupportedEncodingException
   {
      List<String> ret = new LinkedList<>();
      Reader reader = new InputStreamReader(istream, "UTF-8");
      BufferedReader bufreader = new BufferedReader(reader);
      Stream<String> stream = bufreader.lines();

      stream.forEach(entry -> {
         // Eliminate starting and ending blanks
         String className = entry.trim();
         // Ignore comments
         if ((!className.isEmpty()) && (!className.startsWith("#")))
         {
            ret.add(className.trim());
         }
      });
      stream.close();
      return ret;
   }
   
   /**
    * Initialize new add ons.
    *
    * @param jarInfos the jar infos
    * @param loader the loader
    * @param useLoader if should use loader
    */
   private static final void initializeNewAddOns(List<String> jarInfos, ClassLoader loader, boolean useLoader)
   {
      // Look in jar info list
      for (String addOn : jarInfos)
      {
         // Only work on new addons
         if (!loadedAddOnPaths.contains(addOn))
         {
            try
            {
               Class<?> cl = null;
               if (useLoader)
               {
                  // This is a way to load a class in the URL loader
                  cl = ((AddOnsURLClassLoader) loader).findAClass(addOn);
               }
               else
               {
                  // Load a class in system class loader
                  cl = AddOnManager.class.getClassLoader().loadClass(addOn);
               }
               
               if (cl == null)
               {
                  new AddOnsException("Class not found : " + addOn);
               }
               else if (AddOn.class.isAssignableFrom(cl))
               {
                  // load addon and initialize it
                  AddOn instance = (AddOn) cl.getDeclaredConstructor().newInstance();
                  instance.initialize();
                  loadedAddOnPaths.add(addOn);
                  addOns.add(instance);
               }
            }
            catch (AddOnsException lntex)
            {
                // Nothing
            }
            catch (NoClassDefFoundError e)
            {
               new AddOnsException("Unknown class : " + addOn, e);
            }
            catch (Exception e)
            {
               new AddOnsException("Class access exception", e);
            }
         }
      }
   }

   /**
    * Gets the adds ons.
    *
    * @return the adds ons
    */
   protected static List<net.alantea.tools.addons.AddOn> getAddOns()
   {
      return addOns;
   }
   // -----------------------------------------------------

   /**
    * The Class AddOnsURLClassLoader.
    */
   // -----------------------------------------------------
   private static final class AddOnsURLClassLoader extends URLClassLoader
   {

      /**
       * Instantiates a new AddOns URL class loader.
       */
      public AddOnsURLClassLoader()
      {
         super(new URL[0]);
      }

      /**
       * Adds the URL to loader.
       *
       * @param url the url to load
       */
      @Override
      protected final void addURL(URL url)
      {
         super.addURL(url);
      }
      
      /**
       * Find A class to be loaded.
       *
       * @param addOn the add on class name to find and load
       * @return the loaded class
       * @throws ClassNotFoundException the class not found exception
       */
      protected final Class<?> findAClass(String addOn) throws ClassNotFoundException
      {
         return super.findClass(addOn);
      }
   }
}
