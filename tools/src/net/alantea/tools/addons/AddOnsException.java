package net.alantea.tools.addons;

import net.alantea.utils.exception.LntException;

/**
 * The Class AddOnsException.
 * This is just a derived class from LntException. These exceptions are listed in the log.
 */
@SuppressWarnings("serial")
public class AddOnsException extends LntException
{
   /**
    * Constructor.
    * @param text to show.
    */
   public AddOnsException(String text)
   {
      super(text);
   }

   /**
    * Constructor.
    * @param text to show.
    * @param cause of the exception
    */
   public AddOnsException(String text, Throwable cause)
   {
      super(text, cause);
   }
}
