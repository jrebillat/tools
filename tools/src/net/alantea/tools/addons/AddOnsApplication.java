package net.alantea.tools.addons;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;


/**
 * The Class AddOnsApplication.
 * This manage
 */
public class AddOnsApplication
{
	
	/**
	 * Instantiates a new application with addons.
	 *
	 * @param configurationResourceFilePath the configuration resource file path in resources
	 * @param addonsDirectory the addons directory on station
	 */
	protected AddOnsApplication(String configurationResourceFilePath, String addonsDirectory)
	{
      AddOnManager.loadMasterClassLoaderAddOns(configurationResourceFilePath);
      verifyAddOnsDirectory(addonsDirectory + "/addons");
		AddOnManager.loadDirectories(addonsDirectory + "/addons");
	}
	
	/**
	 * Verify add ons directory.
	 *
	 * @param directory the directory
	 */
	private void verifyAddOnsDirectory(String directory)
   {
      File addonsDirectory = new File(directory);
      if (!addonsDirectory.exists())
      {
         try
         {
            Files.createDirectories(addonsDirectory.toPath());
         }
         catch (IOException e)
         {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
      }
      if (!addonsDirectory.isDirectory())
      {
         System.out.println("Error : not a directory : " + addonsDirectory.getAbsolutePath());
         System.exit(1);
      }
   }
}
