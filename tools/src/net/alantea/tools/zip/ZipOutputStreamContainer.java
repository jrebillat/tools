package net.alantea.tools.zip;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import net.alantea.tools.crypto.Codec;
import net.alantea.utils.FileUtilities;

/**
 * The Class ZipOutputStreamContainer.
 */
public final class ZipOutputStreamContainer
{
   
   /** The key. */
   private String key;
   
   /** The algorithm. */
   private String algorithm;
   
   /** The stream. */
   private ByteArrayOutputStream stream = new ByteArrayOutputStream();
   
   /** The output. */
   private ZipOutputStream output;

   /**
    * Instantiates a new zip stream container.
    *
    * @throws FileNotFoundException the file not found exception
    */
   public ZipOutputStreamContainer() throws FileNotFoundException
   {
     this(null, null);
   }

   /**
    * Instantiates a new zip stream container.
    *
    * @param key the key
    * @param algorithm the algorithm
    * @throws FileNotFoundException the file not found exception
    */
   public ZipOutputStreamContainer(String key, String algorithm) throws FileNotFoundException
   {
      this.key = key;
      this.algorithm = algorithm;
      this.output = new ZipOutputStream(stream);
   }

   /**
    * Append full directory and files, recursively, or just one file.
    *
    * @param filePath the file path
    * @param outputPath the output path
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public void append(String filePath, String outputPath) throws IOException
   {
      File file = new File(filePath);
      if (file.isDirectory())
      {
         appendDirectory(file);
      }
      else
      {
         append(file, outputPath);
      }
   }

   /**
    * Append directory and its content, recursively.
    *
    * @param dir the directory 
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public void appendDirectory(File dir) throws IOException
   {
      appendDirectory(dir, null);
   }

   /**
    * Append directory and its content, recursively.
    *
    * @param dir the directory
    * @param parentPath the parent path
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public void appendDirectory(File dir, String parentPath) throws IOException
   {
      if ((dir.exists()) && (dir.isDirectory()))
      {
         for (File file : dir.listFiles((f, name) -> !((".".equals(name)) || ("..".equals(name)))))
         {
            if (file.isDirectory())
            {
               if (parentPath != null)
               {
                  appendDirectory(file, parentPath + "/" + dir.getName());
               }
               else
               {
                  appendDirectory(file, dir.getName());
               }
            }
            else
            {
               if (parentPath != null)
               {
                  append(file, parentPath + "/" + dir.getName() + "/" + file.getName());
               }
               else
               {
                  append(file, dir.getName() + "/" + file.getName());
               }
            }
         }
      }
   }

   /**
    * Append one file.
    *
    * @param file the file
    * @param outputPath the output path
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public void append(File file, String outputPath) throws IOException
   {
      ZipEntry entry = new ZipEntry(outputPath);
      entry.setSize(file.length());
      entry.setTime(file.lastModified());
      output.putNextEntry(entry);
      FileInputStream in = new FileInputStream(file);
      try
      {
         FileUtilities.copy(in, output);
      }
      finally
      {
         try
         {
            in.close();
         }
         catch (IOException e)
         {
            e.printStackTrace();
         }
      }
      output.closeEntry();

   }

   /**
    * Append Stream.
    *
    * @param stream the stream
    * @param outputPath the output path
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public void append(InputStream stream, String outputPath) throws IOException
   {
      ZipEntry entry = new ZipEntry(outputPath);
      entry.setSize(stream.available());
      entry.setTime(new Date().getTime());
      output.putNextEntry(entry);
      FileUtilities.copy(stream, output);
      output.closeEntry();
   }
   
   /**
    * Close.
    *
    * @return the byte array
    * @throws GeneralSecurityException when raised
    */
   public byte[] close() throws GeneralSecurityException
   {
      try
      {
         output.close();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      byte[] bytes = stream.toByteArray();
      if ((key == null) || (algorithm == null))
      {
         return bytes;
      }
      return Codec.encrypt(bytes, key, algorithm);
   }
}
