package net.alantea.tools.zip;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.security.GeneralSecurityException;

import net.alantea.utils.FileUtilities;

/**
 * The Class ZipUtils.
 */
public final class ZipUtils
{

   /**
    * Instantiates a new zip utils.
    */
   private ZipUtils()
   {
   }

   /**
    * Open temporary directory.
    *
    * @param key the key
    * @param containerPath the container path
    * @return the file
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static File openTemporaryDirectory(String key, String containerPath) throws IOException
   {
      ZipInputStreamContainer container = new ZipInputStreamContainer(containerPath);
      return openTemporaryDirectory(key, container);
   }

   /**
    * Open temporary directory.
    *
    * @param key the key
    * @param containerFile the container file
    * @return the file
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static File openTemporaryDirectory(String key, File containerFile) throws IOException
   {
      ZipInputStreamContainer container = new ZipInputStreamContainer(containerFile.getAbsolutePath());
      return openTemporaryDirectory(key, container);
   }

   /**
    * Open temporary directory.
    *
    * @param key the key
    * @param container the container
    * @return the file
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static File openTemporaryDirectory(String key, ZipInputStreamContainer container) throws IOException
   {
      File destination = FileUtilities.createTemporaryDirectory(key);
      openDirectory(container, destination.getAbsolutePath());
      return destination;
   }

   /**
    * Open directory.
    *
    * @param containerFile the container file
    * @param destination the destination
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static void openDirectory(File containerFile, String destination) throws IOException
   {
      ZipInputStreamContainer container = new ZipInputStreamContainer(containerFile.getAbsolutePath());
      openDirectory(container, destination);
   }

   /**
    * Open directory.
    *
    * @param containerPath the container path
    * @param destination the destination
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static void openDirectory(String containerPath, String destination) throws IOException
   {
      ZipInputStreamContainer container = new ZipInputStreamContainer(containerPath);
      openDirectory(container, destination);
   }

   /**
    * Open directory.
    *
    * @param container the container
    * @param destination the destination
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static void openDirectory(ZipInputStreamContainer container, String destination) throws IOException
   {
      Files.createDirectories(new File(destination).toPath());
      for (String path : container.getEntries())
      {
         InputStream input = container.getFile(path);
         File file = new File(destination + "/" + path);
         if (!file.exists())
         {
            Files.createDirectories(file.getParentFile().toPath());
            Files.copy(input,  file.toPath());
         }
         input.close();
      }
   }

   /**
    * Save directory.
    *
    * @param directoryPath the directory path
    * @param containerPath the container path
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static void saveDirectory(String directoryPath, String containerPath) throws IOException
   {
      File directory = new File(directoryPath);
      ZipOutputStreamContainer zipOutput = new ZipOutputStreamContainer();
      for (File file : directory.listFiles())
      {
         if (file.isDirectory())
         {
            zipOutput.appendDirectory(file);
            
         }
         else
         {
            zipOutput.append(file.getAbsolutePath(), file.getName());
         }
      }
      try
      {
         byte[] bytes = zipOutput.close();
         FileOutputStream stream = new FileOutputStream(containerPath);
         try
         {
            stream.write(bytes);
         }
         finally
         {
            stream.close();
         }
      }
      catch (GeneralSecurityException e)
      {
         throw new IOException(e);
      }
   }
}
