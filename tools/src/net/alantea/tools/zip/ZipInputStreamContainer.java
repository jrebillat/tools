package net.alantea.tools.zip;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * The Class ZipInputStreamContainer.
 */
public final class ZipInputStreamContainer
{   
   
   /** The path map. */
   private Map<String, ZipEntry> pathMap = new LinkedHashMap<>();
   
   /** The content map. */
   private Map<ZipEntry, byte[]> contentMap = new LinkedHashMap<>();

   /**
    * Instantiates a new zip container.
    *
    * @param path the path
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public ZipInputStreamContainer(String path) throws IOException
   {
      this(new FileInputStream(new File(path)));
   }

   /**
    * Instantiates a new zip container.
    *
    * @param stream the stream
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public ZipInputStreamContainer(InputStream stream) throws IOException
   {
      ZipInputStream input = new ZipInputStream(stream);

      ZipEntry  zipentry = input.getNextEntry();
      while (zipentry != null)
      {
         if ((zipentry.isDirectory()) || zipentry.getName().endsWith("/"))
         {
            pathMap.put(zipentry.getName(), zipentry);
            zipentry = input.getNextEntry();
            continue;
         }

         ByteArrayOutputStream byteoutputstream = new ByteArrayOutputStream();

         int n;
         byte[] buf = new byte[1024];
         while ((n = input.read(buf, 0, 1024)) > -1)
         {
            byteoutputstream.write(buf, 0, n);
         }

         byteoutputstream.close();
         contentMap.put(zipentry, byteoutputstream.toByteArray());
         pathMap.put(zipentry.getName(), zipentry);
         input.closeEntry();
         zipentry = input.getNextEntry();
      }

      input.close();
   }
   
   /**
    * Gets the entries.
    *
    * @return the entries
    */
   public List<String> getEntries()
   {
      List<String> ret = new ArrayList<>();
      ret.addAll(pathMap.keySet());
      return ret;
   }

   /**
    * Gets the file.
    *
    * @param path the path
    * @return the file
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public InputStream getFile(String path) throws IOException
   {
      ZipEntry entry = pathMap.get(path);
      if ((entry != null) && (!path.endsWith("/")))
      {
         return new ByteArrayInputStream(contentMap.get(entry));
      }
      return null;
   } 
   
   /**
    * Save file.
    *
    * @param root the root
    * @param path the path
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public void saveFile(String root, String path) throws IOException
   {
      InputStream stream = getFile(path);
      if (stream != null)
      {
         byte[] buffer = new byte[stream.available()];
         stream.read(buffer);

         File targetFile = new File(root + path);
         targetFile.getParentFile().mkdirs();
         OutputStream outStream = new FileOutputStream(targetFile);
         outStream.write(buffer);
         outStream.close();
      }  
   }
   
   /**
    * Close.
    */
   public void close()
   {
   }
}
