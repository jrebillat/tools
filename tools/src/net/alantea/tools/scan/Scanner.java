package net.alantea.tools.scan;

import java.util.LinkedList;
import java.util.List;

import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import io.github.lukehutch.fastclasspathscanner.scanner.ScanResult;

/**
 * The Class Scanner.
 */
public final class Scanner
{
   /** The scanner. */
   private static FastClasspathScanner scanner = new FastClasspathScanner()
         .enableMethodAnnotationIndexing()
         .enableFieldAnnotationIndexing()
         .ignoreFieldVisibility()
         .ignoreMethodVisibility();

   /** The scan result. */
   private static ScanResult scanResult = scanner.scan();
   
   /** The loaders. */
   private static List<ClassLoader> loaders = new LinkedList<>();
   /**
    * Instantiates a new scanner.
    */
   private Scanner()
   {
   }
   
   /**
    * Scan classes.
    */
   private static void scan()
   {
      if (scanResult == null)
      {
         scanResult = scanner.scan();
      }
   }
   
   /**
    * Append class loader and rescan.
    *
    * @param loader the loader
    */
   public static void appendClassLoader(ClassLoader loader)
   {
      if (loaders.isEmpty())
      {
         loaders.add(ClassLoader.getSystemClassLoader());
      }
      scanner = scanner.addClassLoader(loader);
      loaders.add(loader);
      scanResult = null;
   }

   /**
    * Gets the names of all classes.
    *
    * @return the names of all classes
    */
   public static List<String> getNamesOfAllClasses()
   {
      scan();
      return scanResult.getNamesOfAllClasses();
   }
   
   /**
    * Gets the names of classes with annotations any of.
    *
    * @param annotations the annotations
    * @return the names of classes with annotations any of
    */
   public static List<String> getNamesOfClassesWithAnnotationsAnyOf(Class<?>... annotations)
   {
      scan();
      return scanResult.getNamesOfClassesWithAnnotationsAnyOf(annotations);
   }

   /**
    * Gets the names of classes with annotations any of.
    *
    * @param annotations the annotations
    * @return the names of classes with annotations any of
    */
   public static List<String> getNamesOfClassesWithAnnotationsAnyOf(String... annotations)
   {
      scan();
      return scanResult.getNamesOfClassesWithAnnotationsAnyOf(annotations);
   }

   /**
    * Gets the names of classes with all given annotations.
    *
    * @param annotations the annotations
    * @return the names of classes with annotations any of
    */
   public static List<String> getNamesOfClassesWithAnnotationsAllOf(String... annotations)
   {
      scan();
      return scanResult.getNamesOfClassesWithAnnotationsAllOf(annotations);
   }
   
   /**
    * Gets the names of classes with all given annotations.
    *
    * @param annotations the annotations
    * @return the names of classes with annotations any of
    */
   public static List<String> getNamesOfClassesWithAnnotationsAllOf(Class<?>... annotations)
   {
      scan();
      return scanResult.getNamesOfClassesWithAnnotationsAllOf(annotations);
   }

   /**
    * Gets the names of classes implementing.
    *
    * @param baseClass the base class
    * @return the names of classes implementing
    */
   public static List<String> getNamesOfClassesImplementing(Class<?> baseClass)
   {
      scan();
      return scanResult.getNamesOfClassesImplementing(baseClass);
   }

   /**
    * Gets the names of classes implementing.
    *
    * @param className the class name
    * @return the names of classes implementing
    */
   public static List<String> getNamesOfClassesImplementing(String className)
   {
      scan();
      return scanResult.getNamesOfClassesImplementing(className);
   }
   
   /**
    * Gets the names of classes with annotation.
    *
    * @param name the name
    * @return the names of classes with annotation
    */
   public static List<String> getNamesOfClassesWithAnnotation(String name)
   {
      scan();
      return scanResult.getNamesOfClassesWithAnnotation(name);
   }

   /**
    * Gets the names of classes with annotation.
    *
    * @param annotation the annotation
    * @return the names of classes with annotation
    */
   public static List<String> getNamesOfClassesWithAnnotation(Class<?> annotation)
   {
      scan();
      return scanResult.getNamesOfClassesWithAnnotation(annotation);
   }

   /**
    * Gets the names of subclasses of.
    *
    * @param baseClass the base class
    * @return the names of subclasses of
    */
   public static List<String> getNamesOfSubclassesOf(Class<?> baseClass)
   {
      scan();
      return scanResult.getNamesOfSubclassesOf(baseClass);
   }

   /**
    * Gets the names of subclasses of.
    *
    * @param className the class name
    * @return the names of subclasses of
    */
   public static List<String> getNamesOfSubclassesOf(String className)
   {
      scan();
      return scanResult.getNamesOfSubclassesOf(className);
   }

   /**
    * Gets the names of classes with field annotation.
    *
    * @param annotation the annotation
    * @return the names of classes with field annotation
    */
   public static List<String> getNamesOfClassesWithFieldAnnotation(Class<?> annotation)
   {
      scan();
      return scanResult.getNamesOfClassesWithFieldAnnotation(annotation);
   }

   /**
    * Gets the names of classes with field annotation.
    *
    * @param className the class name
    * @return the names of classes with field annotation
    */
   public static List<String> getNamesOfClassesWithFieldAnnotation(String className)
   {
      scan();
      return scanResult.getNamesOfClassesWithFieldAnnotation(className);
   }

   /**
    * Gets the names of classes with method annotation.
    *
    * @param annotation the annotation
    * @return the names of classes with method annotation
    */
   public static List<String> getNamesOfClassesWithMethodAnnotation(Class<?> annotation)
   {
      scan();
      return scanResult.getNamesOfClassesWithMethodAnnotation(annotation);
   }

   /**
    * Gets the names of classes with method annotation.
    *
    * @param className the class name
    * @return the names of classes with method annotation
    */
   public static List<String> getNamesOfClassesWithMethodAnnotation(String className)
   {
      scan();
      return scanResult.getNamesOfClassesWithMethodAnnotation(className);
   }
   
   /**
    * Gets the class loaders.
    *
    * @return the class loaders
    */
   public static ClassLoader[] getClassLoaders()
   {
      if (loaders.isEmpty())
      {
         loaders.add(ClassLoader.getSystemClassLoader());
      }
      
      return loaders.toArray(new ClassLoader[0]);
   }
   
   /**
    * Gets the class.
    *
    * @param className the class name
    * @return the class
    */
   public static Class<?> getClass(String className)
   {
      for (ClassLoader loader : getClassLoaders())
      {
         try
         {
            Class<?> cl = loader.loadClass(className);
            return cl;
         }
         catch (ClassNotFoundException e)
         {
         }
      }
      return null;
   }
}
