package net.alantea.tools.color;


import java.awt.Color;

import net.alantea.liteprops.Property;
import net.alantea.utils.ColorNames;

/**
 * The ColorWheel class. A color wheel define a set of colors depending on one base value.
 */
public class ColorWheel
{
   /** The base color. */
   private Property<Color> _baseColor = new Property<Color>();
   
   /** The left color. */
   private Property<Color> _leftColor = new Property<Color>();
   
   /** The right color. */
   private Property<Color> _rightColor = new Property<Color>();
   
   /** The opposite color. */
   private Property<Color> _oppositeColor = new Property<Color>();
   
   /** The brighter color. */
   private Property<Color> _brighterColor = new Property<Color>();
   
   /** The darker color. */
   private Property<Color> _darkerColor = new Property<Color>();
   
   /** The first Third color. */
   private Property<Color> _firstThirdColor = new Property<Color>();
   
   /** The second Third color. */
   private Property<Color> _secondThirdColor = new Property<Color>();
   
   /** The light color. */
   private Property<Color> _lightColor = new Property<Color>();
   
   /** The dark color. */
   private Property<Color> _darkColor = new Property<Color>();
   
   /** The ultralight color. */
   private Property<Color> _ultralightColor = new Property<Color>();
   
   /** The ultradark color. */
   private Property<Color> _ultradarkColor = new Property<Color>();
   
   /** The greyed color. */
   private Property<Color> _greyedColor = new Property<Color>();
   
   /** if the ColorWheel is inverted. */
   private boolean _isInverted;

   /**
    * Constructor binding to a property.
    * @param colorProperty to bind as base
    */
   public ColorWheel(Property<Color> colorProperty)
   {
      init();
      _baseColor.bind(colorProperty);
   }

   /**
    * Constructor from a color.
    * @param color to use as base
    */
   public ColorWheel(Color color)
   {
      init();
      _baseColor.set(color);
   }

   /**
    * Constructor from a web color description.
    * @param color to use as base
    */
   public ColorWheel(String color)
   {
      this(ColorNames.getColor(color));
   }

   /**
    * Constructor from HSBA color description.
    * @param hue the hue
    * @param saturation the saturation
    * @param brightness the brightness
    * @param alpha the alpha value
    */
   public ColorWheel(double hue, double saturation, double brightness, double alpha)
   {
      Color tmp = Color.getHSBColor((float) hue, (float) saturation, (float) brightness);
      Color color = new Color(tmp.getRed() / 255.0f, tmp.getGreen() / 255.0f,
          tmp.getBlue() / 255.0f, (float) alpha);
      init();
      _baseColor.set(color);
   }
   
   /**
    * Sets the color scheme as inverted or not.
    *
    * @param inverted invert if true
    */
   public void setInverted(boolean inverted)
   {
      _isInverted = inverted;
      recalculate();
   }
   
   /**
    * Initialize listener.
    */
   private void init()
   {
      _baseColor.addListener((oldV, newV) ->
      {
         recalculate();
      });
   }

   /**
    * Calculate values from base color.
    */
   private void recalculate()
   {
      Color base = _baseColor.get();
      float[] hsb = Color.RGBtoHSB(base.getRed(), base.getGreen(), base.getBlue(), null);
      
      Color rightColor = derive(0.075, 0.0, 0.0);
      Color leftColor = derive(-0.075, 0.0, 0.0);
      Color brighterColor = derive(0.0, 0.1, 0.3);
      Color darkerColor = derive(0.0, -0.1, -0.3);
      _oppositeColor.set(derive(0.5, 0.0, 0.0));
      Color firstThirdColor = derive(0.4, 0.0, 0.0);
      Color secondThirdColor = derive(-0.4, 0.0, 0.0);
      
      Color tmp = Color.getHSBColor(hsb[0], 0.05f, 1.0f);
      Color ultralightColor = new Color(tmp.getRed() / 255.0f, tmp.getGreen() / 255.0f,
            tmp.getBlue() / 255.0f, base.getAlpha() / 255.0f);
      tmp = Color.getHSBColor(hsb[0], 1.0f, 0.1f);
      Color ultradarkColor = new Color(tmp.getRed() / 255.0f, tmp.getGreen() / 255.0f,
            tmp.getBlue() / 255.0f, base.getAlpha() / 255.0f);
      
      tmp = Color.getHSBColor(hsb[0], 0.1f, 0.95f);
      Color lightColor = new Color(tmp.getRed() / 255.0f, tmp.getGreen() / 255.0f,
            tmp.getBlue() / 255.0f, base.getAlpha() / 255.0f);
      tmp = Color.getHSBColor(hsb[0], 0.9f, 0.25f);
      Color darkColor = new Color(tmp.getRed() / 255.0f, tmp.getGreen() / 255.0f,
            tmp.getBlue() / 255.0f, base.getAlpha() / 255.0f);
      
      tmp = Color.getHSBColor(hsb[0], 0.0f, hsb[2]);
      Color greyedColor = new Color(tmp.getRed() / 255.0f, tmp.getGreen() / 255.0f,
            tmp.getBlue() / 255.0f, base.getAlpha() / 255.0f);
      
      if (_isInverted)
      {
         _leftColor.set(rightColor);
         _rightColor.set(leftColor);
         _brighterColor.set(darkerColor);
         _darkerColor.set(brighterColor);
         _firstThirdColor.set(secondThirdColor);
         _secondThirdColor.set(firstThirdColor);
         _ultralightColor.set(ultradarkColor);
         _ultradarkColor.set(ultralightColor);
         _lightColor.set(darkColor);
         _darkColor.set(lightColor);
      }
      else
      {
         _leftColor.set(leftColor);
         _rightColor.set(rightColor);
         _brighterColor.set(brighterColor);
         _darkerColor.set(darkerColor);
         _firstThirdColor.set(firstThirdColor);
         _secondThirdColor.set(secondThirdColor);
         _ultralightColor.set(ultralightColor);
         _ultradarkColor.set(ultradarkColor);
         _lightColor.set(lightColor);
         _darkColor.set(darkColor);
      }
      _greyedColor.set(greyedColor);
   }
   
   /**
    * Calculate a delta color.
    *
    * @param deltaHue the delta hue
    * @param deltaSaturation the delta saturation
    * @param deltaBrightness the delta brightness
    * @return the color
    */
   private Color derive(double deltaHue, double deltaSaturation, double deltaBrightness)
   {
      Color base = _baseColor.get();
      float[] hsb = Color.RGBtoHSB(base.getRed(), base.getGreen(), base.getBlue(), null);
      Color tmp = Color.getHSBColor(hsb[0] + (float) deltaHue, limit(hsb[1],
            deltaSaturation), limit(hsb[2], deltaBrightness));
      return new Color(tmp.getRed() / 255.0f, tmp.getGreen() / 255.0f,
            tmp.getBlue() / 255.0f, base.getAlpha() / 255.0f);
   }
   
   /**
    * Calculate a delta color.
    *
    * @param deltaHue the delta hue
    * @param deltaSaturation the delta saturation
    * @param deltaBrightness the delta brightness
    * @param deltaAlpha the delta alpha value
    * @return the color
    */
   private Color derive(double deltaHue, double deltaSaturation, double deltaBrightness, double deltaAlpha)
   {
      Color base = _baseColor.get();
      float[] hsb = Color.RGBtoHSB(base.getRed(), base.getGreen(), base.getBlue(), null);
      Color tmp = Color.getHSBColor(hsb[0] + (float) deltaHue, limit(hsb[1],
            deltaSaturation), limit(hsb[2], deltaBrightness));
      return new Color(tmp.getRed() / 255.0f, tmp.getGreen() / 255.0f,
            tmp.getBlue() / 255.0f, limit(base.getAlpha(), deltaAlpha));
   }
   
   /**
    * Calculate a delta color property bound to base color property.
    *
    * @param deltaHue the delta hue
    * @param deltaSaturation the delta saturation
    * @param deltaBrightness the delta brightness
    * @param deltaAlpha the delta alpha value
    * @return the color property, bound to base color
    */
   public Property<Color> deriveColorProperty(double deltaHue, double deltaSaturation, 
         double deltaBrightness, double deltaAlpha)
   {
      Color derived = derive(deltaHue, deltaSaturation, deltaBrightness, deltaAlpha);
      Property<Color> colorProperty = new Property<Color>(derived);
      _baseColor.addListener((oldV, newV) ->
      {
         colorProperty.set(derive(deltaHue, deltaSaturation, deltaBrightness, deltaAlpha));
      });
      return colorProperty;
   }

   /**
    * Derive a grayed version for a color property.
    * @param origin color property to derive as grayed
    * @return the grayed version property, connected to origin property
    */
   public static Property<Color> getGrayedColorProperty(Property<Color> origin)
   {
      Color derived = getGrayedColor(origin.get());
      Property<Color> colorProperty = new Property<Color>(derived);
      origin.addListener((oldV, newV) ->
      {
         colorProperty.set(getGrayedColor(newV));
      });
      return colorProperty;
   }

   /**
    * Derive a grayed version for a color.
    * @param base color to derive as grayed
    * @return the grayed color
    */
   private static Color getGrayedColor(Color base)
   {
      float[] hsb = Color.RGBtoHSB(base.getRed(), base.getGreen(), base.getBlue(), null);
      Color tmp = Color.getHSBColor(hsb[0], 0.0f, hsb[2]);
      return new Color(tmp.getRed() / 255.0f, tmp.getGreen() / 255.0f,
            tmp.getBlue() / 255.0f, base.getAlpha() / 255.0f);
   }
   
   /**
    * Derive a semi-opaque version for a color property.
    * @param origin color property to derive as semi-opaque
    * @param opacity to set, between 0.0 and 1.0
    * @return the semi-opaque version property, connected to origin property
    */
   public static Property<Color> getSemiOpaqueColorProperty(
         Property<Color> origin, double opacity)
   {
      Color derived = getSemiOpaqueColor(origin.get(), opacity);
      Property<Color> colorProperty = new Property<Color>(derived);
      origin.addListener((oldV, newV) ->
      {
         colorProperty.set(getSemiOpaqueColor(newV, opacity));
      });
      return colorProperty;
   }

   /**
    * Derive a semi-opaque version for a color.
    * @param base color to derive
    * @param opacity to set, between 0.0 and 1.0
    * @return the semi-opaque color
    */
   private static Color getSemiOpaqueColor(Color base, double opacity)
   {
      float opac = (float) (Math.min(1.0, Math.max(0.0, opacity)));
      return new Color(base.getRed() / 255.0f, base.getGreen() / 255.0f, base.getBlue() / 255.0f,
            opac);
   }
   
   /**
    * Limit values between 0.0 and 1.0.
    * @param base value
    * @param delta delta to apply
    * @return result
    */
   private float limit(float base, double delta)
   {
      float res = base + (float) delta;
      if (res < 0.0f)
      {
         res = 0.0f;
      }
      if (res > 1.0)
      {
         res = 1.0f;
      }
      return res;
   }
   
   /**
    * Sets the hue.
    *
    * @param hue the new hue
    */
   public void setHue(double hue)
   {
      Color base = _baseColor.get();
      float[] hsb = Color.RGBtoHSB(base.getRed(), base.getGreen(), base.getBlue(), null);
      _baseColor.set(derive(hue - hsb[0], 0.0, 0.0));
   }
   
   /**
    * Sets the saturation.
    *
    * @param saturation the new saturation
    */
   public void setSaturation(double saturation)
   {
      Color base = _baseColor.get();
      float[] hsb = Color.RGBtoHSB(base.getRed(), base.getGreen(), base.getBlue(), null);
      _baseColor.set(derive(0.0, saturation - hsb[1], 0.0));
   }
   
   /**
    * Sets the brightness.
    *
    * @param brightness the new brightness
    */
   public void setBrightness(double brightness)
   {
      Color base = _baseColor.get();
      float[] hsb = Color.RGBtoHSB(base.getRed(), base.getGreen(), base.getBlue(), null);
      _baseColor.set(derive(0.0, 0.0, brightness - hsb[2]));
   }

   /**
    * Base color property.
    *
    * @return the object property
    */
   public final Property<Color> baseColorProperty()
   {
      return this._baseColor;
   }
   
   /**
    * Gets the base color.
    *
    * @return the base color
    */
   public final Color getBaseColor()
   {
      return this.baseColorProperty().get();
   }
   
   /**
    * Sets the base color.
    *
    * @param baseColor the new base color
    */
   public final void setBaseColor(final Color baseColor)
   {
      this.baseColorProperty().set(baseColor);
   }
   
   /**
    * Left color property.
    *
    * @return the object property
    */
   public final Property<Color> leftColorProperty()
   {
      return this._leftColor;
   }
   
   /**
    * Gets the left color.
    *
    * @return the left color
    */
   public final Color getLeftColor()
   {
      return this.leftColorProperty().get();
   }
   
   /**
    * Right color property.
    *
    * @return the object property
    */
   public final Property<Color> rightColorProperty()
   {
      return this._rightColor;
   }
   
   /**
    * Gets the right color.
    *
    * @return the right color
    */
   public final Color getRightColor()
   {
      return this.rightColorProperty().get();
   }
   
   /**
    * Opposite color property.
    *
    * @return the object property
    */
   public final Property<Color> oppositeColorProperty()
   {
      return this._oppositeColor;
   }
   
   /**
    * Gets the opposite color.
    *
    * @return the opposite color
    */
   public final Color getOppositeColor()
   {
      return this.oppositeColorProperty().get();
   }
   
   /**
    * Brigther color property.
    *
    * @return the object property
    */
   public final Property<Color> brighterColorProperty()
   {
      return this._brighterColor;
   }
   
   /**
    * Gets the brigther color.
    *
    * @return the brigther color
    */
   public final Color getBrigtherColor()
   {
      return this.brighterColorProperty().get();
   }
   
   /**
    * Darker color property.
    *
    * @return the object property
    */
   public final Property<Color> darkerColorProperty()
   {
      return this._darkerColor;
   }
   
   /**
    * Gets the darker color.
    *
    * @return the darker color
    */
   public final Color getDarkerColor()
   {
      return this.darkerColorProperty().get();
   }
   
   /**
    * first Third color property.
    *
    * @return the object property
    */
   public final Property<Color> firstThirdColorProperty()
   {
      return this._firstThirdColor;
   }
   
   /**
    * Gets the first Third color.
    *
    * @return the firstThird color
    */
   public final Color getFirstThirdColor()
   {
      return this.firstThirdColorProperty().get();
   }
   
   /**
    * second Third color property.
    *
    * @return the object property
    */
   public final Property<Color> secondThirdColorProperty()
   {
      return this._secondThirdColor;
   }
   
   /**
    * Gets the first Third color.
    *
    * @return the firstThird color
    */
   public final Color getSecondThirdColor()
   {
      return this.secondThirdColorProperty().get();
   }
   
   /**
    * ultra light (nearly white) color property.
    *
    * @return the object property
    */
   public final Property<Color> ultralightColorProperty()
   {
      return this._ultralightColor;
   }
   
   /**
    * Gets the ultra light (nearly white) color.
    *
    * @return the ultra light (nearly white) color
    */
   public final Color getUltralightColor()
   {
      return this.ultralightColorProperty().get();
   }
   
   /**
    * ultra dark (nearly black) color property.
    *
    * @return the object property
    */
   public final Property<Color> ultradarkColorProperty()
   {
      return this._ultradarkColor;
   }
   
   /**
    * Gets the ultra dark (nearly black) color.
    *
    * @return the ultra dark (nearly black) color
    */
   public final Color getUltradarkColor()
   {
      return this.ultradarkColorProperty().get();
   }
   
   /**
    * greyed color property.
    *
    * @return the object property
    */
   public final Property<Color> greyedColorProperty()
   {
      return this._ultradarkColor;
   }
   
   /**
    * Gets the greyed color.
    *
    * @return the greyed color
    */
   public final Color getGreyedColor()
   {
      return this.greyedColorProperty().get();
   }
   
   /**
    * light color property.
    *
    * @return the object property
    */
   public final Property<Color> lightColorProperty()
   {
      return this._lightColor;
   }
   
   /**
    * Gets the light color.
    *
    * @return the light color
    */
   public final Color getLightColor()
   {
      return this.lightColorProperty().get();
   }
   
   /**
    * ultra dark color property.
    *
    * @return the object property
    */
   public final Property<Color> darkColorProperty()
   {
      return this._darkColor;
   }
   
   /**
    * Gets the dark color.
    *
    * @return the dark color
    */
   public final Color getDarkColor()
   {
      return this.darkColorProperty().get();
   }
}
