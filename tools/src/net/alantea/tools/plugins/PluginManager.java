package net.alantea.tools.plugins;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.alantea.tools.scan.Scanner;

/**
 * The Class PluginManager.
 */
public final class PluginManager
{   
   /** The plugin ids map. */
   private static Map<Class<?>, List<Object>> pluginIdsMap = new HashMap<>();
   
   /** The plugin classes map. */
   private static Map<String, Class<?>> pluginClassesMap = new HashMap<>();
   
   /** The plugins map. */
   private static Map<String, Object> pluginsMap = new HashMap<>();
   
   /** The needs refresh. */
   private static boolean needsRefresh = true;

   /**
    * Instantiates a new plugin manager.
    */
   private PluginManager()
   {
   }
   
   /**
    * Adds the classes list.
    *
    * @param path the path
    */
   public static void addClassesList(String path)
   {
      File file = new File(path);
      if ((file.exists()) && (file.isFile()))
      {
         addClassesList(file);
      }
   }
   
   /**
    * Adds the classes list.
    *
    * @param file the file
    */
   public static void addClassesList(File file)
   {
      
   }

   /**
    * Adds the container of the class.
    *
    * @param classToLoad the class to load
    */
   public static void addClassContainer(Class<?> classToLoad)
   {
	   addPluginContainer(classToLoad.getClassLoader());
   }

   /**
    * Adds the container.
    *
    * @param loader the loader
    */
   public static void addPluginContainer(ClassLoader loader)
   {
	   Scanner.appendClassLoader(loader);
	   needsRefresh = true;
   }
   
   /**
    * Gets the plugins.
    *
    * @param <T> the generic type
    * @param id the id
    * @return the plugins
    */
   @SuppressWarnings("unchecked")
   public static <T> List<T> getPlugins(String id)
   {
	   if (id == null) 
	   {
		  return null;
	   }
	   if ((pluginClassesMap.get(id) == null) || (needsRefresh))
	   {
		   refreshPluginsList();
	   }
	   return getPlugins((Class<T>) pluginClassesMap.get(id));
   }
   
   /**
    * Load plugins file.
    *
    * @param file the file
    */
   public static void loadPluginsFile(File file)
   {
      PluginsParser parser = new PluginsParser();
      parser.load(file);
   }
   
   /**
    * Load plugins file.
    *
    * @param path the path
    */
   public static void loadPluginsFile(String path)
   {
      PluginsParser parser = new PluginsParser();
      parser.load(new File(path));
   }
   
   /**
    * Adds the plugin manager.
    *
    * @param apiName the api name
    * @param apiClassName the api class name
    */
   static void addPluginManager(String apiName, String apiClassName)
   {
      Class<?> apiClass = loadClass(apiClassName);
      addPluginInstance(apiName, apiClass);
   }
   
   /**
    * Adds the plugin manager.
    *
    * @param apiKey the api key
    * @param apiClass the api class
    */
   static void addPluginManager(String apiKey, Class<?> apiClass)
   {
      pluginClassesMap.put(apiKey, apiClass);
      List<Object> list = pluginIdsMap.get(apiClass);
      if (list == null)
      {
         list = new LinkedList<>();
         pluginIdsMap.put(apiClass, list);
      }
   }
   
   /**
    * Adds the plugin instance.
    *
    * @param instanceName the instance name
    * @param instanceClassName the instance class name
    */
   static void addPluginInstance(String instanceName, String instanceClassName)
   {
      Class<?> instanceClass = loadClass(instanceName);
      addPluginInstance(instanceName, instanceClass);
   }
   
   /**
    * Adds the plugin instance.
    *
    * @param instanceName the instance name
    * @param instanceClass the instance class
    */
   static void addPluginInstance(String instanceName, Class<?> instanceClass)
   {
      if (pluginsMap.get(instanceName) == null)
      {
         if (instanceClass != null)
         {
            try
            {
               Object instance = instanceClass.newInstance();
               pluginsMap.put(instanceName, instance);
            }
            catch (InstantiationException | IllegalAccessException e)
            {
               e.printStackTrace();
            }
         }
      }
   }
   
   /**
    * Gets the plugins.
    *
    * @param <T> the generic type
    * @param apiInterface the api interface
    * @return the plugins
    */
   @SuppressWarnings("unchecked")
   public static <T> List<T> getPlugins(Class<T> apiInterface)
   {
      if ((apiInterface == null) || (needsRefresh))
      {
         refreshPluginsList();
      }
      List<T> ret = new LinkedList<T>();
      List<Object> list = pluginIdsMap.get(apiInterface);
      if (list != null)
      {
         for (Object obj : list)
         {
            ret.add((T) obj);
         }
      }
      return ret;
   }
   
   /**
    * Load class.
    *
    * @param className the class name
    * @return the class
    */
   private static Class<?> loadClass(String className)
   {
      return Scanner.getClass(className);
   }

   /**
    * Refresh plugins list.
    */
   private static void refreshPluginsList()
   {
       List<String> apiNames = Scanner.getNamesOfClassesWithAnnotation(PluginApi.class);
       for (String apiName : apiNames)
       {
          Class<?> apiClass = loadClass(apiName);

          if (apiClass != null)
          {
             String apiKey = apiClass.getAnnotation(PluginApi.class).value();
             pluginClassesMap.put(apiKey, apiClass);
             List<Object> list = pluginIdsMap.get(apiClass);
             if (list == null)
             {
                list = new LinkedList<>();
                pluginIdsMap.put(apiClass, list);
             }
             
             List<String> instanceNames = Scanner.getNamesOfClassesImplementing(apiClass);
             for (String instanceName : instanceNames)
             {
                if (pluginsMap.get(instanceName) == null)
                {
                   Class<?> instanceClass = loadClass(instanceName);
                   if ((instanceClass != null) && (instanceClass.isAnnotationPresent(PluginInstance.class)))
                   {
                      try
                      {
                         Object instance = instanceClass.newInstance();
                         list.add(instance);
                         pluginsMap.put(instanceName, instance);
                      }
                      catch (InstantiationException | IllegalAccessException e)
                      {
                         e.printStackTrace();
                      }
                   }
                }
             }
          }
       }
       needsRefresh = false;
   }
}
