package net.alantea.tools.plugins;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * The Interface PluginInstance. Should only be set on classes implementing one or more interfaces
 * with a PluginApi annotation.
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface PluginInstance
{
}
