package net.alantea.tools.plugins;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * The Interface PluginApi.
 * Should only be set on interfaces.
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface PluginApi
{
   
   /**
    * Value.
    *
    * @return the string
    */
   String value();
}
