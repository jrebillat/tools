package net.alantea.tools.plugins;

import org.xml.sax.Attributes;

import net.alantea.tools.xml.XMLParser;

/**
 * The Class PluginsParser.
 */
public class PluginsParser extends XMLParser
{

   /**
    * Manager start.
    *
    * @param attributes the attributes
    */
   @SuppressWarnings("unused")
   private void managerStart(Attributes attributes)
   {
      String key = attributes.getValue("key");
      String apiClassName = attributes.getValue("class");
      PluginManager.addPluginManager(key, apiClassName);
   }

   /**
    * Plugin start.
    *
    * @param attributes the attributes
    */
   @SuppressWarnings("unused")
   private void pluginStart(Attributes attributes)
   {
      String key = attributes.getValue("key");
      String instanceClassName = attributes.getValue("class");
      PluginManager.addPluginInstance(key, instanceClassName);
   }
}
