package net.alantea.tools.lang;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class Context.
 */
public class LangageContext
{
   /** The content. */
   private Map<String, Object> content = new HashMap<>();
   
   /** The state. */
   private String state;

   /**
    * Instantiates a new context.
    *
    * @param state the state
    */
   public LangageContext(String state)
   {
      this(null, state);
   }

   /**
    * Instantiates a new context.
    *
    * @param parent the parent
    * @param state the state
    */
   public LangageContext(LangageContext parent, String state)
   {
      this.state = state;
      if (parent != null)
      {
         for (String key : parent.content.keySet())
         {
            content.put(key, parent.content.get(key));
         }
      }
   }
   
   /**
    * Put an element.
    *
    * @param key the key
    * @param value the value
    */
   public void put(String key, Object value)
   {
      content.put(key, value);
   }
   
   /**
    * Gets the element.
    *
    * @param <T> the generic type
    * @param key the key
    * @return the t
    */
   @SuppressWarnings("unchecked")
   public <T> T get(String key)
   {
      return (T) content.get(key);
   }
   
   /**
    * Removes the element.
    *
    * @param key the key
    */
   public void remove(String key)
   {
      content.put(key, null);
   }
   
   /**
    * Gets the state.
    *
    * @return the state
    */
   public String getState()
   {
      return state;
   }
}
