package net.alantea.tools.lang;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import net.alantea.utils.exception.LntException;

/**
 * The Class Langage.
 */
public class Langage
{
   /** The managers map. */
   private CommandManager commandManager = new CommandManager();

   // TODO
   /** The exceptions. */
   private List<LangException> exceptions = new LinkedList<>();

   /** The variables map. */
   private Map<String, Object> variablesMap = new HashMap<>();
   
   /** The states. */
   private List<String> states = new ArrayList<>();
   
   /** The context fifo. */
   private LinkedList<LangageContext> contextFifo = new LinkedList<>();

   /** The state. */
   private LangageContext currentContext = null;
   
   /** The execution allowed. */
   private boolean executionAllowed = true;
   
   /** The if informations. */
   private Stack<IfInformations> ifInformations =  new Stack<IfInformations>();

   /**
    * Instantiates a new langage.
    */
   protected Langage()
   {
      addCommand("comment", this::comment);
      addCommand("true", this::trueFunction);
      addCommand("false", this::falseFunction);

   }

   /**
    * Adds the command.
    *
    * @param name the name
    * @param method the method
    */
   protected void addCommand(String name, Executor method)
   {
      commandManager.addCommand(name, method);
   }

   /**
    * Adds the alias.
    *
    * @param alias the alias
    * @param name the name
    */
   protected void addAlias(String alias, String name)
   {
      commandManager.addAlias(alias, name);
   }

   /**
    * Parse input from string.
    *
    * @param txt the txt
    * @return the execution report
    */
   public ExecutionReport parse(String txt)
   {
      return parse(txt, null);
   }

   /**
    * Parse input from string.
    *
    * @param txt the txt
    * @param runnable the runnable
    * @return the execution report
    */
   public ExecutionReport parse(String txt, AfterParserRunnable runnable)
   {
      return parse(new StringReader(txt), runnable);
   }

   /**
    * Parse input from file path.
    *
    * @param filepath the file path
    * @return the execution report
    */
   public ExecutionReport parseFile(String filepath)
   {
      return parse(filepath, null);
   }

   /**
    * Parse input from file path.
    *
    * @param filepath the file path
    * @param runnable the runnable
    * @return the execution report
    */
   public ExecutionReport parseFile(String filepath, AfterParserRunnable runnable)
   {
      return parse(new File(filepath), runnable);
   }

   /**
    * Parse input from file.
    *
    * @param file the file
    * @return the execution report
    */
   public ExecutionReport parse(File file)
   {
      return parse(file, null);
   }

   /**
    * Parse input from file.
    *
    * @param file the file
    * @param runnable the runnable
    * @return the execution report
    */
   public ExecutionReport parse(File file, AfterParserRunnable runnable)
   {
      ExecutionReport ret = null;
      if (file.exists())
      {
         String filePath = file.getAbsolutePath();
         try
         {
            FileReader reader = new FileReader(file);
            ret = parse(reader, runnable);
            return null;
         }
         catch (IOException e)
         {
            LangException exp = new LangException(-1, filePath, ErrorType.PARSINGERROR, "Unable to read file");
            exceptions.add(exp);
         }
      }
      return ret;
   }

   /**
    * Parse input from input stream.
    *
    * @param stream the stream
    * @return the execution report
    */
   public ExecutionReport parse(InputStream stream)
   {
      return parse(stream, null);
   }

   /**
    * Parse input from input stream.
    *
    * @param stream the stream
    * @param runnable the runnable
    * @return the execution report
    */
   public ExecutionReport parse(InputStream stream, AfterParserRunnable runnable)
   {
      return parse(new InputStreamReader(stream), null);
   }

   /**
    * Parse input from a reader.
    *
    * @param reader the reader
    * @return the execution report
    */
   public ExecutionReport parse(Reader reader)
   {
      return parse(reader, null);
   }

   /**
    * Parse input from a reader.
    *
    * @param reader the reader
    * @param runnable the runnable
    * @return the execution report
    */
   public ExecutionReport parse(Reader reader, AfterParserRunnable runnable)
   {
      ExecutionReport ret = null;
      BufferedReader bufreader = new BufferedReader(reader);

      exceptions.clear();
      String line = "";
      try
      {
         int numLine = 0;
         boolean cont = true;
         while (cont)
         {
            line = bufreader.readLine();
            if (line == null)
            {
               cont = false;
               break;
            }
            ret = analyseLine(numLine, line);
            if (runnable != null)
            {
               runnable.parse(ret);
            }
            numLine++;
         }
         bufreader.close();

      }
      catch (IOException e)
      {
         LangException exp = new LangException(-1, line, ErrorType.PARSINGERROR, "Unable to read file");
         exceptions.add(exp);
      }
      return ret;
   }
   
   /**
    * Adds the state in possible states list.
    *
    * @param newState the state to add
    */
   protected void addState(String newState)
   {
      if (!states.contains(newState))
      {
         states.add(newState);
      }
   }
   
   /**
    * Sets the state if in states list.
    *
    * @param newState the new state
    * @throws LangException the reading exception
    */
   protected void setState(String newState) throws LangException
   {
      if (states.contains(newState))
      {
         currentContext = new LangageContext(newState);
         contextFifo.addFirst(currentContext);
      }
      else
      {
         throw new LangException(0, "setState", ErrorType.BADSTATEERROR, "Unknown state : " + newState);
      }
   }
   
   /**
    * Replaces the state if in states list.
    *
    * @param newState the new state
    * @throws LangException the lang exception
    */
   protected void replaceState(String newState) throws LangException
   {
      if (states.contains(newState))
      {
         contextFifo.removeFirst();
         setState(newState);
      }
      else
      {
         throw new LangException(0, "replaceState", ErrorType.BADSTATEERROR, "Unknown state : " + newState);
      }
   }
   
   /**
    * Unsets the state if set.
    */
   protected void unsetState()
   {
      if (!contextFifo.isEmpty())
      {
         contextFifo.removeFirst();
      }
      if (!contextFifo.isEmpty())
      {
         currentContext = contextFifo.getFirst();
      }
      else
      {
         currentContext = null;
      }
   }
   
   /**
    * Gets the state.
    *
    * @return the state
    */
   protected String getState()
   {
      return (currentContext == null) ? "" : currentContext.getState();
   }
   
   /**
    * Tests the state.
    *
    * @param tested the tested
    * @return true if same state
    */
   protected boolean isState(String tested)
   {
      return getState().equals(tested);
   }
   
   /**
    * Sets the context.
    *
    * @param newState the new state
    * @return the context
    * @throws LangException the lang exception
    */
   protected LangageContext setContext(String newState) throws LangException
   {
      setState(newState);
      return currentContext;
   }
   
   /**
    * Gets the context.
    *
    * @return the context
    */
   protected LangageContext getContext()
   {
      return currentContext;
   }
   
   /**
    * Sets the context.
    *
    * @param newContext the new context
    * @throws LangException the lang exception
    */
   protected void setContext(LangageContext newContext) throws LangException
   {
      if (newContext != null)
      {
         currentContext = newContext;
         contextFifo.addFirst(currentContext);
      }
      else
      {
         throw new LangException(0, "setContext", ErrorType.BADCONTEXTERROR, "Null context");
      }
   }
   
   /**
    * Unsets the context if set.
    */
   protected void unsetContext()
   {
      unsetState();
   }
   
   
   /**
    * Adds an element to the context.
    *
    * @param key the key
    * @param element the element
    */
   protected void addContextElement(String key, Object element)
   {
      if (currentContext != null)
      {
         currentContext.put(key, element);
      }
   }
   
   /**
    * Removes an element to the context.
    *
    * @param key the key
    */
   protected void removeContextElement(String key)
   {
      if (currentContext != null)
      {
         currentContext.remove(key);
      }
   }
   
   /**
    * Gets an element from the context.
    *
    * @param key the key
    * @return the context element
    */
   protected Object getContextElement(String key)
   {
      if (currentContext != null)
      {
         return currentContext.get(key);
      }
      return null;
   }

   /**
    * Analyse line.
    *
    * @param num the num
    * @param currentline the theline
    * @return the execution report
    */
   ExecutionReport analyseLine(int num, String currentline)
   {
      if (currentline != null)
      {
         String theline = currentline.trim();
         if ((theline.startsWith("#")) || (theline.trim().isEmpty()))
         {
            return new CommentReport(num, theline);
         }
         else if (theline.startsWith("if "))
         {
            return executeIf(num, theline);
         }
         else if (theline.equals("endif"))
         {
            return executeEndif(num, theline);
         }
         else if (theline.equals("else"))
         {
            return executeElse(num, theline);
         }
         else if (executionAllowed)
         {
            return analyzeExecutableLine(num, theline);
         }
      }
      return null;
   }

   /**
    * Analyze executable line.
    *
    * @param num the num
    * @param line the line
    * @return the execution report
    */
   private ExecutionReport analyzeExecutableLine(int num, String  line)
   {
      String theline = line;
      String returnedVariableName = null;
      if (theline.startsWith("$"))
      {
         returnedVariableName = theline.substring(1, theline.indexOf(":")).trim();
         theline = theline.substring(theline.indexOf(":") + 1).trim();
      }
      try
      {
         ExecutionReport report = executeLine(num, theline);

         if ((returnedVariableName != null) && (report != null))
         {
            variablesMap.put(returnedVariableName, report.getReturnedValue());
         }
         return report;
      }
      catch (Exception e)
      {
         e.printStackTrace();
         return new ErrorReport(num, theline, ErrorType.PARSINGERROR, e.getMessage());
      }
   }
   
   /**
    * Execute line.
    *
    * @param num the num
    * @param line the line
    * @return the execution report
    */
   private ExecutionReport executeLine(int num, String  line)
   {
         String[] tokens = tokenizeLine(line);

         ExecutionReport report = null;
         Executor method = commandManager.getExecutor(tokens[0]);
         try
         {
            List<Object> arguments = new LinkedList<>();
            for (int i = 1; i < tokens.length; i++)
            {
               Object argument = tokens[i];
               if ((argument instanceof String) && (((String) argument).startsWith("$")))
               {
                  Object value = variablesMap.get(((String) argument).substring(1));
                  if (value != null)
                  {
                     arguments.add(value);
                  }
               }
               else
               {
                  arguments.add(argument);
               }
            }
            if (method != null)
            {
               report = execute(num, line, method, arguments);
            }
            else
            {
               report = new ErrorReport(num, line, ErrorType.BADCOMMANDERROR, "Bad command : " + line);
            }
         }
         catch (Exception e)
         {
            report = new ErrorReport(num, line, ErrorType.PARSINGERROR,
                  "Error executing command : " + line);
         }
         return report;
   }

   /**
    * Tokenize line.
    *
    * @param theline the theline
    * @return the string[]
    */
   private String[] tokenizeLine(String theline)
   {
      String[] ret = null;
      List<String> strings = new LinkedList<>();

      int start = 0;
      boolean insideSubstring = false;
      for (int i = 0; i < theline.length(); i++)
      {
         char currentChar = theline.charAt(i);
         if (currentChar == '"')
         {
            if (!insideSubstring)
            {
               insideSubstring = true;
            }
            else
            {
               strings.add(theline.substring(start, i));
               start = i + 1;
               insideSubstring = false;
               i++;
            }
            start = i + 1;
         }
         else if ((!insideSubstring) && (currentChar == ' '))
         {
            strings.add(theline.substring(start, i));
            start = i + 1;
         }
         else
         {
            // just continue
         }
      }
      if (start <= theline.length() - 1)
      {
         strings.add(theline.substring(start, theline.length()));
      }

      ret = strings.toArray(new String[0]);
      return ret;
   }

   /**
    * Execute.
    *
    * @param num the num
    * @param theline the theline
    * @param method the method
    * @param arguments the arguments
    * @return the execution report
    */
   private ExecutionReport execute(int num, String theline, Executor method, List<Object> arguments)
   {
      ExecutionReport report = null;
      try
      {
         Object object = method.manage(arguments);
         report = new SuccessReport(object);
      }
      catch (Exception e)
      {
         report = new ErrorReport(num, theline, ErrorType.EXECUTIONERROR, e.getMessage());
         e.printStackTrace();
      }
      return report;
   }
   
   /**
    * Execute if.
    *
    * @param num the num
    * @param line the line
    * @return the execution report
    */
   private ExecutionReport executeIf(int num, String line)
   {
      Object object = "Disallowed";
      
      if ((ifInformations.isEmpty() || ifInformations.peek().isExecutionAllowed()))
      {
         String theline = line.substring(3);
         if (line.matches("^if\\s+\\$\\S+$"))
         {
            object = variablesMap.get(theline.substring(theline.indexOf("$")).trim());
         }
         else
         {
            ExecutionReport report = executeLine(num, theline);
            object = report.getReturnedValue();
         }
         boolean lowerExecutionAllowed = executionAllowed;
         executionAllowed = validateIfIsAllowed(object);
         ifInformations.add(new IfInformations(executionAllowed, lowerExecutionAllowed));
      }
      else
      {
         ifInformations.add(new IfInformations(false, executionAllowed));
      }
      return new SuccessReport(object);
   }
   
   /**
    * Execute else.
    *
    * @param num the num
    * @param theline the theline
    * @return the execution report
    */
   private ExecutionReport executeElse(int num, String theline)
   {
      if ((ifInformations.isEmpty() || ifInformations.peek().isElseAllowed()))
      {
         executionAllowed = !executionAllowed;
      }
      return new SuccessReport("Else");
   }

   /**
    * Execute endif.
    *
    * @param num the num
    * @param theline the theline
    * @return the execution report
    */
   private ExecutionReport executeEndif(int num, String theline)
   {
      if (!ifInformations.isEmpty())
      {
         ifInformations.pop();
      }
      
      if (!ifInformations.isEmpty())
      {
         executionAllowed = ifInformations.peek().isExecutionAllowed();
      }
      else
      {
         executionAllowed = true;
      }
      return new SuccessReport("endif");
   }

   /**
    * Validate if is allowed.
    *
    * @param object the object
    * @return true, if successful
    */
   private boolean validateIfIsAllowed(Object object)
   {
      if (object instanceof Boolean)
      {
         return (boolean) object;
      }
      else 
      {
         return (object != null);
      }
   }

   /**
    * Load alias file.
    *
    * @param filepath the filepath
    */
   protected void loadAliasFile(String filepath)
   {
      try
      {
         InputStream resource = null;
         File file = new File(filepath);
         if ((file.exists()) && (file.isFile()))
         {
            resource = new FileInputStream(file);
         }
         else
         {
            resource = ClassLoader.getSystemClassLoader().getResourceAsStream(filepath);
         }

         if (resource != null)
         {
            BufferedReader reader = new BufferedReader(new InputStreamReader(resource));
            while (reader.ready())
            {
               String line = reader.readLine();
               if (line.contains("#"))
               {
                  line = line.substring(0, line.indexOf("#"));
               }
               if ((line != null) && (!line.trim().isEmpty()))
               {
                  String[] tokens = line.split("=", 2);
                  if (tokens.length > 1)
                  {
                     String name = tokens[0].trim();
                     String alias = tokens[1].trim();
                     addAlias(alias, name);
                  }
               }
            }
            reader.close();
         }
         else
            System.out.println("Error with : " + filepath);
      }
      catch (IOException e)
      {
         LangException exp = new LangException(-1, filepath, ErrorType.PARSINGERROR, "Unable to read alias file");
         exceptions.add(exp);
      }
   }

   /**
    * Comment.
    *
    * @param strings the strings
    * @return the object
    * @throws LntException the lnt exception
    */
   private Object comment(List<Object> strings) throws LntException
   {
      for (Object string : strings)
      {
         System.out.print(string);
         System.out.print(" ");
      }
      System.out.println();
      return null;

   }

   /**
    * True function.
    *
    * @param strings the strings
    * @return the object
    * @throws LntException the lnt exception
    */
   private Object trueFunction(List<Object> strings) throws LntException
   {
      return true;
   }

   /**
    * False function.
    *
    * @param strings the strings
    * @return the object
    * @throws LntException the lnt exception
    */
   private Object falseFunction(List<Object> strings) throws LntException
   {
      return false;
   }
   
   /**
    * The Class IfInformations.
    */
   private class IfInformations
   {
      
      /** The execution allowed. */
      private boolean executionAllowed;
      
      /** The else allowed. */
      private boolean elseAllowed;
      
      /**
       * Instantiates a new if informations.
       *
       * @param executionAllowed the execution allowed
       * @param elseAllowed the else allowed
       */
      public IfInformations(boolean executionAllowed, boolean elseAllowed)
      {
         this.executionAllowed = executionAllowed;
         this.elseAllowed = elseAllowed;
      }
      
      /**
       * Checks if is execution allowed.
       *
       * @return true, if is execution allowed
       */
      public boolean isExecutionAllowed()
      {
         return executionAllowed;
      }
      
      /**
       * Checks if is else allowed.
       *
       * @return true, if is else allowed
       */
      public boolean isElseAllowed()
      {
         return elseAllowed;
      }
   }
}
