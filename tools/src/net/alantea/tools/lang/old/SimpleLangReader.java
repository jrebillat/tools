package net.alantea.tools.lang.old;

/**
 * The Class SimpleLangReader.
 */
public abstract class SimpleLangReader extends LangReader
{
   
   /** The buffer. */
   StringBuffer buffer = new StringBuffer();
   
   /**
    * Read.
    *
    * @param txtpath the txtpath
    * @return the string
    */
   public String readLangFile(String txtpath)
   {
      super.read(txtpath);
      return  getHeader() + buffer.toString() + getFooter();
   }
   
   /**
    * Adds the to buffer.
    *
    * @param text the text
    */
   protected void addToBuffer(String text)
   {
      buffer.append(text);
   }

   /**
    * Gets the header.
    *
    * @return the header
    */
   protected abstract String getHeader();
   
   /**
    * Gets the footer.
    *
    * @return the footer
    */
   protected abstract String getFooter();
}
