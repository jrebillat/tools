package net.alantea.tools.lang.old;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.alantea.tools.lang.old.external.CommandResult;
import net.alantea.tools.lang.old.external.CommentResult;
import net.alantea.tools.lang.old.external.Context;
import net.alantea.tools.lang.old.external.ErrorResult;
import net.alantea.tools.lang.old.external.ErrorType;
import net.alantea.tools.lang.old.external.ExecutionReport;
import net.alantea.tools.lang.old.external.ExecutionResult;
import net.alantea.tools.lang.old.external.ManagerError;
import net.alantea.tools.lang.old.external.ManagerMethod;
import net.alantea.tools.lang.old.external.ParserResult;
import net.alantea.tools.lang.old.external.ParserRunnable;
import net.alantea.tools.lang.old.external.ParsingException;
import net.alantea.tools.lang.old.internal.StoredManager;

// TODO: Auto-generated Javadoc
/**
 * The Class LangReader.
 */
public class LangReader
{

   /** The states. */
   private List<String> states = new ArrayList<>();
   
   /** The managers map. */
   private  Map<String, StoredManager> managersMap = new HashMap<>();
   
   /** The aliases map. */
   private  Map<String, String> aliasMap = new HashMap<>();

   /** The exceptions. */
   private List<ParsingException> exceptions = new LinkedList<>();
   
   /** The context fifo. */
   private LinkedList<Context> contextFifo = new LinkedList<>();
   
   /** The file name. */
   private String fileName;
   
   /** The file path. */
   private String filePath;
   
   /** The state. */
   private Context context = null;

   /** The num line. */
   private int numLine;

   /** The line. */
   private String line;

   /** The variables map. */
   private Map<String, Object> variablesMap = new HashMap<>();
   
   /**
    * Instantiates a new lang reader.
    */
   protected LangReader()
   {
      addState("TOP");
      try
      {
         setContext("TOP");
      }
      catch (ParsingException e)
      {
      }
   }
   
   /**
    * Adds the command.
    *
    * @param name the name
    * @param nbargs the arguments number
    * @param method the method
    * @param options the options
    */
   protected void addCommand(String name, int nbargs, ManagerMethod method, String... options)
   {
      addCommand(name, nbargs, nbargs, method, options);
   }
   
   /**
    * Adds the command.
    *
    * @param name the name
    * @param method the method
    * @param options the options
    */
   protected void addCommand(String name, ManagerMethod method, String... options)
   {
      addCommand(name, 0, Integer.MAX_VALUE, method, options);
   }
   
   /**
    * Adds the command.
    *
    * @param name the name
    * @param nbargsMin the nbargs min
    * @param nbargsMax the nbargs max
    * @param method the method
    * @param options the options
    */
   protected void addCommand(String name, int nbargsMin, int nbargsMax, ManagerMethod method, String... options)
   {
      String key = name.trim().toLowerCase();
      String text = null;
      if (key.contains(" "))
      {
         String[] tokens = key.split(" ", 2 );
         key = tokens[0];
         text = (tokens.length ==1) ? "" : tokens[1];
      }
      StoredManager manager = managersMap.get(key);
      if (manager == null)
      {
         manager = new StoredManager();
         managersMap.put(key,  manager);
      }
      manager.addMethod(method, text, nbargsMin, nbargsMax, options);
   }
   
   /**
    * Adds the alias.
    *
    * @param alias the alias
    * @param name the name
    */
   protected void addAlias(String alias, String name)
   {
      aliasMap.put(alias, name);
   }

   /**
    * Load alias file.
    *
    * @param filepath the filepath
    */
   protected void loadAliasFile(String filepath)
   {
      try
      {
         InputStream resource = null;
         File file = new File(filepath);
         if ((file.exists()) && (file.isFile()))
         {
            resource = new FileInputStream(file);
         }
         else
         {
            resource = ClassLoader.getSystemClassLoader().getResourceAsStream(filepath);
         }
         
         if (resource != null)
         {
            BufferedReader reader = new BufferedReader(new InputStreamReader(resource));
            while (reader.ready())
            {
               String line = reader.readLine();
               if (line.contains("#"))
               {
                  line = line.substring(0, line.indexOf("#"));
               }
               if ((line != null) && (!line.trim().isEmpty()))
               {
                  String[] tokens = line.split("=", 2);
                  if (tokens.length > 1)
                  {
                     String name = tokens[0].trim();
                     String alias = tokens[1].trim();
                     addAlias(alias, name);
                  }
               }
            }
            reader.close();
         }
         else
            System.out.println("erreur avec : " + filepath);
      }
      catch (IOException e)
      {
         ParsingException exp = new ParsingException(-1, filePath, ErrorType.PARSINGERROR, "Unable to read alias file");
         exceptions.add(exp);
      }
   }
   
   /**
    * Adds the state in possible states list.
    *
    * @param newState the state to add
    */
   protected void addState(String newState)
   {
      if (!states.contains(newState))
      {
         states.add(newState);
      }
   }
   
   /**
    * Sets the state if in states list.
    *
    * @param newState the new state
    * @throws ParsingException the reading exception
    */
   protected void setState(String newState) throws ParsingException
   {
      if (states.contains(newState))
      {
         context = new Context(newState);
         contextFifo.addFirst(context);
      }
      else
      {
         throw new ParsingException(numLine, line, ErrorType.BADSTATEERROR, "Unknown state : " + newState);
      }
   }
   
   /**
    * Replaces the state if in states list.
    *
    * @param newState the new state
    * @throws ParsingException the reading exception
    */
   protected void replaceState(String newState) throws ParsingException
   {
      if (states.contains(newState))
      {
         contextFifo.removeFirst();
         setState(newState);
      }
      else
      {
         throw new ParsingException(numLine, line, ErrorType.BADSTATEERROR, "Unknown state : " + newState);
      }
   }
   
   /**
    * Unsets the state if set.
    */
   protected void unsetState()
   {
      if (!contextFifo.isEmpty())
      {
         contextFifo.removeFirst();
      }
      if (!contextFifo.isEmpty())
      {
         context = contextFifo.getFirst();
      }
      else
      {
         context = null;
      }
   }
   
   /**
    * Gets the state.
    *
    * @return the state
    */
   protected String getState()
   {
      return (context == null) ? "" : context.getState();
   }
   
   /**
    * Tests the state.
    *
    * @param tested the tested
    * @return true if same state
    */
   protected boolean isState(String tested)
   {
      return getState().equals(tested);
   }
   
   /**
    * Sets the context.
    *
    * @param newState the new state
    * @return the context
    * @throws ParsingException the reading exception
    */
   protected Context setContext(String newState) throws ParsingException
   {
      setState(newState);
      return context;
   }
   
   /**
    * Gets the context.
    *
    * @return the context
    */
   protected Context getContext()
   {
      return context;
   }
   
   /**
    * Sets the context.
    *
    * @param newContext the new context
    * @throws ParsingException the reading exception
    */
   protected void setContext(Context newContext) throws ParsingException
   {
      if (newContext != null)
      {
         context = newContext;
         contextFifo.addFirst(context);
      }
      else
      {
         throw new ParsingException(numLine, line, ErrorType.BADCONTEXTERROR, "Null context");
      }
   }
   
   /**
    * Unsets the context if set.
    */
   protected void unsetContext()
   {
      unsetState();
   }
   
   
   /**
    * Adds an element to the context.
    *
    * @param key the key
    * @param element the element
    */
   protected void addContextElement(String key, Object element)
   {
      if (context != null)
      {
         context.put(key, element);
      }
   }
   
   /**
    * Removes an element to the context.
    *
    * @param key the key
    */
   protected void removeContextElement(String key)
   {
      if (context != null)
      {
         context.remove(key);
      }
   }
   
   /**
    * Gets an element from the context.
    *
    * @param key the key
    * @return the context element
    */
   protected Object getContextElement(String key)
   {
      if (context != null)
      {
         return context.get(key);
      }
      return null;
   }
   
   /**
    * Read input from string.
    *
    * @param txt the txt
    */
   public void read(String txt)
   {
      parse(new StringReader(txt), this::manageRead);
   }
   
   /**
    * Read input from file path.
    *
    * @param filepath the file path
    */
   public void readFile(String filepath)
   {
      parseFile(filepath, this::manageRead);
   }
   
   /**
    * Read input from file.
    *
    * @param file the file
    */
   public void read(File file)
   {
      parse(file, this::manageRead);
   }
   
   /**
    * Read input from input stream.
    *
    * @param stream the stream
    */
   public void read(InputStream stream)
   {
      parse(stream, this::manageRead);
   }
   
   /**
    * Read input from a reader.
    *
    * @param reader the reader
    */
   public void read(Reader reader)
   {
      parse(reader, this::manageRead);
   }
   
   /**
    * Manage read.
    *
    * @param result the result
    * @return the execution report
    */
   private ExecutionReport manageRead(ParserResult result)
   {
      ExecutionReport report = null;
      if (result instanceof CommandResult)
      {
         try
         {
            CommandResult command = (CommandResult) result;
            Object object = command.getMethod().manage(command.getManager(), command.getArguments());
            if (object != null)
            {
               if ((result != null) && (object instanceof ManagerError))
               {
                  ManagerError error = (ManagerError) object;
                  ParsingException exp = new ParsingException(numLine, line, error.getErrorType(), error.getReason());
                  exceptions.add(exp);
               }
            }
            if (!(object instanceof ExecutionReport))
            {
               report = new ExecutionResult(object);
            }
         }
         catch (Exception e)
         {
            e.printStackTrace();
            ParsingException exp = new ParsingException(numLine, line, ErrorType.PARSINGERROR, e.getMessage()); 
            exceptions.add(exp);
         }
      }
      else if (result instanceof ErrorResult)
      {
         exceptions.add(((ErrorResult)result).getException());
      }
      return report;
   }
   
   /**
    * Parse input from string.
    *
    * @param txt the txt
    * @param runnable the runnable
    * @return the execution report
    */
   public ExecutionReport parse(String txt, ParserRunnable runnable)
   {
      return parse(new StringReader(txt), runnable);
   }
   
   /**
    * Parse input from file path.
    *
    * @param filepath the file path
    * @param runnable the runnable
    * @return the execution report
    */
   public ExecutionReport parseFile(String filepath, ParserRunnable runnable)
   {
      return parse(new File(filepath), runnable);
   }
   
   /**
    * Parse input from file.
    *
    * @param file the file
    * @param runnable the runnable
    * @return the execution report
    */
   public ExecutionReport parse(File file, ParserRunnable runnable)
   {
      ExecutionReport ret = null;
      if (file.exists())
      {
         fileName = file.getName();
         filePath = file.getAbsolutePath();
         try
         {
            FileReader reader = new FileReader(file);
            ret = parse(reader, runnable);
            return null;
         }
         catch (IOException e)
         {
            ParsingException exp = new ParsingException(-1, filePath, ErrorType.PARSINGERROR, "Unable to read file");
            exceptions.add(exp);
         }
      }
      return ret;
   }
   
   /**
    * Parse input from input stream.
    *
    * @param stream the stream
    * @param runnable the runnable
    * @return the execution report
    */
   public ExecutionReport parse(InputStream stream, ParserRunnable runnable)
   {
      return parse(new InputStreamReader(stream), runnable);
   }
   
   /**
    * Parse input from a reader.
    *
    * @param reader the reader
    * @param runnable the runnable
    * @return the execution report
    */
   public ExecutionReport parse(Reader reader, ParserRunnable runnable)
   {
      ExecutionReport ret = null;
      BufferedReader bufreader = new BufferedReader(reader);

      exceptions.clear();
      try
      {
         numLine = 0;
         boolean cont = true;
         while (cont)
         {
            line = bufreader.readLine();
            if (line == null)
            {
               cont = false;
               break;
            }
            ret = analyseLine(numLine, line, runnable);
            numLine++;
         }
         bufreader.close();

      }
      catch (IOException e)
      {
         ParsingException exp = new ParsingException(-1, filePath, ErrorType.PARSINGERROR, "Unable to read file");
         exceptions.add(exp);
      }
      return ret;
   }

//   /**
//    * Read concatenated lines.
//    *
//    * @param reader the reader
//    * @return the string
//    * @throws IOException Signals that an I/O exception has occurred.
//    */
//   private String readConcatenatedLines(BufferedReader reader) throws IOException
//   {
//      String theline = reader.readLine();
//      while ((theline != null) && (theline.endsWith("\\")) && (reader.ready()))
//      {
//         String newLine = reader.readLine();
//         if (newLine != null)
//         {
//            theline = theline.substring(0, theline.length() - 1) + " " + newLine;
//         }
//      }
//      return theline;
//   }
   
   /**
 * Analyse line.
 *
 * @param num the num
 * @param currentline the theline
 * @param runnable the runnable
 * @return the execution report
 */
   protected ExecutionReport analyseLine(int num, String currentline, ParserRunnable runnable)
   {
      if (currentline != null)
      {
         String theline = currentline.trim();
         if ((theline.startsWith("#")) || (theline.trim().isEmpty()))
         {
            runnable.parse(new CommentResult(num, theline));
         }
         else
         {
            String returnedVariableName = null;
            if (theline.startsWith("$"))
            {
               returnedVariableName = theline.substring(1, theline.indexOf(":")).trim();
               theline = theline.substring(theline.indexOf(":") + 1).trim();
            }
            try
            {
               String[] tokens = theline.split("[\\s]+", 2);
               String args = "";
               if (tokens.length > 1)
               {
                  args = tokens[1].trim();
               }

               ExecutionReport report = null;
               StoredManager manager = managersMap.get(tokens[0].toLowerCase());
               if (manager != null)
               {
                  ManagerMethod method = null;
                  try
                  {
                     method = manager.getMethod(args);
                     List<Object> arguments = manager.getArguments(args);
                     for (int i = 0; i < arguments.size(); i++)
                     {
                        Object argument = arguments.get(i);
                        if ((argument instanceof String) && (((String) argument).startsWith("$")))
                        {
                           Object value = variablesMap.get(((String) argument).substring(1));
                           if (value != null)
                           {
                              arguments.set(i, value);
                           }
                        }
                     }
                     if (method != null)
                     {
                        report = runnable.parse(new CommandResult(num, theline, manager.getManager(args), method, arguments));
                     }
                     else
                     {
                        report = runnable.parse(new ErrorResult(num, theline, new ParsingException(numLine, line, ErrorType.BADCOMMANDERROR, "Bad command : " + line)));
                     }
                  }
                  catch (ParsingException e)
                  {
                     report = runnable.parse(new ErrorResult(num, theline, new ParsingException(numLine, line, ErrorType.BADARGSNUMBERERROR, "Bad arguments number : " + line)));
                  }
               }
               else
               {
                  report = runnable.parse(new ErrorResult(num, theline, new ParsingException(numLine, line, ErrorType.UNKNOWNCOMMANDERROR, "Unknown command : " + tokens[0])));
               }
               if ((returnedVariableName != null) && (report != null))
               {
                  variablesMap.put(returnedVariableName, report.getReturnedValue());
               }
               return report;
            }
            catch(Exception e)
            {
               e.printStackTrace();
               runnable.parse(new ErrorResult(num, theline, new ParsingException(numLine, line, ErrorType.PARSINGERROR, e.getMessage())));
            }
         }
      }
      return null;
   }
   
   /**
    * Gets the file name.
    *
    * @return the file name
    */
   protected final String getFileName()
   {
      return fileName;
   }
   
   /**
    * Gets the file path.
    *
    * @return the file path
    */
   protected final String getFilePath()
   {
      return filePath;
   }
   
   /**
    * Gets the line number.
    *
    * @return the line number
    */
   protected final int getLineNumber()
   {
      return numLine;
   }
   
   /**
    * Gets the line content.
    *
    * @return the line content
    */
   protected final String getLine()
   {
      return line;
   }
   
   /**
    * Gets the exceptions.
    *
    * @return the exceptions
    */
   public final List<ParsingException> getExceptions()
   {
      return exceptions;
   }
   
   /**
    * Read args.
    *
    * @param args the args
    * @return the string[]
    */
   protected static final List<Object> readArgs(String args)
   {
      if ((args == null) || (args.trim().isEmpty()))
      {
         return new LinkedList<Object>();
      }
      return splitArgs(args);
   }
   
   /**
    * Split args.
    *
    * @param args the args
    * @return the string[]
    */
   public static final List<Object> splitArgs(String args)
   {
      List<Object> list = new ArrayList<Object>();
      Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(args);
      while (m.find())
      {
         String found = m.group(1);
         if ((found.startsWith("\"")) && (found.endsWith("\"")))
         {
            found = found.substring(1, found.length() - 1);
         }
          list.add(found);
      }
      return list;
   }
}
