package net.alantea.tools.lang.old.external;

/**
 * The Class ExecutionResult.
 */
public class ExecutionResult implements ExecutionReport
{
   
   /** The content. */
   private Object content;

   /**
    * Instantiates a new ExecutionResult.
    *
    * @param content the content
    */
   public ExecutionResult(Object content)
   {
      this.content = content;
   }

   /**
    * Gets the returned value.
    *
    * @return the returned value
    */
   @Override
   public Object getReturnedValue()
   {
      return content;
   }
}
