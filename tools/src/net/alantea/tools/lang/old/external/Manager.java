package net.alantea.tools.lang.old.external;

import java.util.List;
import java.util.Map;

import net.alantea.tools.lang.old.internal.StoredManager;

/**
 * The Class Manager.
 */
public class Manager
{
   
   /** The stored manager. */
   private StoredManager storedManager;
   
   /** The key. */
   private String key;
   
   /**
    * Instantiates a new manager.
    *
    * @param storedManager the stored manager
    * @param key the key
    */
   public Manager(StoredManager storedManager, String key)
   {
      super();
      this.storedManager = storedManager;
      this.key = key;
   }
   
   /**
    * Gets the arguments map.
    *
    * @param argsList the args list
    * @return the arguments map
    */
   public Map<String, String> getArgumentsMap(List<String> argsList)
   {
      return storedManager.getArgumentsMap(key, argsList);
   }
}
