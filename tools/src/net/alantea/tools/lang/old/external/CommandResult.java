package net.alantea.tools.lang.old.external;

import java.util.List;

/**
 * The Class CommandResult.
 */
public class CommandResult extends ParserResult
{
   
   /** The Constant TYPE. */
   public static final String TYPE = "CommandResult";
   
   /** The method. */
   private ManagerMethod method;

   /** The arguments. */
   private List<Object> arguments;

   /** The manager. */
   private Manager manager;
   
   /**
    * Instantiates a new command result.
    *
    * @param num the num
    * @param line the line
    * @param manager the manager
    * @param method the method
    * @param arguments the arguments
    */
   public CommandResult(int num, String line, Manager manager, ManagerMethod method, List<Object> arguments)
   {
      super(num, line);
      this.manager = manager;
      this.method = method;
      this.arguments = arguments;
   }

   /**
    * Gets the type.
    *
    * @return the type
    */
   @Override
   public final String getType()
   {
      return TYPE;
   }

   /**
    * Gets the method.
    *
    * @return the method
    */
   public ManagerMethod getMethod()
   {
      return method;
   }

   /**
    * Gets the arguments.
    *
    * @return the arguments
    */
   public List<Object> getArguments()
   {
      return arguments;
   }

   /**
    * Gets the manager.
    *
    * @return the manager
    */
   public Manager getManager()
   {
      return manager;
   }

}
