package net.alantea.tools.lang.old.external;

/**
 * The Interface ParserRunnable.
 */
@FunctionalInterface
public interface ParserRunnable
{
    
    /**
     * Parses the.
     *
     * @param line the line
     * @return the execution report
     */
    public ExecutionReport parse(ParserResult line);
}
