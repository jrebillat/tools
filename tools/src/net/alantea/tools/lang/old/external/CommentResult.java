package net.alantea.tools.lang.old.external;

/**
 * The Class CommentResult.
 */
public class CommentResult extends ParserResult
{
   
   /** The Constant TYPE. */
   public static final String TYPE = "CommentResult";
   
   /**
    * Instantiates a new comment result.
    *
    * @param num the num
    * @param line the line
    */
   public CommentResult(int num, String line)
   {
      super(num, line);
   }

   /**
    * Gets the type.
    *
    * @return the type
    */
   @Override
   public final String getType()
   {
      return TYPE;
   }

}
