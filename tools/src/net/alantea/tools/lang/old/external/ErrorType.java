package net.alantea.tools.lang.old.external;

/**
 * The Enum ErrorType.
 */
public enum ErrorType
{
   
   /** The badcommanderror. */
   BADCOMMANDERROR,
   
   /** The unknowncommanderror. */
   UNKNOWNCOMMANDERROR,
   
   /** The parsingerror. */
   PARSINGERROR,
   
   /** The badargsnumbererror. */
   BADARGSNUMBERERROR,
   
   /** The badargumenterror. */
   BADARGUMENTERROR,
   
   /** The badstateerror. */
   BADSTATEERROR,
   
   /** The badcontexterror. */
   BADCONTEXTERROR
}
