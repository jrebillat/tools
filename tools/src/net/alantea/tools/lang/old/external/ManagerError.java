package net.alantea.tools.lang.old.external;

/**
 * The Class ManagerError.
 */
public class ManagerError implements ExecutionReport
{
   
   /** The error type. */
   private ErrorType errorType;
   
   /** The reason. */
   private String reason;

   /**
    * Instantiates a new manager error.
    *
    * @param errorType the error type
    * @param reason the reason
    */
   public ManagerError(ErrorType errorType, String reason)
   {
      this.errorType = errorType;
      this.reason = reason;
   }

   /**
    * Gets the error type.
    *
    * @return the error type
    */
   public ErrorType getErrorType()
   {
      return errorType;
   }

   /**
    * Gets the reason.
    *
    * @return the reason
    */
   public String getReason()
   {
      return reason;
   }

   @Override
   public Object getReturnedValue()
   {
      return null;
   }
}
