package net.alantea.tools.lang.old.external;

/**
 * The Class ParserResult.
 */
public abstract class ParserResult
{
   
   /** The line number. */
   private int lineNumber;
   
   /** The line. */
   private String line;

   /**
    * Instantiates a new parser result.
    *
    * @param num the num
    * @param line the line
    */
   public ParserResult(int num, String line)
   {
      this.lineNumber = num;
      this.line = line;
   }
   
   /**
    * Gets the type.
    *
    * @return the type
    */
   public abstract String getType();

   /**
    * Gets the line number.
    *
    * @return the line number
    */
   public int getLineNumber()
   {
      return lineNumber;
   }

   /**
    * Gets the line.
    *
    * @return the line
    */
   public String getLine()
   {
      return line;
   }
}
