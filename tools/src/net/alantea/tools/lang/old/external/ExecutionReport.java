package net.alantea.tools.lang.old.external;

public interface ExecutionReport
{
   public Object getReturnedValue();
}
