package net.alantea.tools.lang.old.external;

/**
 * The Class ErrorResult.
 */
public class ErrorResult extends ParserResult
{
   
   /** The Constant TYPE. */
   public static final String TYPE = "ErrorResult";
   
   /** The exception. */
   private ParsingException exception;
   
   /**
    * Instantiates a new error result.
    *
    * @param num the num
    * @param line the line
    * @param exception the exception
    */
   public ErrorResult(int num, String line, ParsingException exception)
   {
      super(num, line);
      this.exception = exception;
   }
   
   /**
    * Gets the exception.
    *
    * @return the exception
    */
   public ParsingException getException()
   {
      return exception;
   }

   /**
    * Gets the type.
    *
    * @return the type
    */
   @Override
   public final String getType()
   {
      return TYPE;
   }

   /**
    * Gets the error type.
    *
    * @return the error type
    */
   public ErrorType getErrorType()
   {
      return (exception == null) ? ErrorType.PARSINGERROR : exception.getErrorType();
   }

}
