package net.alantea.tools.lang.old.external;

import java.util.List;

/**
 * The Interface ManagerMethod.
 */
//-------------------------------------------------------------------------
@FunctionalInterface
public interface ManagerMethod
{
   
   /**
    * Manage.
    *
    * @param manager the manager
    * @param input the input
    * @return the error or null
    * @throws Exception the exception
    */
   public Object manage(Manager manager, List<Object> input) throws Exception;
}