package net.alantea.tools.lang.old.internal;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.alantea.tools.lang.old.LangReader;
import net.alantea.tools.lang.old.external.ErrorType;
import net.alantea.tools.lang.old.external.Manager;
import net.alantea.tools.lang.old.external.ManagerMethod;
import net.alantea.tools.lang.old.external.ParsingException;

/**
 * The Class StoredManager.
 */
public class StoredManager
{
   /** The methods. */
   Map<String, ManagerMethod> methodsMap = new HashMap<>();

   /** The options. */
   Map<String, Options> optionsMap = new HashMap<>();

   /** The max args. */
   int maxSpecs = 0;

   /**
    * Adds the method.
    *
    * @param method the method
    * @param spec the spec
    * @param nbargsMin the nbargs min
    * @param nbargsMax the nbargs max
    * @param args the args
    */
   public void addMethod(ManagerMethod method, String spec, int nbargsMin, int nbargsMax, String... args)
   {
      int count = 0;
      if ((spec != null) && (!spec.isEmpty()))
      {
         count = 1 + (int) spec.chars().filter(ch -> ch == ' ').count();
      }
      maxSpecs = Math.max(maxSpecs, count);
      String key = (spec == null) ? "" : spec;
      methodsMap.put(key, method);
      optionsMap.put(key, new Options(nbargsMin, nbargsMax, args));
   }

   /**
    * Gets the method.
    *
    * @param args the args
    * @return the method
    */
   public ManagerMethod getMethod(String args)
   {
      List<String> splitted = Arrays.asList(args.split(" ", maxSpecs + 1));
      for (int i = Math.min(maxSpecs, splitted.size()); i > 0; i--)
      {
         String key = String.join(" ", splitted.subList(0, i).toArray(new String[0]));
         ManagerMethod method = methodsMap.get(key);
         if (method != null)
         {
            return method;
         }
      }
      return methodsMap.get("");
   }

   /**
    * Gets the method.
    *
    * @param args the args
    * @return the method
    */
   public Manager getManager(String args)
   {
      List<String> splitted = Arrays.asList(args.split(" ", maxSpecs + 1));
      for (int i = Math.min(maxSpecs, splitted.size()); i > 0; i--)
      {
         String key = String.join(" ", splitted.subList(0, i).toArray(new String[0]));
         ManagerMethod method = methodsMap.get(key);
         if (method != null)
         {
            return new Manager(this, key);
         }
      }
      return new Manager(this, "");
   }

   /**
    * Gets the arguments.
    *
    * @param args the args
    * @return the arguments
    * @throws ParsingException the parsing exception
    */
   public List<Object> getArguments(String args) throws ParsingException
   {
      List<Object> splitted = LangReader.splitArgs(args);
      return getArguments(splitted);
   }

   /**
    * Gets the arguments.
    *
    * @param splitted the splitted
    * @return the arguments
    * @throws ParsingException the parsing exception
    */
   List<Object> getArguments(List<Object> splitted) throws ParsingException
   {
      List<Object> ret = splitted;
      Options options = null;

      options = optionsMap.get("");

      for (int i = splitted.size(); i > 0; i--)
      {
         String key = String.join(" ", splitted.subList(0, i).toArray(new String[0]));
         ManagerMethod bettermethod = methodsMap.get(key);
         Options betteroptions = optionsMap.get(key);

         if (bettermethod != null)
         {
            options = betteroptions;
            Object[] argsList = Arrays.copyOfRange(splitted.toArray(new String[0]), i, splitted.size());
            if ((options != null) && (options.getOpts().length > 0))
            {
               List<String> optList = Arrays.asList(options.getOpts());
               Map<String, String> optValuesMap = new HashMap<>();
               List<String> unopts = new LinkedList<>();
               boolean stepOver = false;
               String optKey = null;

               // TODO manage number of args
               for (Object arg : argsList)
               {
                  if (arg instanceof String)
                  {
                     String str = (String) arg;
                     if (stepOver)
                     {
                        optValuesMap.put(optKey, str);
                        stepOver = false;
                     }
                     else if (optList.contains(str))
                     {
                        optKey = str;
                        stepOver = true;
                     }
                     else
                     {
                        unopts.add(str);
                     }
                  }
               }

               List<Object> joined = new LinkedList<>();
               int n = 0;
               for (String opt : optList)
               {
                  String val = optValuesMap.get(opt);
                  if (val == null)
                  {
                     val = unopts.get(n++);
                  }
                  joined.add(val);
               }
               ret = joined;
               if (unopts.size() > n)
               {
                  throw new ParsingException(-1, String.join(" ", splitted.toArray(new String[0])),
                        ErrorType.BADARGSNUMBERERROR, "Bad arguments number");
               }
               break;
            }
            else
            {
               ret = Arrays.asList(argsList);
               break;
            }
         }
      }

      if (options == null)
      {
         throw new ParsingException(-1, String.join(" ", splitted.toArray(new String[0])), ErrorType.BADARGSNUMBERERROR,
               "Bad arguments number");
      }

      int nbargs = ret.size();
      int argsMin = (options == null) ? 0 : options.getNbArgsMin();
      int argsMax = (options == null) ? 0 : options.getNbArgsMax();
      if ((nbargs < argsMin) || (nbargs > argsMax))
      {
         throw new ParsingException(-1, String.join(" ", splitted.toArray(new String[0])), ErrorType.BADARGSNUMBERERROR,
               "Bad argumnts number");
      }
      return ret;
   }

   /**
    * Gets the arguments map.
    *
    * @param key the key
    * @param argsList the args list
    * @return the arguments map
    */
   public Map<String, String> getArgumentsMap(String key, List<String> argsList)
   {
      String[] options = optionsMap.get(key).getOpts();
      List<String> optList = Arrays.asList(options);
      Map<String, String> optValuesMap = new HashMap<>();
      List<String> unopts = new LinkedList<>();
      boolean stepOver = false;
      String optKey = null;

      for (String arg : argsList)
      {
         if (stepOver)
         {
            optValuesMap.put(optKey, arg);
            stepOver = false;
         }
         else if (optList.contains(arg))
         {
            optKey = arg;
            stepOver = true;
         }
         else
         {
            unopts.add(arg);
         }
      }

      Map<String, String> map = new HashMap<>();
      int n = 0;
      for (String opt : optList)
      {
         String val = optValuesMap.get(opt);
         if (val == null)
         {
            val = unopts.get(n++);
         }
         map.put(opt, val);
      }
      return map;
   }

   /**
    * The Class Options.
    */
   private class Options
   {

      /** The opts. */
      private String[] opts;

      /** The nbargs min. */
      private int nbargsMin;

      /** The nb args max. */
      private int nbArgsMax;

      /**
       * Instantiates a new options.
       *
       * @param nbargsMin the nbargs min
       * @param nbArgsMax the nb args max
       * @param opts the opts
       */
      public Options(int nbargsMin, int nbArgsMax, String[] opts)
      {
         super();
         this.opts = opts;
         this.nbargsMin = nbargsMin;
         this.nbArgsMax = nbArgsMax;
      }

      /**
       * Gets the opts.
       *
       * @return the opts
       */
      public String[] getOpts()
      {
         return opts;
      }

      /**
       * Gets the nb args min.
       *
       * @return the nb args min
       */
      public int getNbArgsMin()
      {
         return nbargsMin;
      }

      /**
       * Gets the nb args max.
       *
       * @return the nb args max
       */
      public int getNbArgsMax()
      {
         return nbArgsMax;
      }
   }
}