package net.alantea.tools.lang;

/**
 * The Interface ParserRunnable.
 */
@FunctionalInterface
public interface AfterParserRunnable
{
    
    /**
     * Parses the report.
     *
     * @param report the report
     * @return the execution report
     */
    public ExecutionReport parse(ExecutionReport report);
}
