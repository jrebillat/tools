package net.alantea.tools.lang;

import net.alantea.utils.exception.LntException;

public class LangException extends LntException
{
   
   /**
    * 
    */
   private static final long serialVersionUID = 1L;

   /** The num line. */
   private int lineNumber;
   
   /** The line. */
   private String lineContent;
   
   /** The exception. */
   private String exception;

   /** The error type. */
   private ErrorType errorType;
   
   /**
    * Instantiates a new reading exception.
    *
    * @param numLine the num line
    * @param line the line
    * @param type the type
    * @param exception the exception text
    */
   public LangException(int numLine, String line, ErrorType type, String exception)
   {
      super((exception == null) ? ("Error reading line number " + numLine + "content : '" + line + "'") : exception);
      this.lineNumber = numLine;
      this.lineContent = line;
      this.errorType = type;
      this.exception = exception;
   }

   /**
    * Gets the num line.
    *
    * @return the num line
    */
   public int getNumLine()
   {
      return lineNumber;
   }

   /**
    * Gets the line.
    *
    * @return the line
    */
   public String getLine()
   {
      return lineContent;
   }

   /**
    * Gets the exception.
    *
    * @return the exception
    */
   public String getException()
   {
      return exception;
   }

   /**
    * Gets the error type.
    *
    * @return the error type
    */
   public ErrorType getErrorType()
   {
      return errorType;
   }

}
