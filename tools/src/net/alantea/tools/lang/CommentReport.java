package net.alantea.tools.lang;

/**
 * The Class CommentReport.
 */
public class CommentReport implements ExecutionReport
{
   
   /** The num line. */
   int numLine;
   
   /** The line. */
   String line;
   
   /**
    * Instantiates a new comment report.
    *
    * @param numLine the num line
    * @param line the line
    */
   public CommentReport(int numLine, String line)
   {
      this.numLine = numLine;
      this.line = line;
   }

   /**
    * Gets the returned value.
    *
    * @return the returned value
    */
   @Override
   public Object getReturnedValue()
   {
      return line;
   }
   
   /**
    * Gets the line number.
    *
    * @return the line number
    */
   public int getLineNumber()
   {
      return numLine;
   }

}
