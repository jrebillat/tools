package net.alantea.tools.lang;

import java.util.List;

@FunctionalInterface
public interface Executor
{
   
   /**
    * Manage.
    *
    * @param input the input
    * @return the error or null
    * @throws Exception the exception
    */
   public Object manage(List<Object> input) throws Exception;
}
