package net.alantea.tools.lang;

/**
 * The Enum ErrorType.
 */
public enum ErrorType
{
   
   /** The badcommanderror. */
   BADCOMMANDERROR,
   
   /** The unknowncommanderror. */
   UNKNOWNCOMMANDERROR,
   
   /** The parsingerror. */
   PARSINGERROR,
   
   /** The executionerror. */
   EXECUTIONERROR,
   
   /** The badargsnumbererror. */
   BADARGSNUMBERERROR,
   
   /** The badargumenterror. */
   BADARGUMENTERROR,
   
   /** The badstateerror. */
   BADSTATEERROR,
   
   /** The badcontexterror. */
   BADCONTEXTERROR
}
