package net.alantea.tools.lang;

public class SuccessReport implements ExecutionReport
{
   Object value;

   public SuccessReport(Object value)
   {
      this.value = value;
   }

   @Override
   public Object getReturnedValue()
   {
      return value;
   }

}
