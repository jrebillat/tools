package net.alantea.tools.lang;

import java.util.HashMap;
import java.util.Map;

public class CommandManager
{
   
   /** The commands map. */
   private  Map<String, Executor> commandsMap = new HashMap<>();
   
   /** The aliases map. */
   private  Map<String, String> aliasMap = new HashMap<>();
   
   /**
    * Adds the command.
    *
    * @param name the name
    * @param method the method
    */
   void addCommand(String name, Executor method)
   {
      if ((name == null) || (method == null))
      {
         return;
      }
      
      String key = name.trim();
      commandsMap.put(key,  method);
   }
   
   /**
    * Adds the alias.
    *
    * @param alias the alias
    * @param name the name
    */
   void addAlias(String alias, String name)
   {
      aliasMap.put(alias.trim(), name.trim());
   }
   
   /**
    * Gets the executor.
    *
    * @param name the name
    * @return the executor
    */
   Executor getExecutor(String name)
   {
      String realName = aliasMap.get(name.trim());
      if (realName == null)
      {
         realName = name.trim();
      }
      Executor ret = commandsMap.get(realName);
      return ret;
   }
}
