package net.alantea.tools.lang;

/**
 * The Enum ReportTypes.
 */
public enum ReportTypes
{
   
   /** The Comment. */
   COMMENT,
   
   /** The success. */
   SUCCESS,
   
   /** The failed. */
   FAILED,
}
