package net.alantea.tools.lang;

/**
 * The Class ErrorReport.
 */
public class ErrorReport implements ExecutionReport
{
   
   /** The num line. */
   private int numLine;
   
   /** The theline. */
   private String theline;
   
   /** The parsingerror. */
   private ErrorType parsingerror;
   
   /** The message. */
   private String message;
   
   /**
    * Instantiates a new error report.
    *
    * @param num the num
    * @param theline the theline
    * @param parsingerror the parsingerror
    * @param message the message
    */
   public ErrorReport(int num, String theline, ErrorType parsingerror, String message)
   {
      this.numLine = num;
      this.theline = theline;
      this.parsingerror = parsingerror;
      this.message = message;
   }

   /**
    * Gets the returned value.
    *
    * @return the returned value
    */
   @Override
   public Object getReturnedValue()
   {
      return null;
   }

   /**
    * Gets the line number.
    *
    * @return the line number
    */
   public int getLineNumber()
   {
      return numLine;
   }

   /**
    * Gets the line.
    *
    * @return the line
    */
   public String getline()
   {
      return theline;
   }

   /**
    * Gets the parsing error.
    *
    * @return the parsing error
    */
   public ErrorType getParsingError()
   {
      return parsingerror;
   }

   /**
    * Gets the message.
    *
    * @return the message
    */
   public String getMessage()
   {
      return message;
   }
}
