package net.alantea.tools.lang;

public interface ExecutionReport
{
   public Object getReturnedValue();
}
