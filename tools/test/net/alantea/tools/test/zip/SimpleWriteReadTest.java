package net.alantea.tools.test.zip;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import net.alantea.tools.zip.ZipUtils;
import net.alantea.utils.FileUtilities;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class SimpleWriteReadTest
{
   @Test
   public void T1_CreateFileTest()
   {
      try
      {
         File destination = FileUtilities.createTemporaryDirectory("zipTest-1");
         File file = new File(destination.getAbsoluteFile()+ "/" + "test.txt");
         FileWriter writer = new FileWriter(file);
         writer.write("test");
         writer.close();
         
         ZipUtils.saveDirectory(destination.getAbsolutePath(), "C:Temp/zipTest001.zip");
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
         Assertions.fail();
      }
   }

   @Test
   public void T2_ReadFileTest()
   {
      try
      {
         File destination = ZipUtils.openTemporaryDirectory("zipTest001", "C:Temp/zipTest001.zip");
         File file = new File(destination.getAbsoluteFile()+ "/" + "test.txt");
         FileReader reader = new FileReader(file);
         BufferedReader bufread = new BufferedReader(reader);
         String line = bufread.readLine();
         System.out.println(line);
         reader.close();
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
         Assertions.fail();
      }
   }

   @Test
   public void T3_CreateDirectoryTest()
   {
      try
      {
         File destination = FileUtilities.createTemporaryDirectory("zipTest002");
         new File(destination.getAbsoluteFile()+ "/" + "subDir").mkdir();
         File file = new File(destination.getAbsoluteFile()+ "/subDir/" + "test.txt");
         FileWriter writer = new FileWriter(file);
         writer.write("test2");
         writer.close();
         
         ZipUtils.saveDirectory(destination.getAbsolutePath(), "C:Temp/zipTest002.zip");
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
         Assertions.fail();
      }
   }

   @Test
   public void T4_ReadDirectoryTest()
   {
      try
      {
         File destination = ZipUtils.openTemporaryDirectory("zipTest002", "C:Temp/zipTest002.zip");
         File file = new File(destination.getAbsoluteFile()+ "/subDir/" + "test.txt");
         FileReader reader = new FileReader(file);
         BufferedReader bufread = new BufferedReader(reader);
         String line = bufread.readLine();
         System.out.println(line);
         reader.close();
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
         Assertions.fail();
      }
   }
   
}
