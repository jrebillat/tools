package net.alantea.tools.test.lang;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.tools.test.lang.readers.StateTestingReader;
import net.alantea.utils.exception.LntException;


@TestMethodOrder(MethodOrderer.MethodName.class)

public class StateTestingTest
{
   private static StateTestingReader reader;

   @Test
   public void T0_load()
   {
      reader = new StateTestingReader();
   }
   
   @Test
   public void T1_StatesTest()
   {
      reader.parse("test state TOP\n"
            + "enter state STATE1\n"
            + "test state STATE1\n"
            + "enter state STATE2\n"
            + "test state STATE2\n"
            + "enter state STATE3\n"
            + "test state STATE3\n"
            + "leave state\n"
            + "test state STATE2\n"
            + "enter state STATE3\n"
            + "test state STATE3\n"
            + "leave state\n"
            + "leave state\n"
            + "test state STATE1");
   }
   
   @Test
   public void T2_ContextsTest()
   {
      reader.parse("enter context 1\n"
            + "test context 1\n"
            + "enter context 2\n"
            + "test context 2\n"
            + "enter context 3\n"
            + "test context 3\n"
            + "leave context\n"
            + "test context 2\n"
            + "enter context 3\n"
            + "test context 3\n"
            + "leave context\n"
            + "leave context\n"
            + "test context 1");
   }
   
   @Test
   public void T3_ContextStatesTest()
   {
      reader.parse("enter context 1\n"
            + "test context state 1\n"
            + "enter context 2\n"
            + "test context state 2\n"
            + "enter context 3\n"
            + "test context state 3\n"
            + "leave context\n"
            + "test context state 2\n"
            + "enter context 3\n"
            + "test context state 3\n"
            + "leave context\n"
            + "leave context\n"
            + "test context state 1");
   }
   
   @AfterAll
   public static void cleanAll()
   {
      List<LntException> exceptions = LntException.peekExceptions();
      System.out.println("Exceptions :");
      for (LntException exception : exceptions)
      {
         System.out.println(exception);
      }
   }
}
