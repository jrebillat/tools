package net.alantea.tools.test.lang;

import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.tools.lang.ErrorReport;
import net.alantea.tools.lang.ErrorType;
import net.alantea.tools.lang.SuccessReport;
import net.alantea.tools.test.lang.readers.ArgsCounterReader;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class ValidateTest
{
   private static ArgsCounterReader reader;
   
   @Test
   public void T0_load()
   {
      reader = new ArgsCounterReader();
   }
   
   @Test
   public void T1_simpleCommandTest()
   {
      reader.parse("zero", (result) -> {
         Assertions.assertTrue(result instanceof SuccessReport);
         Assertions.assertEquals(0, result.getReturnedValue());
         return null;});
   }
   
//   @Test
//   public void T2_simpleErrorTest()
//   {
//      reader.parse("tototititatatutu", (result) -> {
//         Assertions.assertTrue(result instanceof ErrorReport);
//         return null;});
//      reader.parse("one", (result) -> {
//         System.out.println(result.getClass());
//         Assertions.assertTrue(result instanceof ErrorReport);
//         return null;});
//   }
//   
//   @Test
//   public void T3_simpleCommentTest()
//   {
//      reader.parse("", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse(" ", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse("   ", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse("\t", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse(" \t", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse("\t ", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse(" \t ", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse("\t\t", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse("\t \t", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse("#", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse("##", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse(" #", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse("#toto", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse("##toto", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse("# toto", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse(" # toto", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//      reader.parse("\t# toto", (result) -> {Assertions.assertEquals(CommentResult.TYPE, result.getType()); return null;});
//   }
//   
//   @Test
//   public void T4_severalCommandsTest()
//   {
//      List<Object> results = new LinkedList<>();
//      reader.parse(
//            "zero\n"
//            + "one value\n"
//            + "two different values",
//            (result) -> {results.add(result); return null;});
//      for (Object result : results)
//      {
//         Assertions.assertEquals(CommandResult.TYPE, result.getType());
//      }
//   }
//   
//   @Test
//   public void T5_severalErrorsTest()
//   {
//      List<Object> results = new LinkedList<>();
//      reader.parse(
//            "toto\n"
//            + "titi\n"
//            + "tata\n"
//            + "tutu",
//            (result) -> {results.add(result); return null;});
//      for (Object result : results)
//      {
//         Assertions.assertEquals(ErrorResult.TYPE, result.getType());
//      }
//   }
//   
//   @Test
//   public void T6_severalCommentsTest()
//   {
//      List<Object> results = new LinkedList<>();
//      reader.parse(
//            " \n"
//            + "#\n"
//            + "\t\n"
//            + "# tutu",
//            (result) -> {results.add(result); return null;});
//      for (Object result : results)
//      {
//         Assertions.assertEquals(CommentResult.TYPE, result.getType());
//      }
//   }
//   
//   @Test
//   public void T7_mixedLinesTest()
//   {
//      List<Object> results = new LinkedList<>();
//      reader.parse(
//            "# \n"
//            + "zero\n"
//            + "tutu\n"
//            + "one\n",
//            (result) -> {results.add(result); return null;});
//      Assertions.assertEquals(CommentResult.TYPE, results.get(0).getType());
//      Assertions.assertEquals(CommandResult.TYPE, results.get(1).getType());
//      Assertions.assertEquals(ErrorResult.TYPE, results.get(2).getType());
//      Assertions.assertEquals(ErrorType.UNKNOWNCOMMANDERROR, ((ErrorResult) results.get(2)).getErrorType());
//      Assertions.assertEquals(ErrorResult.TYPE, results.get(3).getType());
//      Assertions.assertEquals(ErrorType.BADARGSNUMBERERROR, ((ErrorResult) results.get(3)).getErrorType());
//   }
}
