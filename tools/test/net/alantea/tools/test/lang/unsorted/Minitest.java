package net.alantea.tools.test.lang.unsorted;

import java.util.LinkedList;
import java.util.List;

public class Minitest
{

   public static void main(String[] args)
   {
      @SuppressWarnings("unused")
      String[] result = null;
      result = tokenizeLine("a \"b c\" d");
      result = tokenizeLine("a b c toto \"titi tata\" tutu \"d e f g\" tete h i j");
      result = tokenizeLine("toto\"titi tata\" tutu");

   }

   private static String[] tokenizeLine(String theline)
   {
      String[] ret = null;
      List<String> strings = new LinkedList<>();
      
      int start = 0;
      boolean insideSubstring = false;
      for (int i = 0; i < theline.length(); i++)
      {
         char currentChar = theline.charAt(i);
         if (currentChar == '"')
         {
            if (!insideSubstring)
            {
               insideSubstring = true;
            }
            else
            {
               strings.add(theline.substring(start, i));
               start = i + 1;
               insideSubstring = false;
               i++;
            }
            start = i + 1;
         }
         else if ((!insideSubstring) && (currentChar == ' '))
         {
            strings.add(theline.substring(start, i));
            start = i + 1;
         }
         else
         {
            // just continue
         }
      }
      strings.add(theline.substring(start, theline.length()));
      
//      String[] substrings = theline.split("\\\"");
      ret = strings.toArray(new String[0]);
      return ret;
   }
}
