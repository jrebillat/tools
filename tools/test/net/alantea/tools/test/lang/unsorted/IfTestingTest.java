package net.alantea.tools.test.lang.unsorted;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class IfTestingTest
{
   private static PartlyTestingReader reader;

   @Test
   public void T0_load()
   {
      reader = new PartlyTestingReader();
   }
   
   @Test
   public void T1_StatesTest()
   {
      reader.parse("comment hello\n"
      + "if true\n"
      + "  comment \"it is true\"\n"
      + "  if true\n"
      + "    comment \"it is always true\"\n"
      + "  else\n"
      + "    comment \"it is now false\"\n"
      + "  endif\n"
      + "else\n"
      + "  comment \"it is false\"\n"
      + "endif\n"
      + "comment ending");
      
      reader.parse("comment hello\n"
      + "if false\n"
      + "  comment \"it is true\"\n"
      + "  comment \"it is true\"\n"
      + "  comment \"it is true\"\n"
      + "else\n"
      + "  comment \"it is false\"\n"
      + "  comment \"it is false\"\n"
      + "endif\n" + "comment ending");
   }
}
