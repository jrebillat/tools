package net.alantea.tools.test.lang.unsorted;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class PartlyTestingTest
{
   private static PartlyTestingReader reader;

   @Test
   public void T0_load()
   {
      reader = new PartlyTestingReader();
   }
   
   @Test
   public void T1_StatesTest()
   {
      reader.parse("$a : setit\n"
            + "testit $a\n"
            + "toto");
   }
}
