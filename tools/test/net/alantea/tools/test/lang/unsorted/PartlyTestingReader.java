package net.alantea.tools.test.lang.unsorted;


import java.util.List;

import net.alantea.tools.lang.Langage;
import net.alantea.utils.exception.LntException;

public class PartlyTestingReader extends Langage
{
      
   public PartlyTestingReader()
   {
      addCommand("setit", this::manageSetit);
      addCommand("testit", this::manageTestit);
   }

   private Object manageSetit(List<Object> strings) throws LntException
   {
      String[] value = { "One", "Two" };
      return value;
   }

   private Object manageTestit(List<Object> strings) throws LntException
   {
      // TODO
      if (strings == null)
      {
         System.out.println("null argument");
      }
      else
      {
         for (Object object : strings)
         {
            System.out.println(object);
         }
      }
      return null;
   }
}
