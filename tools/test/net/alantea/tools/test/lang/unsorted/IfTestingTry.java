package net.alantea.tools.test.lang.unsorted;

public class IfTestingTry
{
   private static PartlyTestingReader reader;

   public static void main(String[] args)
   {
      reader = new PartlyTestingReader();

      test1();
      System.out.println("--------------------------------------------");
      test2();
      System.out.println("--------------------------------------------");
      test3();
      System.out.println("--------------------------------------------");
      test4();
      System.out.println("--------------------------------------------");
   }
   
   private static void test1()
   {
      reader.parse("comment Test 1\n"
                 + "if true\n"
                 + "  comment \"it is correct (1/1)\"\n"
                 + "endif\n"
                 + "if false\n"
                 + "  comment \"it is incorrect (1/1)\"\n"
                 + "endif\n"
                 + "comment ending");
   }
   
   private static void test2()
   {
      reader.parse("comment Test 2\n"
                 + "if false\n"
                 + "  comment \"it is incorrect (1/3)\"\n"
                 + "  comment \"it is incorrect (2/3)\"\n"
                 + "  comment \"it is incorrect (3/3)\"\n"
                 + "else\n"
                 + "  comment \"it is correct (1/2)\"\n"
                 + "  comment \"it is correct (2/2)\"\n"
                 + "endif\n"
                 + "comment ending");
   }
   
   private static void test3()
   {
      reader.parse("comment Test 3\n"
                 + "if true\n"
                 + "  comment \"it is correct (1/5)\"\n"
                 + "  comment \"it is correct (2/5)\"\n"
                 + "  comment \"it is correct (3/5)\"\n"
                 + "else\n"
                 + "  comment \"it is incorrect (1/5)\"\n"
                 + "  comment \"it is incorrect (2/5)\"\n"
                 + "endif\n"
                 + "if false\n"
                 + "  comment \"it is incorrect (3/5)\"\n"
                 + "  comment \"it is incorrect (4/5)\"\n"
                 + "  comment \"it is incorrect (5/5)\"\n"
                 + "else\n"
                 + "  comment \"it is correct (4/5)\"\n"
                 + "  comment \"it is correct (5/5)\"\n"
                 + "endif\n"
                 + "comment ending");
   }
   
   private static void test4()
   {      
      reader.parse("comment Test 4\n"
      + "if true\n"
      + "   comment \"it is correct (1/4)\"\n"
      + "   if true\n"
      + "      comment \"it is correct (2/4)\"\n"
      + "   else\n"
      + "      comment \"it is incorrect (1/8)\"\n"
      + "   endif\n"
      + "   if false\n"
      + "      comment \"it is incorrect (2/8)\"\n"
      + "   else\n"
      + "      comment \"it is correct (3/4)\"\n"
      + "   endif\n"
      + "else\n"
      + "   comment \"it is incorrect (3/8)\"\n"
      + "endif\n"

      + "comment \" -----\"\n"
      + "if false\n"
      + "   comment \"it is incorrect (4/8)\"\n"
      + "   if true\n"
      + "      comment \"it is incorrect (5/8)\"\n"
      + "   else\n"
      + "      comment \"it is incorrect (6/8)\"\n"
      + "   endif\n"
      + "   if false\n"
      + "      comment \"it is incorrect (7/8)\"\n"
      + "   else\n"
      + "      comment \"it is incorrect (8/8)\"\n"
      + "   endif\n"
      + "else\n"
      + "   comment \"it is correct (4/4)\"\n"
      + "endif\n"
      + "comment ending");
   }
}
