package net.alantea.tools.test.lang;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

@Suite
@SelectClasses({
   ArgsCounterTest.class,
   ArgsTestingTest.class,
   StateTestingTest.class,
   ValidateTest.class })
public class AllTests
{

}
