package net.alantea.tools.test.lang.readers;

import java.util.LinkedList;
import java.util.List;

import net.alantea.tools.lang.ErrorReport;
import net.alantea.tools.lang.ErrorType;
import net.alantea.tools.lang.Langage;
import net.alantea.utils.exception.LntException;

public class ArgsTestingReader extends Langage
{
   private List<Object> arguments = new LinkedList<>();
   
   public ArgsTestingReader()
   {
      super();

      addCommand("test", this::manageAll);
      addCommand("the real value of", this::manageAll);
      addCommand("int", this::manageInteger);
   }
   
   /**
    * Gets the arguments.
    *
    * @return the arguments
    */
   public List<Object> getArguments()
   {
      return arguments;
   }

   private Object manageAll(List<Object> strings) throws LntException
   {
      arguments = strings;
      return null;
   }

   private Object manageInteger(List<Object> strings) throws LntException
   {
      String arg = (String) strings.get(0);
      if (!arg.matches("^[0-9]+$"))
      {
         return new ErrorReport(0, "line", ErrorType.BADARGUMENTERROR, arg);
      }
      return null;
   }
}
