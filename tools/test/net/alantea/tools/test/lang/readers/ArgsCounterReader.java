package net.alantea.tools.test.lang.readers;

import org.junit.jupiter.api.Assertions;

import java.util.List;

import net.alantea.tools.lang.Langage;
import net.alantea.utils.exception.LntException;

public class ArgsCounterReader extends Langage
{
   private int lastVariableCount = 0;

   public ArgsCounterReader()
   {
      super();

      addCommand("zero", this::manageZero);
      addCommand("one", this::manageOne);
      addCommand("two", this::manageTwo);
      addCommand("three", this::manageThree);
      addCommand("variable", this::manageVariable);
      addCommand("this is variable", this::manageVariable);
      addCommand("this equals", this::manageVariable);
      addCommand("the value of",this::manageValueOf);
   }
   
   /**
    * Gets the last variable count.
    *
    * @return the last variable count
    */
   public int getLastVariableCount()
   {
      return lastVariableCount;
   }

   private Object manageZero(List<Object> strings) throws LntException
   {
      Assertions.assertEquals(0, strings.size());
      return 0;
   }

   private Object manageOne(List<Object> tokens) throws LntException
   {
      Assertions.assertEquals(1, tokens.size());
      return 1;
   }

   private Object manageTwo(List<Object> tokens) throws LntException
   {
      Assertions.assertEquals(2, tokens.size());
      return 2;
   }

   private Object manageThree(List<Object> tokens) throws LntException
   {
      Assertions.assertEquals(3, tokens.size());
      return 3;
   }

   private Object manageVariable(List<Object> tokens) throws LntException
   {
      lastVariableCount = tokens.size();
      return lastVariableCount;
   }

   private Object manageValueOf(List<Object> tokens) throws LntException
   {
      Assertions.assertEquals(2, tokens.size());
      return true;
   }
}
