package net.alantea.tools.test.lang.readers;

import org.junit.jupiter.api.Assertions;

import java.util.List;

import net.alantea.tools.lang.LangException;
import net.alantea.tools.lang.Langage;
import net.alantea.tools.lang.LangageContext;
import net.alantea.utils.exception.LntException;

public class StateTestingReader extends Langage
{
   public static final String STATE1 = "STATE1";
   public static final String STATE2 = "STATE2";
   public static final String STATE3 = "STATE3";
   
   public LangageContext context1 = new LangageContext(STATE1);
   public LangageContext context2 = new LangageContext(STATE2);
   public LangageContext context3 = new LangageContext(STATE3);
      
   public StateTestingReader()
   {
      super();
      
      addState(STATE1);
      addState(STATE2);
      addState(STATE3);

      addCommand("enter state", this::manageEnterState);
      addCommand("leave state", this::manageLeaveState);
      addCommand("test state", this::manageTestState);
      addCommand("enter context", this::manageEnterContext);
      addCommand("leave context", this::manageLeaveContext);
      addCommand("test context", this::manageTestContext);
      addCommand("test context state", this::manageTestContextState);
   }

   private Object manageEnterState(List<Object> strings) throws LntException
   {
      try
      {
         setState((String) strings.get(0));
      }
      catch (LangException e)
      {
         Assertions.fail("Unknown state");
      }
      return null;
   }

   private Object manageLeaveState(List<Object> strings) throws LntException
   {
      unsetState();
      return null;
   }

   private Object manageTestState(List<Object> strings) throws LntException
   {
      Assertions.assertEquals(getState(), strings.get(0));
      return null;
   }

   private Object manageEnterContext(List<Object> strings) throws LntException
   {
      try
      {
         switch((String)strings.get(0))
         {
            case "1" :
               setContext(context1);
               break;
               
            case "2" :
               setContext(context2);
               break;
               
            case "3" :
               setContext(context3);
               break;
         }
      }
      catch (LangException e)
      {
         e.printStackTrace();
      }
      return null;
   }

   private Object manageLeaveContext(List<Object> strings) throws LntException
   {
      unsetContext();
      return null;
   }

   private Object manageTestContext(List<Object> strings) throws LntException
   {
      switch((String)strings.get(0))
      {
         case "1" :
            Assertions.assertEquals(getContext(), context1);
            break;
            
         case "2" :
            Assertions.assertEquals(getContext(), context2);
            break;
            
         case "3" :
            Assertions.assertEquals(getContext(), context3);
            break;
      }
      return null;
   }

   private Object manageTestContextState(List<Object> strings) throws LntException
   {
      switch((String)strings.get(0))
      {
         case "1" :
            Assertions.assertEquals(getState(), STATE1);
            break;
            
         case "2" :
            Assertions.assertEquals(getState(), STATE2);
            break;
            
         case "3" :
            Assertions.assertEquals(getState(), STATE3);
            break;
      }
      return null;
   }
}
