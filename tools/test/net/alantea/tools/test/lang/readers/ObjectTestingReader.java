package net.alantea.tools.test.lang.readers;


import java.util.List;

import net.alantea.tools.lang.Langage;
import net.alantea.utils.exception.LntException;

public class ObjectTestingReader extends Langage
{
      
   public ObjectTestingReader()
   {
      addCommand("setit", this::manageSetit);
      addCommand("testit", this::manageTestit);
   }

   private Object manageSetit(List<Object> strings) throws LntException
   {
      String[] value = { "One", "Two" };
      return value;
   }

   private Object manageTestit(List<Object> strings) throws LntException
   {
      // TODO
      System.out.println(strings);
      return null;
   }
}
