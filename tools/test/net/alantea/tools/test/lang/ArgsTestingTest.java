package net.alantea.tools.test.lang;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.List;

import net.alantea.tools.test.lang.readers.ArgsTestingReader;
import net.alantea.utils.exception.LntException;


@TestMethodOrder(MethodOrderer.MethodName.class)

public class ArgsTestingTest
{
   private static ArgsTestingReader reader;

   @Test
   public void T0_load()
   {
      reader = new ArgsTestingReader();
   }
   
   @Test
   public void T1_zeroArgsTest()
   {
      reader.parse("test");
      List<Object> args = reader.getArguments();
      Assertions.assertNotNull(args);
      Assertions.assertEquals(0, args.size());
   }
   
   @Test
   public void T2_oneArgTest()
   {
      reader.parse("test me");
      List<Object> args = reader.getArguments();
      Assertions.assertNotNull(args);
      Assertions.assertEquals(1, args.size());
      Assertions.assertEquals("me", args.get(0));
   }
   
   @Test
   public void T3_twoArgsTest()
   {
      reader.parse("test different values");
      List<Object> args = reader.getArguments();
      Assertions.assertNotNull(args);
      Assertions.assertEquals(2, args.size());
      Assertions.assertEquals("different", args.get(0));
      Assertions.assertEquals("values", args.get(1));
   }
   
   @Test
   public void T4_threeArgsTest()
   {
      reader.parse("test you and me");
      List<Object> args = reader.getArguments();
      Assertions.assertNotNull(args);
      Assertions.assertEquals(3, args.size());
      Assertions.assertEquals("you", args.get(0));
      Assertions.assertEquals("and", args.get(1));
      Assertions.assertEquals("me", args.get(2));
   }
   
//   @Test
//   public void T5_namedArgsTest()
//   {
//      reader.parse("the real value of good \"and professional doctor\" 33");
//      List<Object> args = reader.getArguments();
//      Assertions.assertNotNull(args);
//      Assertions.assertEquals(2, args.size());
//      Assertions.assertEquals("and professional doctor", args.get(0));
//      Assertions.assertEquals("33", args.get(1));
//
//      reader.parse("the real value of good \"and professional doctor\" is \"always 33\"");
//      args = reader.getArguments();
//      Assertions.assertNotNull(args);
//      Assertions.assertEquals(2, args.size());
//      Assertions.assertEquals("and professional doctor", args.get(0));
//      Assertions.assertEquals("always 33", args.get(1));
//   }
   
//   @Test
//   public void T6_intArgTest()
//   {
//      reader.parse("int 0\n"
//            + "int 1\n"
//            + "int somethingbad\n"
//            + "int 123456");
//      List<ParsingException> excepts = reader.getExceptions();
//      Assertions.assertEquals(1, excepts.size());
//      ParsingException ex = excepts.get(0);
//      Assertions.assertEquals(ErrorType.BADARGUMENTERROR, ex.getErrorType());
//      Assertions.assertEquals(2, ex.getNumLine());
//   }
   
//   @Test
//   public void T7_erroneousNamedArgsTest()
//   {
//      reader.parse("the real value of bad doctor is 666");
//      List<ParsingException> excepts = reader.getExceptions();
//      Assertions.assertEquals(1, excepts.size());
//      ParsingException ex = excepts.get(0);
//      Assertions.assertEquals(ErrorType.BADARGSNUMBERERROR, ex.getErrorType());
//   }
   
   @AfterAll
   public static void cleanAll()
   {
      List<LntException> exceptions = LntException.peekExceptions();
      System.out.println("Exceptions :");
      for (LntException exception : exceptions)
      {
         System.out.println(exception);
      }
   }
}
