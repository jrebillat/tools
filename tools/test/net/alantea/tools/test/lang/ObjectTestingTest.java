package net.alantea.tools.test.lang;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.tools.test.lang.readers.ObjectTestingReader;
import net.alantea.utils.exception.LntException;


@TestMethodOrder(MethodOrderer.MethodName.class)

public class ObjectTestingTest
{
   private static ObjectTestingReader reader;

   @Test
   public void T0_load()
   {
      reader = new ObjectTestingReader();
   }
   
   @Test
   public void T1_StatesTest()
   {
      reader.parse("$a : setit\n"
            + "testit $a\n"
            + "toto");
   }
   
   @AfterAll
   public static void cleanAll()
   {
      List<LntException> exceptions = LntException.peekExceptions();
      System.out.println("Exceptions :");
      for (LntException exception : exceptions)
      {
         System.out.println(exception);
      }
   }
}
