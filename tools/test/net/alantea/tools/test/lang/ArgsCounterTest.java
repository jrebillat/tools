package net.alantea.tools.test.lang;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.tools.test.lang.readers.ArgsCounterReader;
import net.alantea.utils.exception.LntException;



@TestMethodOrder(MethodOrderer.MethodName.class)

public class ArgsCounterTest
{
   private static ArgsCounterReader reader;

   @Test
   public void T0_load()
   {
      reader = new ArgsCounterReader();
   }
   
   @Test
   public void T1_zeroArgsTest()
   {
      reader.parse("zero");
   }
   
   @Test
   public void T2_oneArgTest()
   {
      reader.parse("one value");
   }
   
   @Test
   public void T3_twoArgsTest()
   {
      reader.parse("two different values");
   }
   
   @Test
   public void T4_threeArgsTest()
   {
      reader.parse("three different good values");
   }
   
   @Test
   public void T5_variableArgsTest()
   {
      reader.parse("variable");
      Assertions.assertEquals(0, reader.getLastVariableCount());

      reader.parse("variable one");
      Assertions.assertEquals(1, reader.getLastVariableCount());
      
      reader.parse("variable one two");
      Assertions.assertEquals(2, reader.getLastVariableCount());

      reader.parse("variable one two three");
      Assertions.assertEquals(3, reader.getLastVariableCount());

      reader.parse("variable one two three four");
      Assertions.assertEquals(4, reader.getLastVariableCount());
   }
   
   @AfterAll
   public static void cleanAll()
   {
      List<LntException> exceptions = LntException.peekExceptions();
      System.out.println("Exceptions :");
      for (LntException exception : exceptions)
      {
         System.out.println(exception);
      }
   }
}
