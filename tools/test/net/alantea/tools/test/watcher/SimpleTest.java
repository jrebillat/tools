package net.alantea.tools.test.watcher;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import net.alantea.tools.watcher.Watcher;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class SimpleTest
{
   private static final String TEST_FILE = "D:\\Users\\jean\\test.txt";
   
   @Test
   public void T1_CreateFileTest()
   {
      File file = new File(TEST_FILE);
      
      try
      {
         FileWriter writer = new FileWriter(file);
         writer.write("test.");
         writer.flush();
         writer.close();
         System.out.println("file : " + file.getAbsolutePath());
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
         Assertions.fail();
      }

      try
      {
         new Watcher(file, (type, target) -> System.out.println("got event " + type.name() + " for file : " + target.getAbsolutePath()));
      }
      catch (Exception e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
         Assertions.fail();
      }

      try
      {
         FileWriter writer = new FileWriter(file);
         writer.write("test");
         writer.close();
         Thread.sleep(2000);
      }
      catch (IOException | InterruptedException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
         Assertions.fail();
      }
      
//         file.delete();
   }
   
}
