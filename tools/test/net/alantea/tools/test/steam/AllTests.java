package net.alantea.tools.test.steam;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

@Suite
@SelectClasses({
   ControlledStateMachineTest.class,
   EnumeratedStateMachineTest.class,
   EventMachineTest.class,
   EventStateMachineTest.class,
   LangStateMachineTest.class,
   MethodsStateMachineTest.class,
   SimpleStateMachineTest.class,
   StateReactorTest.class,
   })
public class AllTests
{

}
