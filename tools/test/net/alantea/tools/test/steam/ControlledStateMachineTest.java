package net.alantea.tools.test.steam;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.tools.steam.StateMachine;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class ControlledStateMachineTest
{
   private static final String INITIAL_STATE = "InitialState";
   private static final String STATE_ONE = "StateOne";
   private static final String STATE_TWO = "StateTwo";
   private static final String STATE_THREE = "StateThree";
   
   @Test
   public void T01_controlledStateMachineSetSpecificChangesTest()
   {
      StateMachine<String> machine = new StateMachine<>(INITIAL_STATE);
      machine.addStates(STATE_ONE, STATE_TWO, STATE_THREE);
      Assertions.assertNotNull(machine);

      machine.allowChange(INITIAL_STATE, STATE_ONE);
      machine.allowChange(STATE_ONE, STATE_TWO);
      machine.allowChange(STATE_TWO, STATE_THREE);
      machine.allowChange(STATE_THREE, STATE_ONE);
      
      boolean done = machine.setState(STATE_ONE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(STATE_ONE, machine.getState());
      
      done = machine.setState(STATE_TWO);
      Assertions.assertTrue(done);
      Assertions.assertEquals(STATE_TWO, machine.getState());
      
      done = machine.setState(STATE_THREE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(STATE_THREE, machine.getState());
      
      done = machine.setState(STATE_ONE);
      Assertions.assertTrue(done);
      
      done = machine.setState(STATE_THREE);
      Assertions.assertFalse(done);
      Assertions.assertEquals(STATE_ONE, machine.getState());
   }
   
   @Test
   public void T02_controlledStateMachineSetLeaveChangesTest()
   {
      StateMachine<String> machine = new StateMachine<>(INITIAL_STATE);
      machine.addStates(STATE_ONE, STATE_TWO, STATE_THREE);
      Assertions.assertNotNull(machine);

      machine.allowAllChangesFrom(INITIAL_STATE);
      machine.allowAllChangesFrom(STATE_ONE);
      machine.allowAllChangesFrom(STATE_TWO);
      machine.allowAllChangesFrom(STATE_THREE);
      
      boolean done = machine.setState(STATE_ONE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(STATE_ONE, machine.getState());
      
      done = machine.setState(STATE_TWO);
      Assertions.assertTrue(done);
      Assertions.assertEquals(STATE_TWO, machine.getState());
      
      done = machine.setState(STATE_THREE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(STATE_THREE, machine.getState());
      
      done = machine.setState(STATE_ONE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(STATE_ONE, machine.getState());

      done = machine.setState(STATE_THREE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(STATE_THREE, machine.getState());
   }
   
   @Test
   public void T03_controlledStateMachineSetEnterChangesTest()
   {
      StateMachine<String> machine = new StateMachine<>(INITIAL_STATE);
      machine.addStates(STATE_ONE, STATE_TWO, STATE_THREE);
      Assertions.assertNotNull(machine);
      machine.allowAllChangesFrom(INITIAL_STATE);

      machine.allowAllChangesTo(STATE_ONE);
      machine.allowAllChangesTo(STATE_TWO);
      machine.allowAllChangesTo(STATE_THREE);
      
      boolean done = machine.setState(STATE_ONE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(STATE_ONE, machine.getState());
      
      done = machine.setState(STATE_TWO);
      Assertions.assertTrue(done);
      Assertions.assertEquals(STATE_TWO, machine.getState());
      
      done = machine.setState(STATE_THREE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(STATE_THREE, machine.getState());
      
      done = machine.setState(STATE_ONE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(STATE_ONE, machine.getState());

      done = machine.setState(STATE_THREE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(STATE_THREE, machine.getState());
   }
}
