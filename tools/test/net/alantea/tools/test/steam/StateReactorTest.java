package net.alantea.tools.test.steam;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.tools.steam.EnumeratedStateMachine;
import net.alantea.utils.exception.LntException;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class StateReactorTest
{
   private static enum States1 {
      INITIAL_STATE,
      STATE_ONE,
      STATE_TWO
   }
   
   private static enum States2 {
      IDLE_STATE,
      RUNNING_STATE,
      STOPPED_STATE
   }
   
   @Test
   public void T01_simpleReactorTest() throws LntException
   {
      EnumeratedStateMachine<States1> machine = new EnumeratedStateMachine<>(States1.class, States1.INITIAL_STATE);
      Assertions.assertNotNull(machine);
      
      int[] values = new int[1];
      
      machine.createStateReactor(States1.STATE_ONE, 100, () -> values[0]++);
      
      boolean done = machine.setState(States1.STATE_ONE);
      Assertions.assertTrue(done);
      
      sleep(400);
      
      done = machine.setState(States1.STATE_TWO);
      Assertions.assertTrue(done);
      // reactor has been running
      Assertions.assertEquals(4, values[0]);

      sleep(400);

      // reactor has been stopped
      Assertions.assertEquals(4, values[0]);
      done = machine.setState(States1.STATE_ONE);
      Assertions.assertTrue(done);
      
      sleep(400);
      
      done = machine.setState(States1.STATE_TWO);
      Assertions.assertTrue(done);
      // reactor has been running
      Assertions.assertEquals(8, values[0]);
   }
   
   @Test
   public void T02_chainedReactorsTest() throws LntException
   {
      EnumeratedStateMachine<States1> machine1 = new EnumeratedStateMachine<>(States1.class, States1.INITIAL_STATE);
      Assertions.assertNotNull(machine1);
      EnumeratedStateMachine<States2> machine2 = new EnumeratedStateMachine<>(States2.class, States2.IDLE_STATE);
      Assertions.assertNotNull(machine2);
      EnumeratedStateMachine<States2> machine3 = new EnumeratedStateMachine<>(States2.class, States2.IDLE_STATE);
      Assertions.assertNotNull(machine3);
      
      int[] values = new int[3];
      boolean[] done = new boolean[1];
      done[0] = false;

      machine1.createStateReactor(States1.STATE_ONE, 100, () ->{
         values[0]++;
         if (values[0] >= 4)
         {
            machine1.setState(States1.STATE_TWO);
            machine2.setState(States2.RUNNING_STATE);
         }
      });
      
      machine2.createStateReactor(States2.RUNNING_STATE, 100, () ->{
         values[1]++;
         if (values[1] == 5)
         {
            machine2.setState(States2.STOPPED_STATE);
            machine3.setState(States2.RUNNING_STATE);
         }
      });
      
      machine3.createStateReactor(States2.RUNNING_STATE, 100, () ->{
         values[2]++;
         if (values[2] == 6)
         {
            machine3.setState(States2.STOPPED_STATE);
         }
      });

      machine3.addEnterListener(States2.STOPPED_STATE, (o, n) -> done[0] = true);
      
      
      machine1.setState(States1.STATE_ONE);
      while(!done[0])
      {
         sleep(100);
      }
      Assertions.assertEquals(4, values[0]);
      Assertions.assertEquals(5, values[1]);
      Assertions.assertEquals(6, values[2]);
      
   }
   
   private void sleep(long timeout)
   {
      try
      {
         Thread.sleep(timeout);
      }
      catch (InterruptedException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }
}
