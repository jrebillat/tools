package net.alantea.tools.test.steam;

import net.alantea.tools.steam.EventKey;
import net.alantea.tools.steam.EventStateMachine;

public class TestEventStateMachine extends EventStateMachine<TestStateMachineEnum>
{

   public static final EventKey key1 = new EventKey();
   public static final EventKey key2 = new EventKey();
   
   private int value1_1 = 0;
   private int value1_2 = 0;
   private int value2_1 = 0;

   public TestEventStateMachine(TestStateMachineEnum initialState)
   {
      super(initialState);
      
      this.addTransition(TestStateMachineEnum.IDLE_STATE, TestStateMachineEnum.RUNNING_STATE, key1,
            (in, out, evt) -> value1_1++,
            (in, out, evt) -> value1_2++)
      .addTransition(TestStateMachineEnum.RUNNING_STATE, TestStateMachineEnum.STOPPED_STATE, key2,
            (in, out, evt) -> value2_1++);
   }

   public int getValue1_1()
   {
      return value1_1;
   }

   public int getValue1_2()
   {
      return value1_2;
   }

   public int getValue2_1()
   {
      return value2_1;
   }

}
