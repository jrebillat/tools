package net.alantea.tools.test.steam;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.tools.steam.StateMachineReader;
import net.alantea.utils.exception.LntException;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class LangStateMachineTest
{
   private String machineT01LangTxt =
           "in state RUNNING_STATE\n"
         + "  allow enter from IDLE_STATE\n"
         + "in state STOPPED_STATE\n"
         + "  allow enter from RUNNING_STATE\n";

   private String machineT02LangTxt =
           "in state RUNNING_STATE\n"
         + "  allow enter from IDLE_STATE\n"
         + "  on enter execute increment1\n"
         + "in state STOPPED_STATE\n"
         + "  allow enter from RUNNING_STATE\n"
         + "  on enter execute increment2\n";

   @Test
   public void T01_allowStateMachineTest() throws LntException
   {
      TestStateMachine machine = new TestStateMachine(TestStateMachineEnum.IDLE_STATE);
      Assertions.assertNotNull(machine);
      Assertions.assertEquals(0, machine.getEnterValue());
      Assertions.assertEquals(0, machine.getLeaveValue());
      Assertions.assertEquals(0, machine.getAnyEnterValue());
      Assertions.assertEquals(0, machine.getAnyLeaveValue());
      
      new StateMachineReader<TestStateMachineEnum>(machine).read(machineT01LangTxt);
      
      boolean done = machine.setState(TestStateMachineEnum.RUNNING_STATE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(1, machine.getEnterValue());
      Assertions.assertEquals(1, machine.getAnyEnterValue());
      Assertions.assertEquals(1, machine.getAnyLeaveValue());
      
      done = machine.setState(TestStateMachineEnum.STOPPED_STATE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(1, machine.getLeaveValue());
      Assertions.assertEquals(2, machine.getEnterValue());
      Assertions.assertEquals(2, machine.getAnyEnterValue());
      Assertions.assertEquals(2, machine.getAnyLeaveValue());
      
      done = machine.setState(TestStateMachineEnum.RUNNING_STATE);
      Assertions.assertFalse(done);
      Assertions.assertEquals(1, machine.getLeaveValue());
      Assertions.assertEquals(2, machine.getEnterValue());
      Assertions.assertEquals(2, machine.getAnyEnterValue());
      Assertions.assertEquals(2, machine.getAnyLeaveValue());
   }

   @Test
   public void T02_actionStateMachineTest() throws LntException
   {
      TestStateMachine machine = new TestStateMachine(TestStateMachineEnum.IDLE_STATE);
      Assertions.assertNotNull(machine);
      Assertions.assertEquals(0, machine.getIncrement1());
      Assertions.assertEquals(0, machine.getIncrement2());
      
      new StateMachineReader<TestStateMachineEnum>(machine).read(machineT02LangTxt);
      
      boolean done = machine.setState(TestStateMachineEnum.RUNNING_STATE);
      Assertions.assertTrue(done);
      wait(100);
      Assertions.assertEquals(1, machine.getEnterValue());
      Assertions.assertEquals(1, machine.getAnyEnterValue());
      Assertions.assertEquals(1, machine.getAnyLeaveValue());
      Assertions.assertEquals(1, machine.getIncrement1());
      Assertions.assertEquals(0, machine.getIncrement2());
      
      done = machine.setState(TestStateMachineEnum.STOPPED_STATE);
      Assertions.assertTrue(done);
      wait(100);
      Assertions.assertEquals(1, machine.getIncrement1());
      Assertions.assertEquals(1, machine.getIncrement2());
   }
   
   private void wait(int timeout)
   {
      try
      {
         Thread.sleep(100);
      }
      catch (InterruptedException e)
      {
         // Nothing
      }
   }
}
