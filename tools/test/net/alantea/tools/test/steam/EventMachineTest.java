package net.alantea.tools.test.steam;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class EventMachineTest
{
   @Test
   public void T01_simpleEventMachineTest()
   {
      TestEventMachine machine = new TestEventMachine(TestStateMachineEnum.IDLE_STATE);
      Assertions.assertNotNull(machine);

      TestStateMachineEnum state = machine.getState();
      Assertions.assertEquals(machine.getValue1_1(), 0);
      Assertions.assertEquals(machine.getValue1_2(), 0);
      Assertions.assertEquals(machine.getValue2_1(), 0);

      Assertions.assertEquals(TestStateMachineEnum.IDLE_STATE, state);
      
      machine.trigger(TestEventMachine.GOTORUNNING);

      state = machine.getState();
      Assertions.assertEquals(TestStateMachineEnum.RUNNING_STATE, state);
      Assertions.assertEquals(machine.getValue1_1(), 1);
      Assertions.assertEquals(machine.getValue1_2(), 1);
      Assertions.assertEquals(machine.getValue2_1(), 0);
      
      machine.trigger(TestEventMachine.GOTORUNNING); // second time

      state = machine.getState();
      Assertions.assertEquals(TestStateMachineEnum.RUNNING_STATE, state);
      Assertions.assertEquals(machine.getValue1_1(), 1);
      Assertions.assertEquals(machine.getValue1_2(), 1);
      Assertions.assertEquals(machine.getValue2_1(), 0);
      
      machine.trigger(TestEventMachine.GOTOSTOPPED);

      state = machine.getState();
      Assertions.assertEquals(TestStateMachineEnum.STOPPED_STATE, state);
      Assertions.assertEquals(machine.getValue1_1(), 1);
      Assertions.assertEquals(machine.getValue1_2(), 1);
      Assertions.assertEquals(machine.getValue2_1(), 1);
   }
}
