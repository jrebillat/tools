package net.alantea.tools.test.steam;

import net.alantea.tools.steam.EventMachine;

public class TestEventMachine extends EventMachine<TestStateMachineEnum, String>
{

   public static final String GOTORUNNING = "GOTORUNNING";
   public static final String GOTOSTOPPED = "GOTOSTOPPED";
   
   private int value1_1 = 0;
   private int value1_2 = 0;
   private int value2_1 = 0;

   public TestEventMachine(TestStateMachineEnum initialState)
   {
      super(initialState);
      
      this.addTransition(TestStateMachineEnum.IDLE_STATE, TestStateMachineEnum.RUNNING_STATE, GOTORUNNING,
            (in, out, evt) -> value1_1++,
            (in, out, evt) -> value1_2++)
      .addTransition(TestStateMachineEnum.RUNNING_STATE, TestStateMachineEnum.STOPPED_STATE, GOTOSTOPPED,
            (in, out, evt) -> value2_1++);
   }

   public int getValue1_1()
   {
      return value1_1;
   }

   public int getValue1_2()
   {
      return value1_2;
   }

   public int getValue2_1()
   {
      return value2_1;
   }

}
