package net.alantea.tools.test.steam;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.tools.steam.Event;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class EventStateMachineTest
{
   @Test
   public void T01_simpleEventMachineTest()
   {
      TestEventStateMachine machine = new TestEventStateMachine(TestStateMachineEnum.IDLE_STATE);
      Assertions.assertNotNull(machine);

      TestStateMachineEnum state = machine.getState();
      Assertions.assertEquals(0, machine.getValue1_1());
      Assertions.assertEquals(0, machine.getValue1_2());
      Assertions.assertEquals(0, machine.getValue2_1());

      Assertions.assertEquals(TestStateMachineEnum.IDLE_STATE, state);
      
      new Event(TestEventStateMachine.key1).trigger();

      state = machine.getState();
//      Assertions.assertEquals(TestStateMachineEnum.RUNNING_STATE, state);
      Assertions.assertEquals(1, machine.getValue1_1());
      Assertions.assertEquals(1, machine.getValue1_2());
      Assertions.assertEquals(0, machine.getValue2_1());

      new Event(TestEventStateMachine.key1).trigger(); // second time

      state = machine.getState();
//      Assertions.assertEquals(TestStateMachineEnum.RUNNING_STATE, state);
      Assertions.assertEquals(1, machine.getValue1_1());
      Assertions.assertEquals(1, machine.getValue1_2());
      Assertions.assertEquals(0, machine.getValue2_1());

      new Event(TestEventStateMachine.key2).trigger();

      state = machine.getState();
//      Assertions.assertEquals(TestStateMachineEnum.STOPPED_STATE, state);
      Assertions.assertEquals(1, machine.getValue1_1());
      Assertions.assertEquals(1, machine.getValue1_2());
      Assertions.assertEquals(1, machine.getValue2_1());
   }
}
