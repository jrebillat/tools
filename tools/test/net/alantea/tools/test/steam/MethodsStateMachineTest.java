package net.alantea.tools.test.steam;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.utils.exception.LntException;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class MethodsStateMachineTest
{
   @Test
   public void T01_methodsStateMachineTest() throws LntException
   {
      TestStateMachine machine = new TestStateMachine(TestStateMachineEnum.IDLE_STATE);
      Assertions.assertNotNull(machine);
      Assertions.assertEquals(0, machine.getEnterValue());
      Assertions.assertEquals(0, machine.getLeaveValue());
      Assertions.assertEquals(0, machine.getAnyEnterValue());
      Assertions.assertEquals(0, machine.getAnyLeaveValue());
      
      boolean done = machine.setState(TestStateMachineEnum.RUNNING_STATE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(1, machine.getEnterValue());
      Assertions.assertEquals(1, machine.getAnyEnterValue());
      Assertions.assertEquals(1, machine.getAnyLeaveValue());
      
      done = machine.setState(TestStateMachineEnum.STOPPED_STATE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(1, machine.getLeaveValue());
      Assertions.assertEquals(2, machine.getEnterValue());
      Assertions.assertEquals(2, machine.getAnyEnterValue());
      Assertions.assertEquals(2, machine.getAnyLeaveValue());
   }
}
