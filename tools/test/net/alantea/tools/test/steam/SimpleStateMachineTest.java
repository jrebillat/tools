package net.alantea.tools.test.steam;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.liteprops.StringProperty;
import net.alantea.tools.steam.StateMachine;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class SimpleStateMachineTest
{
   private static final String INITIAL_STATE = "InitialState";
   private static final String NEW_STATE = "NewState";
   private static final String STATE_ONE = "StateOne";
   private static final String STATE_TWO = "StateTwo";
   private static final String STATE_THREE = "StateThree";
   
   @Test
   public void T01_createSimpleStateMachineTest()
   {
      StateMachine<String> machine = new StateMachine<>(INITIAL_STATE);
      Assertions.assertNotNull(machine);
   }
   
   @Test
   public void T02_simpleStateMachineChangeOneStateTest()
   {
      StateMachine<String> machine = new StateMachine<>(INITIAL_STATE);
      machine.addStates(NEW_STATE, STATE_ONE, STATE_TWO, STATE_THREE);
      Assertions.assertNotNull(machine);
      
      Object state = machine.getState();
      Assertions.assertEquals(INITIAL_STATE, state);
      
      machine.setState(NEW_STATE);
      state = machine.getState();
      Assertions.assertEquals(NEW_STATE, state);
   }
   
   @Test
   public void T03_simpleStateMachineChangeMultipleStatesTest()
   {
      StateMachine<String> machine = new StateMachine<>(INITIAL_STATE);
      machine.addStates(NEW_STATE, STATE_ONE, STATE_TWO, STATE_THREE);
      Assertions.assertNotNull(machine);
      
      Object state = machine.getState();
      Assertions.assertEquals(INITIAL_STATE, state);
      
      machine.setState(STATE_ONE);
      state = machine.getState();
      Assertions.assertEquals(STATE_ONE, state);
      
      machine.setState(STATE_TWO);
      state = machine.getState();
      Assertions.assertEquals(STATE_TWO, state);
      
      machine.setState(STATE_THREE);
      state = machine.getState();
      Assertions.assertEquals(STATE_THREE, state);
   }
   
   @Test
   public void T04_simpleStateMachineChangeStateWithGlobalListenerTest()
   {
      StateMachine<String> machine = new StateMachine<>(INITIAL_STATE);
      machine.addStates(NEW_STATE, STATE_ONE, STATE_TWO, STATE_THREE);
      Assertions.assertNotNull(machine);
      
      boolean[] passed = new boolean[1];
      passed[0] = false;
      
      machine.addListener((o, n) -> passed[0] = true);
      
      machine.setState(NEW_STATE);
      wait(10);
      Assertions.assertTrue(passed[0]);
   }
   
   @Test
   public void T05_simpleStateMachineChangeStateWithEnterListenerTest()
   {
      StateMachine<String> machine = new StateMachine<>(INITIAL_STATE);
      machine.addStates(NEW_STATE, STATE_ONE, STATE_TWO, STATE_THREE);
      Assertions.assertNotNull(machine);
      
      boolean[] passed = new boolean[1];
      passed[0] = false;
      
      machine.addEnterListener(STATE_TWO, (o, n) -> passed[0] = true);
      
      machine.setState(STATE_ONE);
      wait(10);
      Assertions.assertFalse(passed[0]);
      
      machine.setState(STATE_TWO);
      wait(10);
      Assertions.assertTrue(passed[0]);
   }
   
   @Test
   public void T06_simpleStateMachineChangeStateWithLeaveListenerTest()
   {
      StateMachine<String> machine = new StateMachine<>(INITIAL_STATE);
      machine.addStates(NEW_STATE, STATE_ONE, STATE_TWO, STATE_THREE);
      machine.allowChange(INITIAL_STATE, STATE_ONE);
      machine.allowChange(STATE_ONE, STATE_TWO);
      machine.allowChange(STATE_TWO, STATE_THREE);
      Assertions.assertNotNull(machine);
      
      boolean[] passed = new boolean[1];
      passed[0] = false;
      
      machine.addLeaveListener(STATE_TWO, (o, n) -> passed[0] = true);
      
      machine.setState(STATE_ONE);
      wait(10);
      Assertions.assertFalse(passed[0]);
      
      machine.setState(STATE_TWO);
      wait(10);
      Assertions.assertFalse(passed[0]);
      
      machine.setState(STATE_THREE);
      wait(10);
      Assertions.assertTrue(passed[0]);
   }
   
   @Test
   public void T07_simpleStateMachineChangeStateWithSpecificListenerTest()
   {
      StateMachine<String> machine = new StateMachine<>(INITIAL_STATE);
      machine.addStates(NEW_STATE, STATE_ONE, STATE_TWO, STATE_THREE);
      machine.allowChange(INITIAL_STATE, STATE_ONE);
      machine.allowChange(STATE_ONE, STATE_TWO);
      machine.allowChange(STATE_TWO, STATE_THREE);
      Assertions.assertNotNull(machine);
      
      boolean[] passed = new boolean[1];
      passed[0] = false;
      
      machine.addListener(STATE_ONE, STATE_TWO, (o, n) -> passed[0] = true);
      
      machine.setState(STATE_ONE);
      wait(10);
      Assertions.assertFalse(passed[0]);
      
      machine.setState(STATE_TWO);
      wait(10);
      Assertions.assertTrue(passed[0]);
      passed[0] = false;
      
      machine.setState(STATE_THREE);
      wait(10);
      Assertions.assertFalse(passed[0]);
      
      machine.setState(STATE_TWO);
      wait(10);
      Assertions.assertFalse(passed[0]);
   }
   
   @Test
   public void T08_simpleStateMachineChangeStateWithSeveralListenersTest()
   {
      StateMachine<String> machine = new StateMachine<>(INITIAL_STATE);
      machine.addStates(NEW_STATE, STATE_ONE, STATE_TWO, STATE_THREE);
      machine.allowChange(INITIAL_STATE, STATE_ONE);
      machine.allowChange(STATE_ONE, STATE_TWO);
      machine.allowChange(STATE_TWO, STATE_THREE);
      Assertions.assertNotNull(machine);
      
      boolean[] passed = new boolean[3];
      passed[0] = false;
      passed[1] = false;
      passed[2] = false;
      
      machine.addListener(STATE_ONE, STATE_TWO, (o, n) -> passed[0] = true);
      machine.addListener(STATE_TWO, STATE_THREE, (o, n) -> passed[1] = true);
      machine.addListener(STATE_THREE, STATE_ONE, (o, n) -> passed[2] = true);
      
      machine.setState(STATE_ONE);
      wait(10);
      Assertions.assertFalse(passed[0]);
      Assertions.assertFalse(passed[1]);
      Assertions.assertFalse(passed[2]);
      
      machine.setState(STATE_TWO);
      wait(10);
      Assertions.assertTrue(passed[0]);
      Assertions.assertFalse(passed[1]);
      Assertions.assertFalse(passed[2]);
      passed[0] = false;
      
      machine.setState(STATE_THREE);
      wait(10);
      Assertions.assertFalse(passed[0]);
      Assertions.assertTrue(passed[1]);
      Assertions.assertFalse(passed[2]);
      passed[1] = false;
      
      machine.setState(STATE_ONE);
      wait(10);
      Assertions.assertFalse(passed[0]);
      Assertions.assertFalse(passed[1]);
      Assertions.assertTrue(passed[2]);
   }
   
   @Test
   public void T09_simpleStateMachineChangeStateWithConcurrentListenersTest()
   {
      StateMachine<String> machine = new StateMachine<>(INITIAL_STATE);
      machine.addStates(NEW_STATE, STATE_ONE, STATE_TWO, STATE_THREE);
      machine.allowChange(INITIAL_STATE, STATE_ONE);
      machine.allowChange(STATE_ONE, STATE_TWO);
      machine.allowChange(STATE_TWO, STATE_THREE);
      Assertions.assertNotNull(machine);
      
      boolean[] passed = new boolean[3];
      passed[0] = false;
      passed[1] = false;
      passed[2] = false;
      
      machine.addListener(STATE_TWO, STATE_THREE, (o, n) -> passed[0] = true);
      machine.addListener(STATE_TWO, STATE_THREE, (o, n) -> passed[1] = true);
      machine.addListener(STATE_TWO, STATE_THREE, (o, n) -> passed[2] = true);
      
      machine.setState(STATE_ONE);
      wait(10);
      Assertions.assertFalse(passed[0]);
      Assertions.assertFalse(passed[1]);
      Assertions.assertFalse(passed[2]);
      
      machine.setState(STATE_TWO);
      wait(10);
      Assertions.assertFalse(passed[0]);
      Assertions.assertFalse(passed[1]);
      Assertions.assertFalse(passed[2]);
      
      machine.setState(STATE_THREE);
      wait(10);
      Assertions.assertTrue(passed[0]);
      Assertions.assertTrue(passed[1]);
      Assertions.assertTrue(passed[2]);
      passed[0] = false;
      passed[1] = false;
      passed[2] = false;
      
      machine.setState(STATE_ONE);
      wait(10);
      Assertions.assertFalse(passed[0]);
      Assertions.assertFalse(passed[1]);
      Assertions.assertFalse(passed[2]);
   }
   
   @Test
   public void T10_simpleStateMachineChangeStateWithDifferentListenersTest()
   {
      StateMachine<String> machine = new StateMachine<>(INITIAL_STATE);
      machine.addStates(NEW_STATE, STATE_ONE, STATE_TWO, STATE_THREE);
      machine.allowChange(INITIAL_STATE, STATE_ONE);
      machine.allowChange(STATE_ONE, STATE_TWO);
      machine.allowChange(STATE_TWO, STATE_THREE);
      Assertions.assertNotNull(machine);
      
      boolean[] passed = new boolean[4];
      passed[0] = false;
      passed[1] = false;
      passed[2] = false;
      passed[3] = false;
      
      machine.addListener((o, n) -> passed[0] = true);
      machine.addListener(STATE_TWO, STATE_THREE, (o, n) -> passed[1] = true);
      machine.addEnterListener(STATE_THREE, (o, n) -> passed[2] = true);
      machine.addLeaveListener(STATE_TWO, (o, n) -> passed[3] = true);
      
      machine.setState(STATE_ONE);
      wait(10);
      Assertions.assertTrue(passed[0]);
      Assertions.assertFalse(passed[1]);
      Assertions.assertFalse(passed[2]);
      Assertions.assertFalse(passed[3]);
      passed[0] = false;
      
      machine.setState(STATE_TWO);
      wait(10);
      Assertions.assertTrue(passed[0]);
      Assertions.assertFalse(passed[1]);
      Assertions.assertFalse(passed[2]);
      Assertions.assertFalse(passed[3]);
      
      machine.setState(STATE_THREE);
      wait(10);
      Assertions.assertTrue(passed[0]);
      Assertions.assertTrue(passed[1]);
      Assertions.assertTrue(passed[2]);
      Assertions.assertTrue(passed[3]);
      passed[0] = false;
      passed[1] = false;
      passed[2] = false;
      passed[3] = false;
      
      machine.setState(STATE_ONE);
      wait(10);
      Assertions.assertTrue(passed[0]);
      Assertions.assertFalse(passed[1]);
      Assertions.assertFalse(passed[2]);
      Assertions.assertFalse(passed[3]);
      passed[0] = false;
      
      machine.setState(STATE_THREE);
      wait(10);
      Assertions.assertTrue(passed[0]);
      Assertions.assertFalse(passed[1]);
      Assertions.assertTrue(passed[2]);
      Assertions.assertFalse(passed[3]);
      passed[0] = false;
      passed[2] = false;

      machine.setState(STATE_TWO);
      wait(10);
      Assertions.assertTrue(passed[0]);
      Assertions.assertFalse(passed[1]);
      Assertions.assertFalse(passed[2]);
      Assertions.assertFalse(passed[3]);
      passed[0] = false;
      
      machine.setState(STATE_ONE);
      wait(10);
      Assertions.assertTrue(passed[0]);
      Assertions.assertFalse(passed[1]);
      Assertions.assertFalse(passed[2]);
      Assertions.assertTrue(passed[3]);
   }
   
   @Test
   public void T11_simpleStateMachineChangeStateWithPropertyLinkTest()
   {
      StateMachine<String> machine = new StateMachine<>(INITIAL_STATE);
      machine.addStates(NEW_STATE, STATE_ONE, STATE_TWO, STATE_THREE);
      machine.allowChange(INITIAL_STATE, STATE_ONE);
      machine.allowChange(STATE_ONE, STATE_TWO);
      machine.allowChange(STATE_TWO, STATE_THREE);
      Assertions.assertNotNull(machine);
      
      StringProperty linkedProperty = new StringProperty("");
      Assertions.assertEquals("", linkedProperty.get());
      
      machine.linkToState(linkedProperty);
      Assertions.assertEquals(INITIAL_STATE, linkedProperty.get());
      
      machine.setState(STATE_ONE);
      wait(10);
      Assertions.assertEquals(STATE_ONE, linkedProperty.get());
      
      machine.setState(STATE_TWO);
      wait(10);
      Assertions.assertEquals(STATE_TWO, linkedProperty.get());
      
      machine.setState(STATE_THREE);
      wait(10);
      Assertions.assertEquals(STATE_THREE, linkedProperty.get());
   }
   
   private void wait(int timeout)
   {
      try
      {
         Thread.sleep(100);
      }
      catch (InterruptedException e)
      {
         // Nothing
      }
   }
}
