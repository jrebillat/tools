package net.alantea.tools.test.steam;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.tools.steam.EnumeratedStateMachine;
import net.alantea.utils.exception.LntException;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class EnumeratedStateMachineTest
{
   private static enum States {
      INITIAL_STATE,
      STATE_ONE,
      STATE_TWO,
      STATE_THREE
   }
   
   @Test
   public void T01_enumeratededStateMachineCreationTest() throws LntException
   {
      EnumeratedStateMachine<States> machine = new EnumeratedStateMachine<>(States.class, States.INITIAL_STATE);
      Assertions.assertNotNull(machine);

      machine.allowChange(States.INITIAL_STATE, States.STATE_ONE);
      machine.allowChange(States.STATE_ONE, States.STATE_TWO);
      machine.allowChange(States.STATE_TWO, States.STATE_THREE);
      machine.allowChange(States.STATE_THREE, States.STATE_ONE);
      
      boolean done = machine.setState(States.STATE_ONE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(States.STATE_ONE, machine.getState());
      
      done = machine.setState(States.STATE_TWO);
      Assertions.assertTrue(done);
      Assertions.assertEquals(States.STATE_TWO, machine.getState());
      
      done = machine.setState(States.STATE_THREE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(States.STATE_THREE, machine.getState());
      
      done = machine.setState(States.STATE_ONE);
      Assertions.assertTrue(done);
      
      done = machine.setState(States.STATE_THREE);
      Assertions.assertFalse(done);
      Assertions.assertEquals(States.STATE_ONE, machine.getState());
   }
   
   @Test
   public void T02_controlledStateMachineSetLeaveChangesTest() throws LntException
   {
      EnumeratedStateMachine<States> machine = new EnumeratedStateMachine<>(States.class, States.INITIAL_STATE);
      Assertions.assertNotNull(machine);

      machine.allowAllChangesFrom(States.INITIAL_STATE);
      machine.allowAllChangesFrom(States.STATE_ONE);
      machine.allowAllChangesFrom(States.STATE_TWO);
      machine.allowAllChangesFrom(States.STATE_THREE);
      
      boolean done = machine.setState(States.STATE_ONE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(States.STATE_ONE, machine.getState());
      
      done = machine.setState(States.STATE_TWO);
      Assertions.assertTrue(done);
      Assertions.assertEquals(States.STATE_TWO, machine.getState());
      
      done = machine.setState(States.STATE_THREE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(States.STATE_THREE, machine.getState());
      
      done = machine.setState(States.STATE_ONE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(States.STATE_ONE, machine.getState());

      done = machine.setState(States.STATE_THREE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(States.STATE_THREE, machine.getState());
   }
   
   @Test
   public void T03_controlledStateMachineSetEnterChangesTest() throws LntException
   {
      EnumeratedStateMachine<States> machine = new EnumeratedStateMachine<>(States.class, States.INITIAL_STATE);
      Assertions.assertNotNull(machine);
      machine.allowAllChangesFrom(States.INITIAL_STATE);

      machine.allowAllChangesTo(States.STATE_ONE);
      machine.allowAllChangesTo(States.STATE_TWO);
      machine.allowAllChangesTo(States.STATE_THREE);
      
      boolean done = machine.setState(States.STATE_ONE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(States.STATE_ONE, machine.getState());
      
      done = machine.setState(States.STATE_TWO);
      Assertions.assertTrue(done);
      Assertions.assertEquals(States.STATE_TWO, machine.getState());
      
      done = machine.setState(States.STATE_THREE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(States.STATE_THREE, machine.getState());
      
      done = machine.setState(States.STATE_ONE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(States.STATE_ONE, machine.getState());

      done = machine.setState(States.STATE_THREE);
      Assertions.assertTrue(done);
      Assertions.assertEquals(States.STATE_THREE, machine.getState());
   }
}
