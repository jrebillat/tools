package net.alantea.tools.test.steam;

import net.alantea.tools.steam.EnumeratedStateMachine;
import net.alantea.utils.exception.LntException;

public class TestStateMachine extends EnumeratedStateMachine<TestStateMachineEnum>
{
   private int entered = 0;
   private int leaved = 0;
   private int anyIn;
   private int anyOut;
   private int incr1 = 0;
   private int incr2 = 0;
   
   public TestStateMachine(TestStateMachineEnum startingState) throws LntException
   {
      super(TestStateMachineEnum.class, startingState);
   }
   
   public TestStateMachine(String reference, TestStateMachineEnum startingState) throws LntException
   {
      super(reference, TestStateMachineEnum.class, startingState);
   }

   @SuppressWarnings("unused")
   private void onRUNNING_STATEEnter()
   {
      entered = 1;
   }

   @SuppressWarnings("unused")
   private void onRUNNING_STATELeave()
   {
      leaved = 1;
   }

   @SuppressWarnings("unused")
   private void onSTOPPED_STATEEnter()
   {
      entered = 2;
   }

   public int getEnterValue()
   {
      return entered;
   }

   public int getLeaveValue()
   {
      return leaved;
   }

   @SuppressWarnings("unused")
   private void onAnyStateEnter()
   {
      anyIn++;
   }

   @SuppressWarnings("unused")
   private void onAnyStateLeave()
   {
      anyOut++;
   }

   public int getAnyEnterValue()
   {
      return anyIn;
   }

   public int getAnyLeaveValue()
   {
      return anyOut;
   }
   
   public void increment1()
   {
      incr1++;
   }
   
   public int getIncrement1()
   {
      return incr1;
   }
   
   public void increment2()
   {
      incr2++;
   }
   
   public int getIncrement2()
   {
      return incr2;
   }
}
