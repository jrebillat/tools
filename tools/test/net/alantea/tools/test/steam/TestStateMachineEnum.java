package net.alantea.tools.test.steam;

public enum TestStateMachineEnum
{
   IDLE_STATE,
   RUNNING_STATE,
   STOPPED_STATE
}
